﻿namespace Cancom.ViewModel
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClientId { get; set; }
    }
}