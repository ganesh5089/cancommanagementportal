﻿namespace Cancom.ViewModel
{
    public class AccessGeneralSettingsXmlViewModel
    {
        public string WebserviceUrl { get; set; }
        public string ServerIp { get; set; }
        public string TermsAndConditions { get; set; }
    }
}
