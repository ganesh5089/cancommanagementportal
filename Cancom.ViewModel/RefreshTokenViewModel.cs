﻿using System;

namespace Cancom.ViewModel
{
    public class RefreshTokenViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public string ProtectedTicket { get; set; }
    }
}
