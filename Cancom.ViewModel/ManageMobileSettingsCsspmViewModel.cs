﻿namespace Cancom.ViewModel
{
    public class ManageMobileSettingsCsspmViewModel
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string SiteName { get; set; }
        public string MobileName { get; set; }
        public string ProductName { get; set; }
        public string StationName { get; set; }
        public bool IsDownloadMobileFile { get; set; }
    }
}
