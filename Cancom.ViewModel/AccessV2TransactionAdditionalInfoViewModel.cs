﻿namespace Cancom.ViewModel
{
    public class AccessV2TransactionAdditionalInfoViewModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
