﻿namespace Cancom.ViewModel
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public long Length { get; set; }
        public byte[] Data { get; set; }
    }
}
