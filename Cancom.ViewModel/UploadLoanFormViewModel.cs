﻿namespace Cancom.ViewModel
{
    public class UploadLoanFormViewModel
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public bool IsAdmin { get; set; }
    }
}
