﻿namespace Cancom.ViewModel
{
    public class AccessSettingsViewModel
    {
        public int CsspmId { get; set; }
        public AccessGeneralSettingsViewModel GeneralSettings { get; set; }
        public AccessClientSettingsViewModel ClientSettings { get; set; }
        public AccessEnterSettingsViewModel EnterSettings { get; set; }
        public AccessExitSettingsViewModel ExitSettings { get; set; }
        public AccessNetworkSettingsViewModel NetworkSettings { get; set; }
        public AccessFormSettingsViewModel FormSettings { get; set; }
        public AccessOptionsSettingsViewModel OptionsSettings { get; set; }
    }
}
