﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class AccessV2TransactionsDetailsViewModel
    {
        public IEnumerable<AccessV2TransactionAdditionalInfoViewModel> AdditionalInfo { get; set; }
        public AccessV2TransactionDriverLicenseViewModel DriverLicense { get; set; }
        public AccessV2TransactionVehicleLicenseViewModel VehicleLicense { get; set; }
        public string TransactionStatus { get; set; }
    }
   
}
