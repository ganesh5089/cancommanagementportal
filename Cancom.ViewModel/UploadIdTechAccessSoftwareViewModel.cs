﻿using System;

namespace Cancom.ViewModel
{
    public class UploadIdTechAccessSoftwareViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public string UploadedBy { get; set; }
        public DateTime CreationTs { get; set; }
        public bool Active { get; set; }
    }
}
