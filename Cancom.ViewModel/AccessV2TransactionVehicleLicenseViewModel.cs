﻿namespace Cancom.ViewModel
{
    public class AccessV2TransactionVehicleLicenseViewModel
    {
        public string RegistrationNo { get; set; }
        public string VehicleRegistrationNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public string ExpiryDate { get; set; }
        public string VinNo { get; set; }
        public string EngineNo { get; set; }
        public string LicenseNo { get; set; }
    }
}
