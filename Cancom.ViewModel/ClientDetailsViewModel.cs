﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class ClientDetailsViewModel
    {
        public string ClientName { get; set; }
        public int ClientId { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }
}
