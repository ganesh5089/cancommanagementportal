﻿namespace Cancom.ViewModel
{
    public class AccessSettingsXmlViewModel
    {
        public int CsspmId { get; set; }
        public string Version { get; set; }
        public MobileDeviceSettingsXmlViewModel MobileDeviceSettings { get; set; }
        public AdminUserSettingsXmlViewModel AdminUserSettings { get; set; }
        public AccessGeneralSettingsXmlViewModel GeneralSettings { get; set; }
        public AccessClientSettingsXmlViewModel ClientSettings { get; set; }
        public AccessEnterSettingsXmlViewModel EnterSettings { get; set; }
        public AccessExitSettingsXmlViewModel ExitSettings { get; set; }
        public AccessNetworkSettingsXmlViewModel NetworkSettings { get; set; }
        public AccessFormSettingsXmlViewModel FormSettings { get; set; }
        public AccessOptionsSettingsXmlViewModel OptionsSettings { get; set; }
    }
}
