﻿namespace Cancom.ViewModel
{
    public class LookupViewModel
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}
