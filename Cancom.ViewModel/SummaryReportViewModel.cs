﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class SummaryReportViewModel
    {
        public string ChartHeading { get; set; }
        public List<AverageSummaryReportViewModel> ChartData { get; set; }
    }
}
