﻿using System;
using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class FineManagementViewViewModel
    {
        //public int id { get; set; }
        //public string reference_no { get; set; }
        //public int client_id { get; set; }
        //public int fleet_id { get; set; }
        //public int proxy_id { get; set; }
        //public int province_id { get; set; }
        //public int court_region_id { get; set; }
        //public string charge_code { get; set; }
        //public bool isAARTO { get; set; }
        //public decimal offence_amount { get; set; }
        //public bool isNAG { get; set; }
        //public decimal discount_amount { get; set; }
        //public int demerit_points { get; set; }
        //public DateTime offence_date { get; set; }
        //public DateTime? court_date { get; set; }
        //public int ticket_type_id { get; set; }
        //public bool update_is { get; set; }
        //public DateTime created_date { get; set; }
        //public DateTime modify_date { get; set; }
        //public int? user_id { get; set; }
        //public int? application_id { get; set; }
        //public string stored_procedure { get; set; }
        //public int provider_id { get; set; }
        //public decimal reduced_amount { get; set; }
        //public int? original_ticket_id { get; set; }
        //public bool? syntell { get; set; }
        //public bool? TMT { get; set; }
        //public bool? eNatis { get; set; }
        //public string cityOrTown { get; set; }
        //public string steetNameA { get; set; }
        //public string otherLocationInfo { get; set; }
        //public string registration { get; set; }
        //public string court_region { get; set; }
        //public string first_name { get; set; }
        //public string surname { get; set; }
        //public int? return_info_id { get; set; }
        //public string ass_client_name { get; set; }
        //public string ass_client_no { get; set; }
        //public string ass_rental_comp { get; set; }
        //public string branch_code { get; set; }
        //public string branch_rented { get; set; }
        //public string branch_returned { get; set; }
        //public string client_licence_no { get; set; }
        //public DateTime? riCreatedDate { get; set; }
        //public string date_rented { get; set; }
        //public string date_returned { get; set; }
        //public string file_name { get; set; }
        //public bool? id_doc_type { get; set; }
        //public DateTime? riModifiedDate { get; set; }
        //public string offender_cell { get; set; }
        //public string offender_email { get; set; }
        //public string offender_id { get; set; }
        //public string offender_name { get; set; }
        //public string offender_surname { get; set; }
        //public string phys_addr1 { get; set; }
        //public string phys_addr2 { get; set; }
        //public string phys_addr3 { get; set; }
        //public string phys_addr4 { get; set; }
        //public string phys_code { get; set; }
        //public string post_addr1 { get; set; }
        //public string post_addr2 { get; set; }
        //public string post_addr3 { get; set; }
        //public string post_addr4 { get; set; }
        //public string post_code { get; set; }
        //public string ra_number { get; set; }
        //public string time_rented { get; set; }
        //public string time_returned { get; set; }
        //public string country { get; set; }

        public FineManagementFineInfoViewModel FineInfo { get; set; }
        public FineManagementFineImageViewModel FineImages { get; set; }
        public FineManagementReturnInfoViewModel ReturnInfo { get; set; }
        public FineManagementItcInfoViewModel ItcInfo { get; set; }
        public FineManagementRedirectionInfoViewModel RedirectionInfo { get; set; }
    }

    #region Fine Info
    public class FineManagementFineInfoViewModel
    {
        public int TicketId { get; set; }
        public int ClientId { get; set; }
        public string ReferenceNo { get; set; }
        public string CourtRegion { get; set; }
        public string OffenceDate { get; set; }
        public string Registration { get; set; }
        public decimal OffenceAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public int DemeritPoints { get; set; }
        public string ChargeCode { get; set; }
        public bool IsAarto { get; set; }
        public bool IsNag { get; set; }
        public string TicketTypeDescription { get; set; }
        public string MetroStatus { get; set; }
        public string CurrentCancomStatus { get; set; }
        public string CityOrTown { get; set; }
        public string Suburb { get; set; }
        public string SteetName { get; set; }
        public string OtherLocationInfo { get; set; }
        public List<FineManagementFineInfoStatusViewModel> FineStatusList { get; set; }
    }

    public class FineManagementFineInfoStatusViewModel
    {
        public string Status { get; set; }
        public DateTime Date { get; set; }
    }
    #endregion

    #region Fine Images
    public class FineManagementFineImageViewModel
    {
        public List<FineManagementFineImagePathViewModel> FineImageList { get; set; }
        public List<FineManagementFineImageDetailsViewModel> FineImageDetailList { get; set; }
    }
    public class FineManagementFineImagePathViewModel
    {
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
    }
    public class FineManagementFineImageDetailsViewModel
    {
        public string FirstIssueDate { get; set; }
        public string CourtDate { get; set; }
        public string ElectronicUpload { get; set; }
        public string DateRecieved { get; set; }
        public int ImageId { get; set; }
    }
    #endregion

    #region Return Info
    public class FineManagementReturnInfoViewModel
    {
        # region Offender Details

        public int Id { get; set; }
        public string OffenderName { get; set; }
        public string OffendeSurname { get; set; }
        public string OffenderId { get; set; }
        public String IdDocType { get; set; }
        public string OffenderCell { get; set; }
        public string OffenderEmail { get; set; }

        public string PhysAddr1 { get; set; }
        public string PhysAddrUnitNumber { get; set; }
        public string PhysAddrComplex { get; set; }
        public string PhysAddrStreetnumber { get; set; }
        public string PhysAddrStreetname { get; set; }
        public string PhysAddrSuburb { get; set; }
        public string PhysAddrCity { get; set; }

        public string PhysAddr2 { get; set; }
        public string PhysAddr3 { get; set; }
        public string PhysAddr4 { get; set; }
        public string PostAddr1 { get; set; }
        public string PostAddr2 { get; set; }
        public string PostAddr3 { get; set; }
        public string PostAddr4 { get; set; }
        public string InfoSource { get; set; }
        public string PhysCode { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        #endregion

        #region Rental Details
        public string RaNumber { get; set; }
        public string ClientLicenceNo { get; set; }
        public string AssRentalComp { get; set; }
        public string AssClientNo { get; set; }
        public string AssClientName { get; set; }
        public string BranchCode { get; set; }
        public string DateRented { get; set; }
        public string TimeRented { get; set; }
        public string BranchRented { get; set; }
        public string DateReturned { get; set; }
        public string TimeReturned { get; set; }
        public string BranchReturned { get; set; }
        #endregion
    }
    #endregion

    #region ITC Info
    public class FineManagementItcInfoViewModel
    {
        //----Personal Info----//

        public string Surname { get; set; }
        public string Forename1 { get; set; }
        public string Dob { get; set; }
        public string Email { get; set; }

        //----Contact Details----//

        public string Cell1No { get; set; }
        public string Cell2No { get; set; }
        public string Cell3No { get; set; }

        //----Physical Address 1----//

        public string ItcFirstAddr1 { get; set; }
        public string ItcFirstAddr2 { get; set; }
        public string ItcFirstAddr3 { get; set; }
        public string ItcFirstAddr4 { get; set; }

        //----Physical Address 2----//

        public string ItcSecondAddr1 { get; set; }
        public string ItcSecondAddr2 { get; set; }
        public string ItcSecondAddr3 { get; set; }
        public string ItcSecondAddr4 { get; set; }

        //----Physical Address 3----//

        public string ItcThirdAddr1 { get; set; }
        public string ItcThirdAddr2 { get; set; }
        public string ItcThirdAddr3 { get; set; }
        public string ItcThirdAddr4 { get; set; }

        //----Physical Address 4----//

        public string ItcFourthAddr1 { get; set; }
        public string ItcFourthAddr2 { get; set; }
        public string ItcFourthAddr3 { get; set; }
        public string ItcFourthAddr4 { get; set; }
    }
    #endregion

    #region Redirection Info
    public class FineManagementRedirectionInfoViewModel
    {
        # region Offender Details
        public string OffenderName { get; set; }
        public string OffenderSurname { get; set; }
        public string OffenderId { get; set; }
        public string OffenderCell { get; set; }
        public string OffenderEmail { get; set; }
        public bool? IdDocType { get; set; }
        public string PhysAddr1 { get; set; }
        public string PhysAddr2 { get; set; }
        public string PhysAddr3 { get; set; }
        public string PhysAddr4 { get; set; }
        public string PostAddr1 { get; set; }
        public string PostAddr2 { get; set; }
        public string PostAddr3 { get; set; }
        public string PostAddr4 { get; set; }
        public string InfoSource { get; set; }
        #endregion

        #region Rental Details
        public string RaNumber { get; set; }
        public string ClientLicenceNo { get; set; }
        public string AssRentalComp { get; set; }
        public string AssClientNo { get; set; }
        public string AssClientName { get; set; }
        public string BranchCode { get; set; }
        public string DateRented { get; set; }
        public string TimeRented { get; set; }
        public string BranchRented { get; set; }
        public string DateReturned { get; set; }
        public string TimeReturned { get; set; }
        public string BranchReturned { get; set; }
        #endregion
    }
    #endregion
}
