﻿namespace Cancom.ViewModel
{
    public class MobileDeviceSettingsXmlViewModel
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int MobileId { get; set; }
        public string MobileName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int StationId { get; set; }
        public string StationName { get; set; }
       
    }
}
