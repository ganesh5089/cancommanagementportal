﻿namespace Cancom.ViewModel
{
    public class AccessExitSettingsXmlViewModel
    {
        public bool? VehicleType { get; set; }
        public string VehicleTypeValue { get; set; }
        public bool? VehicleLicense { get; set; }
        public bool? DriverLicense { get; set; }
        public bool? Signature { get; set; }
        public bool? ValidateDriver { get; set; }
        public bool? SmsExit { get; set; }
    }
}
