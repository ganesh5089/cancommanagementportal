﻿namespace Cancom.ViewModel
{
    public class UserAccessPermission
    {
        public int UserAccessId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ClientId { get; set; }

        public string ClientName { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
