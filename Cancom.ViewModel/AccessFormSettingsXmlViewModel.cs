﻿namespace Cancom.ViewModel
{
    public class AccessFormSettingsXmlViewModel
    {
        public bool? IsCellLength { get; set; }
        public string CellLengthValue { get; set; }
        public bool? IsCardNumberLength { get; set; }
        public string CardNumberLengthValue { get; set; }
        public bool? AdvancedNumpad { get; set; }
    }
}
