﻿using System;

namespace Cancom.ViewModel
{
    public class ViewTicketViewModel
    {
        public string NoticeNo { get; set; }

        public string RaNumber { get; set; }
        
        public string CarReg { get; set; }

        public string Region { get; set; }

        public DateTime? FormattedDate { get; set; }

        public string MetroStatus { get; set; }

        public string Status { get; set; }

        public bool? Image { get; set; }

        public string ViewFine { get; set; }
    }
}
