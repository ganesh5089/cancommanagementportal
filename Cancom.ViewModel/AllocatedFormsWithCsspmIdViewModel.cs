﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class AllocatedFormsWithCsspmIdViewModel
    {
        public int CsspmId { get; set; }
        public List<LookupViewModel> AllocatedForms { get; set; }
    }
}
