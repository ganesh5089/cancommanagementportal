﻿namespace Cancom.ViewModel
{
    public class DashboardClientCountViewModel
    {
        public int TotalWebClient { get; set; }
        public int TotalAccessClient { get; set; }
        public int TotalActiveAccessClient { get; set; }
        public int TotalInActiveAccessClient { get; set; }
    }
}