﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class SabTransactionsViewModel
    {
        public List<LateTransactionsViewModel> LateTransactions { get; set; }
        public int Count { get; set; }
    }
}
