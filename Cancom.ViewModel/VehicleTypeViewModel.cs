﻿namespace Cancom.ViewModel
{
    public class VehicleTypeViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int SiteId { get; set; }
        public int Csspmid { get; set; }
        public string SiteName { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionName { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int StationId { get; set; }
        public int ProductId { get; set; }
        public int MobileId { get; set; }
    }
}
