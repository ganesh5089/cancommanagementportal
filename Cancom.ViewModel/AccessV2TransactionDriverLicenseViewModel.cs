﻿namespace Cancom.ViewModel
{
    public class AccessV2TransactionDriverLicenseViewModel
    {
        public string DriverName { get; set; }
        public string IdNumber { get; set; }
        public string Dob { get; set; }
        public string Gender { get; set; }
        public string CertificateNo { get; set; }
        public string DateValidTill { get; set; }
        public string Image { get; set; }

    }
}
