﻿namespace Cancom.ViewModel
{
    public class AccessGeneralSettingsViewModel
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public string WebserviceUrl { get; set; }
        public string ServerIp { get; set; }
        public string TermsAndConditions { get; set; }
    }
}
