﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class UploadDataViewModel
    {
        public List<KeyValuePair<string, string>> FormData { get; set; }
        public FileViewModel FileViewModel { get; set; }
    }
}
