﻿namespace Cancom.ViewModel
{
    public class AverageSummaryReportViewModel
    {
        public string ScannedDate { get; set; }
        public decimal? AverageTime { get; set; }
    }
}
