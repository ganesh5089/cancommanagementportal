﻿using System;

namespace Cancom.ViewModel
{
    public class TransactionTypeViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Active { get; set; }
        public int SiteId { get; set; }
        public int Csspmid { get; set; }
        public string SiteName { get; set; }

        public int ClientId { get; set; }
        public int StationId { get; set; }
        public int ProductId { get; set; }
        public int MobileId { get; set; }
    }
}
