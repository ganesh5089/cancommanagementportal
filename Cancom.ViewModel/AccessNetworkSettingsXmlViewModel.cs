﻿namespace Cancom.ViewModel
{
    public class AccessNetworkSettingsXmlViewModel
    {
        public bool? Use3G { get; set; }
        public bool? Use802Network { get; set; }
        public bool? UseGps { get; set; }
        public bool? UsePhone { get; set; }
        public bool? UseTranAccess { get; set; }
        public bool? UseSsFeature { get; set; }
        public string Value { get; set; }
    }
}
