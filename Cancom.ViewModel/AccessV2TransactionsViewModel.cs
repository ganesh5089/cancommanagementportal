﻿using System;

namespace Cancom.ViewModel
{
    public class AccessV2TransactionsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RegistrationNo { get; set; }
        public string DriverId { get; set; }
        public string Type { get; set; }
        public string Site { get; set; }
        public DateTime Date { get; set; }
    }
}
