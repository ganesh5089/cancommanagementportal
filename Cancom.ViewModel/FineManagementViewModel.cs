﻿using System;

namespace Cancom.ViewModel
{
    public class FineManagementPropertyViewModel
    {
        public string NoticeNo { get; set; }
        public string RaNumber { get; set; }
        public string CarRegistration { get; set; }
        public string Region { get; set; }
        public DateTime OffenceDate { get; set; }
        public string MetroStatus { get; set; }
        public string Status { get; set; }
        public int? TicketId { get; set; }
    }
}
