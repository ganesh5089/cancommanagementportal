﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cancom.ViewModel
{
    public class SabTransactionCountViewModel
    {
        public int? LateTransaction { get; set; }
        public int? OnTimeTransaction { get; set; }
        public String ChartHeading { get; set; }
    }
}
