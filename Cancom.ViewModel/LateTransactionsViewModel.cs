﻿namespace Cancom.ViewModel
{
    public class LateTransactionsViewModel
    {
        public string SiteName { get; set; }
        public string MobileName { get; set; }
        public string UploadedTime { get; set; }
    }
}
