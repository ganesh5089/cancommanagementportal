﻿namespace Cancom.ViewModel
{
    public class AccessOptionsSettingsViewModel
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public bool? VehicleExitPermission { get; set; }
        public bool? StockTracking { get; set; }
        public bool? OverRideBudgetFlow { get; set; }
        public bool? OnPermissionEntry { get; set; }
        public string EnterPermissionDetails { get; set; }
        public bool? RacCardCheck { get; set; }
        public bool? CheckForDuplicateEntry { get; set; }
        public bool? CheckForDuplicateDocs { get; set; }
        public bool? CheckForNoumberOfOccupants { get; set; }
        public bool? OverrideDriverLicense { get; set; }

    }
}
