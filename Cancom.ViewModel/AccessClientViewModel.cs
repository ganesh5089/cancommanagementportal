﻿namespace Cancom.ViewModel
{
    public class AccessClientViewModel
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int SiteId { get; set; }
        public int MobileId { get; set; }
        public int ProductId { get; set; }
        public int StationId { get; set; }
        public string ClientName { get; set; }
        public string SiteName { get; set; }
        public string MobileName { get; set; }
        public string ProductName { get; set; }
        public string StationName { get; set; }
        public string Description { get; set; }
    }
}
