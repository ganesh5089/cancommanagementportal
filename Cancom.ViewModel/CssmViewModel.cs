﻿using System.Collections.Generic;

namespace Cancom.ViewModel
{
    public class CssmViewModel
    {
        public string Title { get; set; }
        public List<CssmTreeSiteViewModel> Levels { get; set; }
    }

    public class CssmTreeSiteViewModel
    {
        public string Title { get; set; }
        public int Csspmid { get; set; }
        public List<CssmTreeStationViewModel> Levels { get; set; }
    }

    public class CssmTreeStationViewModel
    {
        public string Title { get; set; }
        public int Csspmid { get; set; }
        public List<CssmTreeMobileViewModel> Levels { get; set; }
    }

    public class CssmTreeMobileViewModel
    {
        public string Title { get; set; }
        public int Csspmid { get; set; }
    }
}
