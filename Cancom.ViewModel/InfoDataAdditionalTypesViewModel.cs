﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cancom.ViewModel
{
    public class InfoDataAdditionalTypesViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Seq { get; set; }
        public int SiteId { get; set; }
        public bool Active { get; set; }
    }
}
