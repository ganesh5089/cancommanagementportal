﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace Cancom.Commands
{
    public class AsyncValidationRequestHandler<TRequest, TResponse>
          : IAsyncCommandRequestHandler<TRequest, TResponse>
          where TRequest : IAsyncRequest<TResponse>
          where TResponse : CommandResult, new()
    {
        private readonly IAsyncRequestHandler<TRequest, TResponse> innerHander;
        private readonly IValidator<TRequest>[] validators;

        public AsyncValidationRequestHandler(IAsyncRequestHandler<TRequest, TResponse> innerHandler, IValidator<TRequest>[] validators)
        {
            this.validators = validators;
            innerHander = innerHandler;
        }

        public async Task<TResponse> Handle(TRequest message)
        {
            var context = new ValidationContext(message);

            var results = new List<ValidationResult>();
            foreach (var validator in validators)
            {
                results.Add(await validator.ValidateAsync(context));
            }

            var failures = results.SelectMany(r => r.Errors)
                                  .Where(f => f != null)
                                  .ToList();

            if (failures.Any())
            {
                var result = new TResponse();
                result.SetFailures(failures.Select(f => f.ErrorMessage).ToList());
                return result;
            }

            return await innerHander.Handle(message);
        }
    }
}
