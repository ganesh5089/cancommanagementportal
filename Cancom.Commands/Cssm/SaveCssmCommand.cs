﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.Cssm
{
    public class SaveCssmCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public int Csspmid { get; set; }
        public string ClientName { get; set; }
        public string SiteName { get; set; }
        public string StationName { get; set; }
        public string ProductName { get; set; }
        public string MobileName { get; set; }
    }
}
