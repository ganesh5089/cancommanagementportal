﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.Cssm
{
    public class SaveCssmCommandHandler : IAsyncCommandRequestHandler<SaveCssmCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveCssmCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveCssmCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_insert_admin_csspm_new(command.Csspmid, command.ClientName, command.SiteName, command.StationName, command.ProductName, command.MobileName);
            });
            
            return new SuccessResult();
        }
    }
}
