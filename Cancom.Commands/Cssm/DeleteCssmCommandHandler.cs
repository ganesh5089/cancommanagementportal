﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.Cssm
{
    public class DeleteCssmCommandHandler : IAsyncCommandRequestHandler<DeleteCssmCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public DeleteCssmCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteCssmCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_delete_csspm(command.Id);
            });
            
            return new SuccessResult();
        }
    }
}
