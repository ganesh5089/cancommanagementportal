﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.Cssm
{
    public class DeleteCssmCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteCssmCommand(IUserProvider user, int id) : base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
