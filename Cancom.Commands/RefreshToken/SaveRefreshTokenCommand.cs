﻿using System;
using Cancom.Common;
using MediatR;

namespace Cancom.Commands.RefreshToken
{
    public class SaveRefreshTokenCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public SaveRefreshTokenCommand()
        {

        }

        public SaveRefreshTokenCommand(string id, string userName, DateTime issuedUtc, DateTime expiresUtc, string protectedTicket)
        {
            Id = id;
            UserName = userName;
            IssuedUtc = issuedUtc;
            ExpiresUtc = expiresUtc;
            ProtectedTicket = protectedTicket;
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public string ProtectedTicket { get; set; }
    }
}
