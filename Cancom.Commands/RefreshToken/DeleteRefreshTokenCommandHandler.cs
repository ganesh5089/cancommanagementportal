﻿using System.Threading.Tasks;
using Cancom.Data.LoginDB;

namespace Cancom.Commands.RefreshToken
{
   public class DeleteRefreshTokenCommandHandler : IAsyncCommandRequestHandler<DeleteRefreshTokenCommand, CommandResult>
    {
        private readonly loginDBDataContext context;
        public DeleteRefreshTokenCommandHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteRefreshTokenCommand command)
        {
            var refreshToken = await context.PortalRefreshToken.FindAsync(command.Id);

            if (refreshToken == null)
                return new FailureResult("Could not finf token id");

            context.PortalRefreshToken.Remove(refreshToken);
            await context.SaveChangesAsync();
            return new SuccessResult();
        }
    }
}
