﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;
using MediatR;
using AutoMapper;

namespace Cancom.Commands.RefreshToken
{
    public class SaveRefreshTokenCommandHandler : IAsyncCommandRequestHandler<SaveRefreshTokenCommand, CommandResult>
    {
        private readonly loginDBDataContext context;
        private readonly IMediator mediatr;

        public SaveRefreshTokenCommandHandler(IMediator mediatr, loginDBDataContext context)
        {
            this.mediatr = mediatr;
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveRefreshTokenCommand command)
        {
            var existingToken = context.PortalRefreshToken
                .AsNoTracking()
                .SingleOrDefault(r => r.UserName == command.UserName);

            if (existingToken != null)
            {
                await mediatr.SendAsync(new DeleteRefreshTokenCommand(command.User, existingToken.Id));
            }

            var token = Mapper.Map<PortalRefreshToken>(command);
             
            context.PortalRefreshToken.Add(token);
            await context.SaveChangesAsync();
            return new SuccessResult();
        }
    }
}