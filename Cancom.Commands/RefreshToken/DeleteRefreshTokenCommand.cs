﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.RefreshToken
{
    public class DeleteRefreshTokenCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteRefreshTokenCommand(IUserProvider user, string id) : base(user)
        {
            Id = id;
        }

        public string Id { get; set; }
    }
}