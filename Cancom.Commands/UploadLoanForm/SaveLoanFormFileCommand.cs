﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Commands.UploadLoanForm
{
    public class SaveLoanFormFileCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public List<KeyValuePair<string, string>> FormData { get; set; }
        public FileViewModel FileViewModel { get; set; }
    }
}
