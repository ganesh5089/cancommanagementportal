﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.UploadLoanForm
{
    public class SaveLoanFormFileCommandHandler : IAsyncCommandRequestHandler<SaveLoanFormFileCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveLoanFormFileCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }


        public async Task<CommandResult> Handle(SaveLoanFormFileCommand command)
        {
            int clientId = Convert.ToInt32(command.FormData.FirstOrDefault(a => a.Key == "ClientId").Value);

            var filename = clientId + "_" + command.FileViewModel.Name;

            using (Stream file = File.OpenWrite(AppDomain.CurrentDomain.BaseDirectory + @"RdlcFileDump\" + filename))
            {
                file.Write(command.FileViewModel.Data, 0, command.FileViewModel.Data.Length);
            }

            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_insert_loanform(clientId, filename);
            });
            
            return new SuccessResult();
        }
    }
}
