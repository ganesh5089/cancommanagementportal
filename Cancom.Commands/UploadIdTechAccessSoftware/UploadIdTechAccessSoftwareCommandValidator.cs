﻿using System;
using FluentValidation;

namespace Cancom.Commands.UploadIdTechAccessSoftware
{
    public class UploadIdTechAccessSoftwareCommandValidator : AbstractValidator<UploadIdTechAccessSoftwareCommand>
    {
        public UploadIdTechAccessSoftwareCommandValidator()
        {
            RuleFor(i => i.FileName).NotEmpty().WithMessage("File Name is required");
            RuleFor(i => i.FileType).NotEmpty().WithMessage("File Type is required");
            RuleFor(i => i.Version).NotEmpty().WithMessage("Version is required");
            RuleFor(i => i.Description).NotEmpty().WithMessage("Description is required");
            RuleFor(i => i.FileData).NotEmpty().WithMessage("File data is required.");

            RuleFor(x => x).Must((x, cancellation) => IsValidVersionNumber(x))
                    .OverridePropertyName("Version")
                    .WithMessage("Invalid version number");
        }


        private static bool IsValidVersionNumber(UploadIdTechAccessSoftwareCommand command)
        {
            var currentDate = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            return currentDate <= Convert.ToInt32(command.Version);

        }
    }
}
