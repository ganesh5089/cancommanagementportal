﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.UploadIdTechAccessSoftware
{
    public class UploadIdTechAccessSoftwareCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public UploadIdTechAccessSoftwareCommand() { }
        public UploadIdTechAccessSoftwareCommand(IUserProvider user) : base(user) { }
        public string FileName { get; set; }
        public int FileType { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }
        public byte[] FileData { get; set; }
    }
}
