﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.UploadIdTechAccessSoftware
{
    public class UploadIdTechAccessSoftwareCommandHandler : IAsyncCommandRequestHandler<UploadIdTechAccessSoftwareCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public UploadIdTechAccessSoftwareCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(UploadIdTechAccessSoftwareCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.new_insert_idtechsoftwarerepository(command.FileName, command.FileType, command.Version, command.Description,
                    command.FileData, command.LoggedOnUserId, command.LoggedOnUserId);
            });

            return new SuccessResult();
        }
    }
}
