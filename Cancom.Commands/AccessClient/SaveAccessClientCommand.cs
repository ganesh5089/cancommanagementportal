﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.AccessClient
{
    public class SaveAccessClientCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string SiteName { get; set; }
        public string MobileName { get; set; }
        public string ProductName { get; set; }
        public string StationName { get; set; }
    }
}
