﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.AccessClient
{
    public class DeleteAccessClientCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteAccessClientCommand(IUserProvider user, int id) : base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
