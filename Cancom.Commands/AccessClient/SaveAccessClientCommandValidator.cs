﻿using FluentValidation;

namespace Cancom.Commands.AccessClient
{
    public class SaveAccessClientCommandValidator : AbstractValidator<SaveAccessClientCommand>
    {
        public SaveAccessClientCommandValidator()
        {
            RuleFor(i => i.ClientName).NotEmpty().WithMessage("Client name is required");
            RuleFor(i => i.SiteName).NotEmpty().WithMessage("Site name is required");
            RuleFor(i => i.StationName).NotEmpty().WithMessage("Station name is required");
            RuleFor(i => i.ProductName).NotEmpty().WithMessage("Product name is required");
            RuleFor(i => i.MobileName).NotEmpty().WithMessage("Mobile name is required");
        }
    }
}
