﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.AccessClient
{
    public class DeleteAccessClientCommandHandler : IAsyncCommandRequestHandler<DeleteAccessClientCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public DeleteAccessClientCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteAccessClientCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_delete_csspm(command.Id);
            });
            
            return new SuccessResult();
        }
    }
}
