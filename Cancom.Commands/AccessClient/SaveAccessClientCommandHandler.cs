﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.AccessClient
{
    public class SaveAccessClientCommandHandler : IAsyncCommandRequestHandler<SaveAccessClientCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveAccessClientCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveAccessClientCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_insert_admin_csspm_new(command.Id, command.ClientName, command.SiteName, command.StationName, command.ProductName, command.MobileName);
            });
            
            return new SuccessResult();
        }
    }
}
