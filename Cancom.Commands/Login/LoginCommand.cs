﻿using Cancom.Common;
using Cancom.Data.LoginDB;
using MediatR;

namespace Cancom.Commands.Login
{
    public class LoginCommand : CancomMessage,  IAsyncRequest<CommandResult<User>>
    {
        public LoginCommand(string companyNameAndUsername, string password)
        {
            CompanyNameAndUsername = companyNameAndUsername;
            Password = password;
        }

        public string CompanyNameAndUsername { get; set; }
       
        public string Password { get; set; }
    }
}