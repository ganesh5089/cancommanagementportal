﻿using FluentValidation;

namespace Cancom.Commands.Login
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(i => i.CompanyNameAndUsername).NotEmpty().WithMessage("Username is required");
            RuleFor(i => i.Password).NotEmpty().WithMessage("Password is required");
        }
    }
}
