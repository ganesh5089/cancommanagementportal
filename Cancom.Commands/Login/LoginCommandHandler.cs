﻿using System.Data.Entity;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;
using MediatR;

namespace Cancom.Commands.Login
{
    public class LoginCommandHandler :  IAsyncCommandRequestHandler<LoginCommand, CommandResult<User>>
    {
        private readonly loginDBDataContext context;
        private readonly IMediator mediatr;

        public LoginCommandHandler(IMediator mediatr, loginDBDataContext context)
        {
            this.mediatr = mediatr;
            this.context = context;
        }

        public async Task<CommandResult<User>> Handle(LoginCommand msg)
        {
            var username = msg.CompanyNameAndUsername;
            var password = msg.Password;
            //var hashedPassword = msg.Password.ToSha256();

            var user = await context.Users
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Username.ToLower() == username && u.Password == password);

            if (user == null)
                return new FailureResult<User>("Invalid Username or Password");

            return new SuccessResult<User>(user);

        }
    }
}