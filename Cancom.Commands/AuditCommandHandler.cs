﻿using System;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.Data;
using Cancom.Data.LoginDB;
using MediatR;

namespace Cancom.Commands
{
    public interface IAsyncPreAuditRequestHandler<in TRequest> where TRequest : CancomMessage
    {
        Task HandleAsync(TRequest request);
    }

    public class AuditCommandHandler<TRequest, TResponse>
        : IAsyncCommandRequestHandler<TRequest, TResponse>
        where TRequest : CancomMessage, IAsyncRequest<TResponse>
        where TResponse : CommandResult, new()
    {
        private readonly IAsyncRequestHandler<TRequest, TResponse> _inner;
        private readonly IAsyncPreAuditRequestHandler<TRequest>[] _asyncPreAuditRequestHandlers;
        private readonly loginDBDataContext context;

        public AuditCommandHandler(IAsyncRequestHandler<TRequest, TResponse> inner, IAsyncPreAuditRequestHandler<TRequest>[] asyncPreAuditRequestHandlers, CfmSv3DataContextFactory factory)
        {
            _inner = inner;
            _asyncPreAuditRequestHandlers = asyncPreAuditRequestHandlers;
            context = factory.GetNewLoginDbDataContext();
        }

        public async Task<TResponse> Handle(TRequest msg)
        {
            TResponse result;

            try
            {
                result = await _inner.Handle(msg);
            }
            catch (Exception ex)
            {
                msg.CommandResult = new FailureResult(ex.Message);
                throw;
            }
            finally
            {
                if (msg.AuditThisMessage)
                    await AuditCommand(msg);
            }

            return result;
        }

        private async Task AuditCommand(TRequest msg)
        {
            foreach (var preRequestHandler in _asyncPreAuditRequestHandlers)
            {
                await preRequestHandler.HandleAsync(msg);
            }

            //var audit = new CommandAudit
            //{
            //    CompanyId = msg.CompanyId,
            //    LoggedOnUserId = msg.LoggedOnUserId,
            //    TimeStamp = DateTime.UtcNow,
            //    MessageId = msg.MessageId,
            //    IsSuccess = msg.CommandResult?.IsSuccess ?? false,
            //    ExceptionMessage = msg.CommandResult?.Exception?.Message ?? msg.CommandResult?.Failures?.JoinWithComma() ?? string.Empty,
            //    CommandType = msg.GetType().Name,
            //    CommandData = JsonConvert.SerializeObject(msg)
            //};

            //context.CommandAudits.Add(audit);
            //await context.SaveChangesAsyncWithErrorHandling();
        }
    }
}
