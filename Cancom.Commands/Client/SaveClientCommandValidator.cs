﻿using FluentValidation;

namespace Cancom.Commands.Client
{
  public  class SaveClientCommandValidator : AbstractValidator<SaveClientCommand>
    {
        public SaveClientCommandValidator()
        {
            RuleFor(i => i.ClientId).NotEmpty().WithMessage("Client id is required");
            RuleFor(i => i.ClientName).NotEmpty().WithMessage("Client name is required");
        }
    }
}
