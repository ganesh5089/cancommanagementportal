﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Commands.Client 
{
    public class SaveClientCommand : CancomMessage, IAsyncRequest<CommandResult>
    { 
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }
}
