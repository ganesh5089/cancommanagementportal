﻿using System.Data.Entity;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;

namespace Cancom.Commands.Client
{
    public class DeleteClientCommandHandler : IAsyncCommandRequestHandler<DeleteClientCommand, CommandResult>
    {
        private readonly loginDBDataContext context;

        public DeleteClientCommandHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteClientCommand command)
        {

            var client = await context.Clients.FirstOrDefaultAsync(i => i.ClientID == command.Id);

            if (client == null)
                return new FailureResult("No record found");

            context.Clients.Remove(client);
            context.SaveChanges();
            return new SuccessResult();

        }
    }
}
