﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;
using AutoMapper;

namespace Cancom.Commands.Client
{
    public class SaveClientCommandHandler : IAsyncCommandRequestHandler<SaveClientCommand, CommandResult>
    {
        private readonly loginDBDataContext context;

        public SaveClientCommandHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveClientCommand command)
        {
            var client = await context.Clients.FirstOrDefaultAsync(item => item.ClientID == command.ClientId) ?? new Data.LoginDB.Client();

            return client.ClientID == 0 ? await Add(command, client) : await Edit(command);
        }

        private async Task<CommandResult> Add(SaveClientCommand command, Data.LoginDB.Client client)
        {
            client.ClientID = command.ClientId;
            client.Name = command.ClientName;

            context.Clients.Add(client);

            if (command.Products != null && command.Products.Count > 0)
            {
                foreach (var clientProduct in command.Products.Select(product => new ClientProduct
                {
                    ClientID = command.ClientId,
                    ProductID = product.Id
                }))
                {
                    context.ClientProducts.Add(clientProduct);
                }
            }

            context.Users.Add(Mapper.Map<User>(command));

            await context.SaveChangesAsync();
            return new SuccessResult();
        }

        private async Task<CommandResult> Edit(SaveClientCommand command)
        {
            if (command.Products != null && command.Products.Any())
            {
                var clientAllProducts = await context.ClientProducts.Where(item => item.ClientID == command.ClientId).ToListAsync();

                context.ClientProducts.RemoveRange(clientAllProducts);

                var data = command.Products.Select(Mapper.Map<ClientProduct>).ToList();
                context.ClientProducts.AddRange(data);
            }
            context.Users.Add(Mapper.Map<User>(command));

            await context.SaveChangesAsync();
            return new SuccessResult();
        }
    }
}
