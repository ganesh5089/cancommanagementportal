﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.Client
{
    public class DeleteClientCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteClientCommand(IUserProvider user, int id) : base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
