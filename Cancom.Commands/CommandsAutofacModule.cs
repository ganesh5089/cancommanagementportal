﻿using System.Linq;
using Autofac;
using Autofac.Core;
using Cancom.Data;
using Cancom.Data.CFMSv3;
using Cancom.Data.CFMSv3_FileRepo;
using Cancom.Data.IDTechAccessV2;
using Cancom.Data.LoginDB;
using FluentValidation;
using MediatR;

namespace Cancom.Commands
{
    public class CommandsAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
 
            builder.RegisterType<loginDBDataContext>().InstancePerRequest();
            builder.RegisterType<CFMSv3DataContext>().InstancePerRequest();
            builder.RegisterType<IDTechAccessV2Entities>().InstancePerRequest();
            builder.RegisterType<CFMSv3_FileRepoEntities>().InstancePerRequest();

            //singletons (same instance serves ALL requests for the duration of the lifetime of the app)
            builder.RegisterInstance<IDataCache>(new GlobalDataCache(new loginDBDataContext(), new CFMSv3DataContext(), new IDTechAccessV2Entities(), new CFMSv3_FileRepoEntities()));

            builder.RegisterType<CfmSv3DataContextFactory>();

            //register all async command handlers
            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces()
                   .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncCommandRequestHandler<,>)))
                   .Select(interfaceType => new KeyedService("IAsyncCommandRequestHandler", interfaceType.GetInterfaces().Single(i => i.Name.StartsWith("IAsyncRequestHandler")))));

            //register all implementations of IAsyncPreAuditRequestHandler<>
            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces().Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncPreAuditRequestHandler<>))));

            //register decorators which effectively create a pipeline of command handlers
            builder.RegisterGenericDecorator(typeof(AsyncValidationRequestHandler<,>), typeof(IAsyncRequestHandler<,>), "IAsyncCommandRequestHandler", "AsyncValidationRequestHandler");
            builder.RegisterGenericDecorator(typeof(AsyncCommandPipeline<,>), typeof(IAsyncRequestHandler<,>), "AsyncValidationRequestHandler", "AsyncCommandPipeline");
            builder.RegisterGenericDecorator(typeof(AuditCommandHandler<,>), typeof(IAsyncRequestHandler<,>), "AsyncCommandPipeline");

            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces().Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncPreCommandRequestHandler<>))));

            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces().Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncPostCommandRequestHandler<,>))));

            RegisterFluentValidators(builder);


        }

        private void RegisterFluentValidators(ContainerBuilder builder)
        {
            var findValidatorsInAssembly = AssemblyScanner.FindValidatorsInAssembly(GetType().Assembly).ToList();
            foreach (var item in findValidatorsInAssembly.Where(i => !i.ValidatorType.IsAbstract))
            {
                builder.RegisterType(item.ValidatorType).As(item.InterfaceType);
            }
        }

    }
}