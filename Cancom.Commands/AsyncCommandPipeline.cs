﻿using System.Threading.Tasks;
using Cancom.Common;
using MediatR;

namespace Cancom.Commands
{
    public interface IAsyncPreCommandRequestHandler<in TRequest> where TRequest : CancomMessage
    {
        Task HandleAsync(TRequest request);
    }

    public interface IAsyncPostCommandRequestHandler<in TRequest, in TResponse> where TRequest : CancomMessage where TResponse : CommandResult, new()
    {
        Task HandleAsync(TRequest request, TResponse response);
    }

    public class AsyncCommandPipeline<TRequest, TResponse>
        : IAsyncCommandRequestHandler<TRequest, TResponse>
        where TRequest : CancomMessage, IAsyncRequest<TResponse>
        where TResponse : CommandResult, new()
    {

        private readonly IAsyncRequestHandler<TRequest, TResponse> _inner;
        private readonly IAsyncPreCommandRequestHandler<TRequest>[] _asyncPreCommandRequestHandlers;
        private readonly IAsyncPostCommandRequestHandler<TRequest, TResponse>[] _asyncPostCommandRequestHandlers;

        public AsyncCommandPipeline(
            IAsyncRequestHandler<TRequest, TResponse> inner,
            IAsyncPreCommandRequestHandler<TRequest>[] asyncPreCommandRequestHandlers,
            IAsyncPostCommandRequestHandler<TRequest, TResponse>[] asyncPostCommandRequestHandlers)
        {
            _inner = inner;
            _asyncPreCommandRequestHandlers = asyncPreCommandRequestHandlers;
            _asyncPostCommandRequestHandlers = asyncPostCommandRequestHandlers;
        }

        public async Task<TResponse> Handle(TRequest message)
        {
            foreach (var preRequestHandler in _asyncPreCommandRequestHandlers)
            {
                await preRequestHandler.HandleAsync(message);
            }

            var result = await _inner.Handle(message);

            message.CommandResult = result;

            foreach (var postRequestHandler in _asyncPostCommandRequestHandlers)
            {
                await postRequestHandler.HandleAsync(message, result);
            }

            return result;
        }
    }
}
