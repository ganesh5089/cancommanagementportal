﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.VehicleType
{
    public class SaveVehicleTypeCommandHandler : IAsyncCommandRequestHandler<SaveVehicleTypeCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveVehicleTypeCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveVehicleTypeCommand command)
        {
            if (command.Id == 0)
            {
                var csspm =
                    context.admin_csspm.FirstOrDefault(
                        item => item.ClientID == command.ClientId && item.SiteID == command.SiteId &&
                                item.StationID == command.StationId &&
                                item.ProductID == command.ProductId &&
                                item.MobileID == command.MobileId);
                if (csspm != null && csspm.ID > 0)
                {
                    await AsyncHelper.MakeItAsync(() =>
                    {
                        context.sp_insert_info_data_cartype_types_new(command.Id, command.Description, command.SiteId, command.TransactionTypeId, csspm.ID);
                    });
                    
                    return new SuccessResult();
                }
            }
            else
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    context.sp_insert_info_data_cartype_types_new(command.Id, command.Description, command.SiteId, command.TransactionTypeId, command.Id);
                });
                
                return new SuccessResult();
            }
            return new FailureResult("Something went wrong on CSSPMID");
        }
    }
}
