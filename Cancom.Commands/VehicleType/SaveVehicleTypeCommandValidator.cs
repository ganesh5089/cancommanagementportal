﻿using FluentValidation;

namespace Cancom.Commands.VehicleType
{
    public class SaveVehicleTypeCommandValidator : AbstractValidator<SaveVehicleTypeCommand>
    {
        public SaveVehicleTypeCommandValidator()
        {
            RuleFor(i => i.SiteId).NotEmpty().WithMessage("Site name is required");
            RuleFor(i => i.Description).NotEmpty().WithMessage("Description is required");
            RuleFor(i => i.TransactionTypeId).NotEmpty().WithMessage("Transaction Type is required");
        }
    }
}
