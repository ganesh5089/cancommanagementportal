﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.VehicleType
{
    public class DeleteVehicleTypeCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteVehicleTypeCommand(IUserProvider user, int id) : base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
