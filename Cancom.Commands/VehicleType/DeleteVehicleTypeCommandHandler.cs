﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.VehicleType
{
    public class DeleteVehicleTypeCommandHandler : IAsyncCommandRequestHandler<DeleteVehicleTypeCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public DeleteVehicleTypeCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteVehicleTypeCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_delete_info_data_cartype_types(command.Id);
            });

            return new SuccessResult();
        }
    }
}
