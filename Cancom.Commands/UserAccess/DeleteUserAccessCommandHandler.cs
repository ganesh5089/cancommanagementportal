﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;

namespace Cancom.Commands.UserAccess
{
    
    public class DeleteUserAccessCommandHandler : IAsyncCommandRequestHandler<DeleteUserAccessCommand, CommandResult>
    {
        private readonly loginDBDataContext context;

        public DeleteUserAccessCommandHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteUserAccessCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.deletePermission(command.UserAccessId);
            });

            await AsyncHelper.MakeItAsync(() =>
            {
                context.delete_client_products(command.ClientId, command.UserId);
            });
            
            return new SuccessResult();
        }
    }
}
