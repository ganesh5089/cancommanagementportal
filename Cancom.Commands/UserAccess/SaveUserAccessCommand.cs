﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Commands.UserAccess
{
    public class SaveUserAccessCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public List<LookupViewModel> Products { get; set; }
    }
}
