﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;
using NExtensions;

namespace Cancom.Commands.UserAccess
{
    public class SaveUserAccessCommandHandler : IAsyncCommandRequestHandler<SaveUserAccessCommand, CommandResult>
    {
        private readonly loginDBDataContext context;

        public SaveUserAccessCommandHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveUserAccessCommand command)
        {
            var products = string.Empty;
            if (command.Products != null && command.Products.Count > 0)
            {
                products = command.Products.Select(item => item.Key).JoinWithComma();
            }

            await AsyncHelper.MakeItAsync(() =>
            {
                context.Insert_user_and_client_products(command.ClientId, command.UserId, products);
            });
            
            return new SuccessResult();
        }
    }
}
