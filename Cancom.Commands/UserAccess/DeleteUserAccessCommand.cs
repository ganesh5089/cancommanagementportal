﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.UserAccess
{
    public class DeleteUserAccessCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteUserAccessCommand(IUserProvider user) : base(user)
        {
            
        }

        public int UserAccessId { get; set; }
        public int ClientId { get; set; }
        public int UserId { get; set; }
    }
}
