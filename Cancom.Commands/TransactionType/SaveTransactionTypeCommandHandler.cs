﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.TransactionType
{
    public class SaveTransactionTypeCommandHandler : IAsyncCommandRequestHandler<SaveTransactionTypeCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveTransactionTypeCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(SaveTransactionTypeCommand command)
        {
            if (command.Csspmid == null || command.Csspmid == 0)
            {
                var csspmid = context.admin_csspm.FirstOrDefault(item => item.ClientID == command.ClientId && item.SiteID == command.SiteId &&
                                                   item.StationID == command.StationId &&
                                                   item.ProductID == command.ProductId &&
                                                   item.MobileID == command.MobileId);
                if (csspmid != null)
                {
                    command.Csspmid = csspmid.ID;
                }
                else
                {
                    return new FailureResult("Something Went Wrong in CSSPM");
                }
            }

            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_insert_transaction_types_portal(command.Id, command.Description, command.SiteId, command.Csspmid);
            });

            return new SuccessResult();

            
        }
    }
}
