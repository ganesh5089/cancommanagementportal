﻿using FluentValidation;

namespace Cancom.Commands.TransactionType
{
    public class SaveTransactionTypeCommandValidator : AbstractValidator<SaveTransactionTypeCommand>
    {
        public SaveTransactionTypeCommandValidator()
        {
            RuleFor(i => i.SiteId).NotEmpty().WithMessage("Site name is required");
            RuleFor(i => i.Description).NotEmpty().WithMessage("Description is required");
        }
    }
}
