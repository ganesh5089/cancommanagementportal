﻿using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.TransactionType
{
    public class DeleteTransactionTypeCommandHandler : IAsyncCommandRequestHandler<DeleteTransactionTypeCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public DeleteTransactionTypeCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(DeleteTransactionTypeCommand command)
        {
            await AsyncHelper.MakeItAsync(() =>
            {
                context.sp_delete_transaction_types(command.Id);
            });
            
            return new SuccessResult();
        }
    }
}
