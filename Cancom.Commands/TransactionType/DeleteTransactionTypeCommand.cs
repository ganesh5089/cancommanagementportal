﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.TransactionType
{
    public class DeleteTransactionTypeCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public DeleteTransactionTypeCommand(IUserProvider user, int id) : base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
