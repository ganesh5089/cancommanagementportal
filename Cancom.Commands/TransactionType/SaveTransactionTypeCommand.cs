﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.TransactionType
{
    public class SaveTransactionTypeCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int? SiteId { get; set; }
        public int? Csspmid { get; set; }
        public int ClientId { get; set; }
        public int StationId { get; set; }
        public int ProductId { get; set; }
        public int MobileId { get; set; }
    }
}
