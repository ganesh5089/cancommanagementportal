﻿using System;
using System.Collections.Generic;
using Cancom.Common;
using NExtensions;

namespace Cancom.Commands
{
    public class CommandResult : ICommandResult
    {
        protected bool isSuccess;
        protected IEnumerable<string> failures;

        public CommandResult() { } //DO NOT remove this default constructor
        public CommandResult(bool isSuccess, IEnumerable<string> failures)
        {
            this.isSuccess = isSuccess;
            this.failures = failures;
        }

        public bool IsSuccess { get { return isSuccess;} }
        public bool IsFailure { get { return !IsSuccess; } }
        public IEnumerable<string> Failures { get { return failures; } }
        public string HtmlFormattedFailures { get { return failures.JoinWith("<br/>"); } }

        public Exception Exception { get; set; }
        public bool HasException { get { return Exception != null; } }

        public void SetFailures(IEnumerable<string> failures)
        {
            isSuccess = false;
            this.failures = failures;
        }
    }

    public class SuccessResult : CommandResult
    {
        public SuccessResult() : base(true, new List<string>()) { }
    }

    public class FailureResult : CommandResult
    {
        public FailureResult(IEnumerable<string> failures) : base(false, failures) { }
        public FailureResult(string failure) : base(false, new[] {failure}) { }
    }

    public class CommandResult<T> : CommandResult 
    {
        public CommandResult() { } //DO NOT remove this default constructor
        public CommandResult(bool isSuccess, IEnumerable<string> failures)
            : base(isSuccess, failures)
        {
        }

        public T Value { get; set; }
    }

    public class SuccessResult<T> : CommandResult<T>
    {
        public SuccessResult(T value) : base(true, new List<string>())
        {
            Value = value;
        }
    }

    public class FailureResult<T> : CommandResult<T> 
    {
        public FailureResult(IEnumerable<string> failures) : base(false, failures)
        {
            Value = default(T);
        }
        public FailureResult(string failure) : base(false, new[]{failure})
        {
            Value = default(T);
        }
    }
}