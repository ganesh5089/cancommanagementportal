﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveGeneralSettingsCommand : CancomMessage, IAsyncRequest<CommandResult<int>>
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public string WebserviceUrl { get; set; }
        public string ServerIp { get; set; }
        public string TermsAndConditions { get; set; }
    }
}
