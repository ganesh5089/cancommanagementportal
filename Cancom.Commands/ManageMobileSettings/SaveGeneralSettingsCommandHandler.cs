﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveGeneralSettingsCommandHandler : IAsyncCommandRequestHandler<SaveGeneralSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveGeneralSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveGeneralSettingsCommand command)
        {
            var accessGeneralSetting = context.AccessGeneralSetting.FirstOrDefault(item => item.Id == command.Id) ?? new AccessGeneralSetting();

            Mapper.Map(command, accessGeneralSetting);
         
            if (command.Id == 0)
            {
                accessGeneralSetting.CreatedDate = DateTime.Now;
                context.AccessGeneralSetting.Add(accessGeneralSetting);
            }

            accessGeneralSetting.ModifiedDate = DateTime.Now;
            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessGeneralSetting.Id);
        }
    }
}
