﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveExitSettingsCommandHandler : IAsyncCommandRequestHandler<SaveExitSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveExitSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveExitSettingsCommand command)
        {
            var accessExitSetting = context.AccessExitSetting.FirstOrDefault(item => item.CsspmId == command.CsspmId) ?? new AccessExitSetting();

            Mapper.Map(command, accessExitSetting);

            if (command.Id == 0)
            {
                accessExitSetting.CreatedDate = DateTime.Now;
                context.AccessExitSetting.Add(accessExitSetting);
            }

            accessExitSetting.ModifiedDate = DateTime.Now;

            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessExitSetting.Id);
        }
    }
}
