﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveExitSettingsCommand : CancomMessage, IAsyncRequest<CommandResult<int>>
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public bool? VehicleType { get; set; }
        public string VehicleTypeValue { get; set; }
        public bool? VehicleLicense { get; set; }
        public bool? DriverLicense { get; set; }
        public bool? Signature { get; set; }
        public bool? ValidateDriver { get; set; }
        public bool? SmsExit { get; set; }
    }
}
