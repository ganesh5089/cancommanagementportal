﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveFormSettingsCommand : CancomMessage, IAsyncRequest<CommandResult<int>>
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public bool? IsCellLength { get; set; }
        public string CellLengthValue { get; set; }
        public bool? IsCardNumberLength { get; set; }
        public string CardNumberLengthValue { get; set; }
        public bool? AdvancedNumpad { get; set; }
    }
}
