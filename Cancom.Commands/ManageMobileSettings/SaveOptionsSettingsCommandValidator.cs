﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveOptionsSettingsCommandValidator : AbstractValidator<SaveOptionsSettingsCommand>
    {
        public SaveOptionsSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
        }
    }
}
