﻿using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveClientSettingsCommandValidator : AbstractValidator<SaveClientSettingsCommand>
    {
        public SaveClientSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");

            RuleFor(i => i.NetworkInterval)
                .NotEmpty().When(j => j.NetworkCheck).WithMessage("Network Interval is required")
                .GreaterThan(0).When(j => j.NetworkCheck).WithMessage("Network Interval must be greater than 0");

            RuleFor(i => i.NetworkErrorMessage)
                .NotEmpty().When(j => j.NetworkCheck).WithMessage("Network Error Message is required")
                .Length(1, 100).When(j => j.NetworkCheck).WithMessage("Network Error Message should not be greater than 100 characters");

            RuleFor(i => i.BatteryWarningPercentage)
                .NotEmpty().When(j => j.BatteryCheck).WithMessage("Battery Warning Percentage is required")
                .GreaterThan(0).When(j => j.BatteryCheck).WithMessage("Battery Warning Percentage must be greater than 0");

            RuleFor(i => i.BatteryInterval)
              .NotEmpty().When(j => j.BatteryCheck).WithMessage("Battery Interval is required")
              .GreaterThan(0).When(j => j.BatteryCheck).WithMessage("Battery Interval must be greater than 0");
        }
    }
}
