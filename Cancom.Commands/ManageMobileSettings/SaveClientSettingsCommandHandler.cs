﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveClientSettingsCommandHandler : IAsyncCommandRequestHandler<SaveClientSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveClientSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveClientSettingsCommand command)
        {
            var accessClientSetting = context.AccessClientSetting.FirstOrDefault(item => item.Id == command.Id) ?? new AccessClientSetting();

            Mapper.Map(command, accessClientSetting);

            if (command.Id == 0)
            {
                accessClientSetting.CreatedDate = DateTime.Now;
                context.AccessClientSetting.Add(accessClientSetting);
            }

            accessClientSetting.ModifiedDate = DateTime.Now;
            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessClientSetting.Id);
        }
    }
}