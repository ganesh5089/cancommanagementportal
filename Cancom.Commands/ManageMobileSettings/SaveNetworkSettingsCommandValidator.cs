﻿using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveNetworkSettingsCommandValidator : AbstractValidator<SaveNetworkSettingsCommand>
    {
        public SaveNetworkSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
        }
    }
}
