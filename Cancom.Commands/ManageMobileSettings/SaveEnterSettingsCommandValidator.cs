﻿using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveEnterSettingsCommandValidator : AbstractValidator<SaveEnterSettingsCommand>
    {
        public SaveEnterSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
        }
    }
}
