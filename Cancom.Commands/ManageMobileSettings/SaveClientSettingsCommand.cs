﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveClientSettingsCommand : CancomMessage, IAsyncRequest<CommandResult<int>>
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public bool? AccessVerification { get; set; }
        public bool? BlacklistAlert { get; set; }
        public bool? UseOldImager { get; set; }
        public bool? Impro { get; set; }
        public bool? HoneyComb { get; set; }
        public bool? CustomerDocumentVerification { get; set; }
        public bool NetworkCheck { get; set; }
        public int? NetworkInterval { get; set; }
        public string NetworkErrorMessage { get; set; }
        public bool BatteryCheck { get; set; }
        public int? BatteryWarningPercentage { get; set; }
        public int? BatteryInterval { get; set; }
    }
}
