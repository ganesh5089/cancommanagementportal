﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveFormSettingsCommandHandler : IAsyncCommandRequestHandler<SaveFormSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveFormSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveFormSettingsCommand command)
        {
            var accessFormSetting = context.AccessFormSetting.FirstOrDefault(item => item.CsspmId == command.CsspmId) ?? new AccessFormSetting();

            Mapper.Map(command, accessFormSetting);

            if (command.Id == 0)
            {
                accessFormSetting.CreatedDate = DateTime.Now;
                context.AccessFormSetting.Add(accessFormSetting);
            }

            accessFormSetting.ModifiedDate = DateTime.Now;

            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessFormSetting.Id);
        }
    }
}
