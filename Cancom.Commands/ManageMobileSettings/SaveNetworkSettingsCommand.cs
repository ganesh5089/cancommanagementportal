﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveNetworkSettingsCommand : CancomMessage, IAsyncRequest<CommandResult<int>>
    {
        public int Id { get; set; }
        public int CsspmId { get; set; }
        public bool? Use3G { get; set; }
        public bool? Use802Network { get; set; }
        public bool? UseGps { get; set; }
        public bool? UsePhone { get; set; }
        public bool? UseTranAccess { get; set; }
        public bool? UseSSFeature { get; set; }
        public string Value { get; set; }
    }
}
