﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveEnterSettingsCommandHandler : IAsyncCommandRequestHandler<SaveEnterSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveEnterSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveEnterSettingsCommand command)
        {
            var accessEnterSetting = context.AccessEnterSetting.FirstOrDefault(item => item.CsspmId == command.CsspmId) ?? new AccessEnterSetting();

            Mapper.Map(command, accessEnterSetting);

            if (command.Id == 0)
            {
                accessEnterSetting.CreatedDate = DateTime.Now;
                context.AccessEnterSetting.Add(accessEnterSetting);
            }

            accessEnterSetting.ModifiedDate = DateTime.Now;

            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessEnterSetting.Id);
        }
    }
}
