﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveOptionsSettingsCommandHandler : IAsyncCommandRequestHandler<SaveOptionsSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveOptionsSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveOptionsSettingsCommand command)
        {
            var accessOptionsSetting = context.AccessOptionsSetting.FirstOrDefault(item => item.CsspmId == command.CsspmId) ?? new AccessOptionsSetting();

            Mapper.Map(command, accessOptionsSetting);

            if (command.Id == 0)
            {
                accessOptionsSetting.CreatedDate = DateTime.Now;
                context.AccessOptionsSetting.Add(accessOptionsSetting);
            }

            accessOptionsSetting.ModifiedDate = DateTime.Now;

            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessOptionsSetting.Id);
        }
    }
}
