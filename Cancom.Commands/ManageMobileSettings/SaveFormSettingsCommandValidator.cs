﻿using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveFormSettingsCommandValidator : AbstractValidator<SaveEnterSettingsCommand>
    {
        public SaveFormSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
        }
    }
}
