﻿using Cancom.Common.Extensions;
using FluentValidation;
using FluentValidation.Results;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveGeneralSettingsCommandValidator : AbstractValidator<SaveGeneralSettingsCommand>
    {
        public SaveGeneralSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
            RuleFor(i => i.WebserviceUrl).NotEmpty().WithMessage("Webservice Url is required");
            RuleFor(i => i.ServerIp).NotEmpty().WithMessage("Server Url is required");
            RuleFor(i => i.TermsAndConditions).NotEmpty().WithMessage("Terms and Conditions is required");
            Custom(CheckForServerUrlValidationAsync);
            Custom(CheckForUrlValidationAsync);
        }

        private static ValidationFailure CheckForServerUrlValidationAsync(SaveGeneralSettingsCommand command)
        {
            return !command.ServerIp.IsValidUrl()
                ? new ValidationFailure("Server Url", "Invalid Server Url")
                : null;
        }

        private static ValidationFailure CheckForUrlValidationAsync(SaveGeneralSettingsCommand command)
        {
            return !command.WebserviceUrl.IsValidUrl()
                ? new ValidationFailure("Web Service Url", "Invalid Web Service Url")
                : null;
        }
    }
}