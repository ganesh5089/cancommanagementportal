﻿using FluentValidation;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveExitSettingsCommandValidator : AbstractValidator<SaveEnterSettingsCommand>
    {
        public SaveExitSettingsCommandValidator()
        {
            RuleFor(i => i.CsspmId).NotEmpty().WithMessage("CSSPMID is required");
        }
    }
}
