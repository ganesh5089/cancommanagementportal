﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageMobileSettings
{
    public class SaveNetworkSettingsCommandHandler : IAsyncCommandRequestHandler<SaveNetworkSettingsCommand, CommandResult<int>>
    {
        private readonly IDTechAccessV2Entities context;

        public SaveNetworkSettingsCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult<int>> Handle(SaveNetworkSettingsCommand command)
        {
            var accessNetworkSetting = context.AccessNetworkSetting.FirstOrDefault(item => item.CsspmId == command.CsspmId) ?? new AccessNetworkSetting();

            Mapper.Map(command, accessNetworkSetting);

            if (command.Id == 0)
            {
                accessNetworkSetting.CreatedDate = DateTime.Now;
                context.AccessNetworkSetting.Add(accessNetworkSetting);
            }

            accessNetworkSetting.ModifiedDate = DateTime.Now;

            await context.SaveChangesAsync();
            return new SuccessResult<int>(accessNetworkSetting.Id);
        }
    }
}
