﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageFormFlow
{
    public class FormFlowUpAllocatedCommandHandler : IAsyncCommandRequestHandler<FormFlowUpAllocatedCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public FormFlowUpAllocatedCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(FormFlowUpAllocatedCommand command)
        {
            var csspm = context.admin_csspm.FirstOrDefault(
                        item => item.ClientID == command.ClientId && item.SiteID == command.SiteId &&
                                item.StationID == command.StationId &&
                                item.ProductID == command.ProductId &&
                                item.MobileID == command.MobileId);
            if (csspm != null && csspm.ID > 0)
            {
                var form = context.formflows.FirstOrDefault(item => item.CSSPMID == csspm.ID &&
                item.TypeID == command.TransactionTypeId && item.CarTypeID == command.VehicleTypeId && item.FormName == command.FormName);
                if (form != null)
                {
                    await AsyncHelper.MakeItAsync(() =>
                    {
                        context.sp_insert_formflow_down(form.ID, csspm.ID);
                    });
                    
                    return new SuccessResult();
                }
            }
            return new FailureResult("Error on moving up the Allocated Forms");
        }
    }
}
