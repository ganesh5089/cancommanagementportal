﻿using Cancom.Common;
using MediatR;

namespace Cancom.Commands.ManageFormFlow
{
    public class FormFlowDownAllocatedCommand : CancomMessage, IAsyncRequest<CommandResult>
    {
        public int ClientId { get; set; }
        public int StationId { get; set; }
        public int ProductId { get; set; }
        public int MobileId { get; set; }
        public int SiteId { get; set; }
        public int TransactionTypeId { get; set; }
        public int VehicleTypeId { get; set; }
        public string FormName { get; set; }
    }
}
