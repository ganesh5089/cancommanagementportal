﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageFormFlow
{
    public class FormFlowRemoveAllocatedCommandHandler : IAsyncCommandRequestHandler<FormFlowRemoveAllocatedCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public FormFlowRemoveAllocatedCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(FormFlowRemoveAllocatedCommand command)
        {
            var csspm = context.admin_csspm.FirstOrDefault(
                        item => item.ClientID == command.ClientId && item.SiteID == command.SiteId &&
                                item.StationID == command.StationId &&
                                item.ProductID == command.ProductId &&
                                item.MobileID == command.MobileId);
            if (csspm != null && csspm.ID > 0 && command.RemovedAllocatedForms != null && command.RemovedAllocatedForms.Count > 0)
            {
                foreach (var selectedForm in command.RemovedAllocatedForms)
                {
                    var form = context.formflows.FirstOrDefault(item => item.CSSPMID == csspm.ID && item.FormName == selectedForm.Value);
                    if (form != null)
                    {
                        await AsyncHelper.MakeItAsync(() =>
                        {
                            context.sp_insert_formflow_remove(form.ID, csspm.ID);
                        });
                        
                        return new SuccessResult();
                    }
                }
            }
            return new FailureResult("Error on moving to Allocated Forms");
        }
    }
}
