﻿using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Commands.ManageFormFlow
{
    public class FormFlowAddAllocatedCommandHandler : IAsyncCommandRequestHandler<FormFlowAddAllocatedCommand, CommandResult>
    {
        private readonly IDTechAccessV2Entities context;

        public FormFlowAddAllocatedCommandHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(FormFlowAddAllocatedCommand command)
        {
            var csspm = context.admin_csspm.FirstOrDefault(
                        item => item.ClientID == command.ClientId && item.SiteID == command.SiteId &&
                                item.StationID == command.StationId &&
                                item.ProductID == command.ProductId &&
                                item.MobileID == command.MobileId);
            if (csspm != null && csspm.ID > 0)
            {
                if (command.NewAllocatedForms != null && command.NewAllocatedForms.Count > 0)
                {
                    foreach (var selectedForm in command.NewAllocatedForms)
                    {
                        await AsyncHelper.MakeItAsync(() =>
                        {
                            context.sp_insert_formflow(selectedForm.Value, command.VehicleTypeId, command.TransactionTypeId, csspm.ID);
                        });
                    }
                }
                return new SuccessResult();
            }
            return new FailureResult("Error on moving to Allocated Forms");
        }
    }
}
