﻿using MediatR;

namespace Cancom.Commands
{
    public interface IAsyncCommandRequestHandler<TRequest, TResponse> : IAsyncRequestHandler<TRequest, TResponse> where TRequest : IAsyncRequest<TResponse>
    {
         
    }
}