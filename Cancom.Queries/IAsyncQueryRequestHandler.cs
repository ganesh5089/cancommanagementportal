﻿using MediatR;

namespace Cancom.Queries
{
    public interface IAsyncQueryRequestHandler<TRequest, TResponse> : IAsyncRequestHandler<TRequest, TResponse> where TRequest : IAsyncRequest<TResponse>
    {

    }
}
