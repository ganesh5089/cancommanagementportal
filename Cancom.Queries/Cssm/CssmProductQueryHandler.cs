﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Cssm
{
    public class CssmProductQueryHandler : IAsyncQueryRequestHandler<CssmProductQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CssmProductQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CssmProductQuery query)
        {
            var data = await context.admin_products.AsNoTracking().OrderBy(item => item.ProductName).ToListAsync();
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
