﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Cssm
{
    public class CssmProductQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public CssmProductQuery(IUserProvider user) : base(user)
        {
        }
    }
}
