﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Cssm
{
    public class CssmTreeQueryHandler : IAsyncQueryRequestHandler<CssmTreeQuery, CssmViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public CssmTreeQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<CssmViewModel> Handle(CssmTreeQuery message)
        {
            var cssmViewModel = new CssmViewModel
            {
                Levels = new List<CssmTreeSiteViewModel>()
            };

            var data = new List<sp_select_csspm_by_clientid_Result>(); 

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_by_clientid(message.Id).ToList();
            });

            var siteListDistinct = data.Select(item => item.SiteID).Distinct();
            foreach (var site in siteListDistinct.Select(siteId => data.FirstOrDefault(item => item.SiteID == siteId)))
            {
                cssmViewModel.Title = site.ClientName;
                CssmTreeSiteViewModel cssmTreeSiteViewModel = new CssmTreeSiteViewModel();
                cssmTreeSiteViewModel.Csspmid = site.CSSPMID;
                cssmTreeSiteViewModel.Levels = new List<CssmTreeStationViewModel>();
                cssmTreeSiteViewModel.Title = site.SiteName;

                var stationList = data.Where(item => item.SiteID == site.SiteID).ToList();
                var stationListDistinct = stationList.Select(item => item.StationID).Distinct();
                foreach (var stationId in stationListDistinct)
                {
                    var station = stationList.FirstOrDefault(item => item.StationID == stationId);
                    var cssmTreeStationViewModel = new CssmTreeStationViewModel
                    {
                        Levels = new List<CssmTreeMobileViewModel>()
                    };
                    if (station != null)
                    {
                        cssmTreeStationViewModel.Title = station.StationName;
                        cssmTreeStationViewModel.Csspmid = station.CSSPMID;
                        var mobileList = data.Where(item => item.StationID == station.StationID).ToList();
                        foreach (var mobile in mobileList)
                        {
                            CssmTreeMobileViewModel cssmTreeMobileViewModel = new CssmTreeMobileViewModel();
                            cssmTreeMobileViewModel.Title = mobile.MobileName;
                            cssmTreeMobileViewModel.Csspmid = mobile.CSSPMID;
                            cssmTreeStationViewModel.Levels.Add(cssmTreeMobileViewModel);
                        }
                    }
                    cssmTreeSiteViewModel.Levels.Add(cssmTreeStationViewModel);
                }
                cssmViewModel.Levels.Add(cssmTreeSiteViewModel);
            }
            return cssmViewModel;
        }
    }
}
