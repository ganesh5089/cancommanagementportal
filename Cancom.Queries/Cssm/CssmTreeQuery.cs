﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Cssm
{
    public class CssmTreeQuery : CancomMessage, IAsyncRequest<CssmViewModel>
    {
        public CssmTreeQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
