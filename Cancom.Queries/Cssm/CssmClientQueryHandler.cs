﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
    using Cancom.Common.Helpers;
    using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Cssm
{
    public class CssmClientQueryHandler : IAsyncQueryRequestHandler<CssmClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CssmClientQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CssmClientQuery message)
        {
            var data = new List<sp_select_csspm_clients_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_clients(true).OrderBy(item => item.ClientName).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
