﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.User
{
    public class UserQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    { 
        public int ClientId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
