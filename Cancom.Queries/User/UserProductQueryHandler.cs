﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.User
{
    public class UserProductQueryHandler : IAsyncQueryRequestHandler<UserProductQuery, IEnumerable<LookupViewModel>>
    {
        private readonly loginDBDataContext context;

        public UserProductQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(UserProductQuery query)
        {
            var data = new List<getClientProducts_new_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.getClientProducts_new(query.ClientId, query.UserId, query.IsAdmin).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
