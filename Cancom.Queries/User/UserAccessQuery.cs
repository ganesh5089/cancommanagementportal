﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.User
{ 
    public class UserAccessQuery : CancomMessage, IAsyncRequest<IEnumerable<UserAccessPermission>>
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
    }
}
