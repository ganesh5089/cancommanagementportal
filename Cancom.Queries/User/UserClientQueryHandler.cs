﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.User
{
    public class UserClientQueryHandler : IAsyncQueryRequestHandler<UserClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly loginDBDataContext context;

        public UserClientQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(UserClientQuery message)
        {
            var data = new List<getAllClient_List_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.getAllClient_List(true).OrderBy(item => item.Name).ToList();
            });

            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
