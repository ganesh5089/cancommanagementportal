﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.User
{
    public class UserProductQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
