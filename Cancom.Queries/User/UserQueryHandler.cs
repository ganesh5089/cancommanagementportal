﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.User
{
    public class UserQueryHandler : IAsyncQueryRequestHandler<UserQuery, IEnumerable<LookupViewModel>>
    {
        private readonly loginDBDataContext context;

        public UserQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(UserQuery query)
        {
            var data = new List<getUsers_by_clientid_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.getUsers_by_clientid(query.ClientId, query.IsAdmin).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);    
        }
    }
}
