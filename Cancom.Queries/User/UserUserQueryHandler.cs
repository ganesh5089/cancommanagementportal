﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.User
{
    public class UserUserQueryHandler : IAsyncQueryRequestHandler<UserUserQuery, IEnumerable<LookupViewModel>>
    {
        private readonly loginDBDataContext context;

        public UserUserQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(UserUserQuery query)
        {
            var data = new List<getUsers_by_clientid_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.getUsers_by_clientid(query.ClientId, query.IsAdmin).OrderBy(item => item.Username).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
