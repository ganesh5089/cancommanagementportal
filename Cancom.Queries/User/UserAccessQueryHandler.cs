﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.User
{
    public class UserAccessQueryHandler : IAsyncQueryRequestHandler<UserAccessQuery, IEnumerable<UserAccessPermission>>
    {
        private readonly loginDBDataContext context;

        public UserAccessQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<UserAccessPermission>> Handle(UserAccessQuery query)
        {
            var data = new List<getClientUserPermissions_Products_List_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.getClientUserPermissions_Products_List(query.ClientId, query.UserId).ToList();
            });
            
            return data.Select(Mapper.Map<UserAccessPermission>);
        }
    }
}
