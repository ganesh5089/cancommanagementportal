﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.User
{
    public class UserClientQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public UserClientQuery(IUserProvider user) : base(user)
        {
        }
    }
}
