﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.VehicleType
{
    public class VehicleTypeQuery : CancomMessage, IAsyncRequest<IEnumerable<VehicleTypeViewModel>>
    {
        public VehicleTypeQuery(IUserProvider user) : base(user)
        {

        }
    }
}
