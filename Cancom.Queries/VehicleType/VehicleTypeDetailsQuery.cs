﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.VehicleType
{
    public class VehicleTypeDetailsQuery : CancomMessage, IAsyncRequest<VehicleTypeViewModel>
    {
        public VehicleTypeDetailsQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
