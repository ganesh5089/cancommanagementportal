﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.VehicleType
{
    public class VehicleTypeDetailsQueryHandler : IAsyncQueryRequestHandler<VehicleTypeDetailsQuery, VehicleTypeViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public VehicleTypeDetailsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<VehicleTypeViewModel> Handle(VehicleTypeDetailsQuery message)
        {
            var data = new sp_select_info_data_cartype_types_id_Result();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_cartype_types_id(true, message.Id).FirstOrDefault();
            });
            
            return Mapper.Map<VehicleTypeViewModel>(data);
        }
    }
}
