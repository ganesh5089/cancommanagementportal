﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.VehicleType
{
    public class VehicleTypeQueryHandler : IAsyncQueryRequestHandler<VehicleTypeQuery, IEnumerable<VehicleTypeViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public VehicleTypeQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<VehicleTypeViewModel>> Handle(VehicleTypeQuery message)
        {
            var data = new List<sp_select_info_data_cartype_types_view_Portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_cartype_types_view_Portal(true).ToList();
            });
            
            return data.Select(Mapper.Map<VehicleTypeViewModel>);
        }
    }
}
