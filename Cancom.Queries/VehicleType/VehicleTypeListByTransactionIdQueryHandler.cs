﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;
using System.Data.Entity;

namespace Cancom.Queries.VehicleType
{
    public class VehicleTypeListByTransactionIdQueryHandler : IAsyncQueryRequestHandler<VehicleTypeListByTransactionIdQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public VehicleTypeListByTransactionIdQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(VehicleTypeListByTransactionIdQuery message)
        {
            var csspm = context.admin_csspm.FirstOrDefault(item => item.ClientID == message.ClientId && item.SiteID == message.SiteId &&
                                                   item.StationID == message.StationId &&
                                                   item.ProductID == message.ProductId &&
                                                   item.MobileID == message.MobileId);
            if (csspm != null && csspm.ID > 0)
            {
                var data = await context.info_data_cartype_types.Where(item => item.TransactionTypeID == message.TransactionTypeId && item.CSSPMID == csspm.ID && item.Active).AsNoTracking().ToListAsync();
                return data.Select(Mapper.Map<LookupViewModel>);
            }
            return new List<LookupViewModel>();
        }
    }
}
