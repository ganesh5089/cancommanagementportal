﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.ManageFormFlow
{
    public class GetAllocatedFormQuery : CancomMessage, IAsyncRequest<AllocatedFormsWithCsspmIdViewModel>
    {
        public int ClientId { get; set; }
        public int SiteId { get; set; }
        public int StationId { get; set; }
        public int ProductId { get; set; }
        public int MobileId { get; set; }
        public int TransactionTypeId { get; set; }
        public int VehicleTypeId { get; set; }
    }
}
