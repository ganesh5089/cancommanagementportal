﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.ManageFormFlow
{
    public class ManageFormFlowAllocatedFormsQueryHandler : IAsyncQueryRequestHandler<ManageFormFlowAllocatedFormsQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public ManageFormFlowAllocatedFormsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(ManageFormFlowAllocatedFormsQuery query)
        {
            var csspm = context.admin_csspm.FirstOrDefault(item => item.ClientID == query.ClientId && item.SiteID == query.SiteId &&
                                                   item.StationID == query.StationId &&
                                                   item.ProductID == query.ProductId &&
                                                   item.MobileID == query.MobileId);
            if (csspm != null && csspm.ID > 0)
            {
                var data = new List<sp_select_formflow_allocated_Result>();

                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.sp_select_formflow_allocated(true, query.TransactionTypeId, query.VehicleTypeId, csspm.ID).ToList();
                });
                
                return data.Select(Mapper.Map<LookupViewModel>);
            }
            return new List<LookupViewModel>();
        }
    }
}
