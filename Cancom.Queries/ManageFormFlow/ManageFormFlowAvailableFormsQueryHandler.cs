﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;
using System.Data.Entity;

namespace Cancom.Queries.ManageFormFlow
{
    public class ManageFormFlowAvailableFormsQueryHandler : IAsyncQueryRequestHandler<ManageFormFlowAvailableFormsQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public ManageFormFlowAvailableFormsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(ManageFormFlowAvailableFormsQuery message)
        {
            var data = await context.formflow_forms.Where(item => item.Active).AsNoTracking().OrderBy(item => item.FormName).ToListAsync();
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
