﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.ManageFormFlow
{
    public class GetAllocatedFormQueryHandler : IAsyncQueryRequestHandler<GetAllocatedFormQuery, AllocatedFormsWithCsspmIdViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public GetAllocatedFormQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<AllocatedFormsWithCsspmIdViewModel> Handle(GetAllocatedFormQuery query)
        {
            var csspmId = context.admin_csspm
                .AsNoTracking()
                .FirstOrDefault(item => item.ClientID == query.ClientId
                                        && item.SiteID == query.SiteId
                                        && item.StationID == query.StationId
                                        && item.ProductID == query.ProductId
                                        && item.MobileID == query.MobileId)?.ID;

            var result = new AllocatedFormsWithCsspmIdViewModel();
            if (!csspmId.HasValue)
                return result;
       
            var data = new List<sp_select_formflow_allocated_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_formflow_allocated(true, query.TransactionTypeId, query.VehicleTypeId, csspmId).ToList();
            });

            result.CsspmId = csspmId.Value;
            result.AllocatedForms = data.Select(Mapper.Map<LookupViewModel>).ToList();
            return result;
        }
    }
}
