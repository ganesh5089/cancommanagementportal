﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.ManageFormFlow
{
    public class ManageFormFlowAvailableFormsQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public ManageFormFlowAvailableFormsQuery(IUserProvider user) : base(user)
        {

        }
    }
}
