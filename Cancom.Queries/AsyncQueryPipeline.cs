﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace Cancom.Queries
{
    public interface IAsyncPreQueryHandler<in TRequest>
    {
        Task HandleAsync(TRequest request);
    }

    public interface IAsyncPostQueryHandler<in TRequest, TResponse>
    {
        Task<TResponse> HandleAsync(TRequest request, TResponse response);
    }

    public class AsyncQueryPipeline<TRequest, TResponse> : IAsyncQueryRequestHandler<TRequest, TResponse>
        where TRequest : IAsyncRequest<TResponse>
    {
        private readonly IAsyncRequestHandler<TRequest, TResponse> _inner;
        private readonly IAsyncPreQueryHandler<TRequest>[] _asyncPreQuerytHandlers;
        private readonly IAsyncPostQueryHandler<TRequest, TResponse>[] _asyncPostQueryHandlers;
       
        public AsyncQueryPipeline(
           IAsyncRequestHandler<TRequest, TResponse> inner,
           IAsyncPreQueryHandler<TRequest>[] asyncPreRequestHandlers,
           IAsyncPostQueryHandler<TRequest, TResponse>[] asyncPostRequestHandlers)
        {
            _inner = inner;
            _asyncPreQuerytHandlers = asyncPreRequestHandlers;
            _asyncPostQueryHandlers = asyncPostRequestHandlers;
        }

        public async Task<TResponse> Handle(TRequest message)
        {

            foreach (var preRequestHandler in _asyncPreQuerytHandlers)
            {
                await preRequestHandler.HandleAsync(message);
            }
          
            var result = await _inner.Handle(message);

            foreach (var postRequestHandler in _asyncPostQueryHandlers)
            {
                await postRequestHandler.HandleAsync(message, result);
            }

            return result;
        }
    }
}
