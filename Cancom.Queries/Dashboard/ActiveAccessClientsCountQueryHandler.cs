﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Dashboard
{
    public class ActiveAccessClientsCountQueryHandler : IAsyncQueryRequestHandler<ActiveAccessClientsCountQuery, int>
    {
        private readonly IDTechAccessV2Entities context;

        public ActiveAccessClientsCountQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<int> Handle(ActiveAccessClientsCountQuery message)
        {
            var data = await context.admin_csspm
                .AsNoTracking()
                .Where(i=>i.Active)
                .ToListAsync();

            return data.Count;
        }
    }
}
