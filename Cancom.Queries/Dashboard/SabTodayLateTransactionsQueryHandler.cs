﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Common.Helpers;

namespace Cancom.Queries.Dashboard
{
    public class SabTodayLateTransactionsQueryHandler : IAsyncQueryRequestHandler<SabTodayLateTransactionsQuery, SabTransactionsViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public SabTodayLateTransactionsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SabTransactionsViewModel> Handle(SabTodayLateTransactionsQuery query)
        {
            SabTransactionsViewModel sabTransactionsViewModel = new SabTransactionsViewModel();
            var data = new List<SABTransactions_Portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.SABTransactions_Portal(DateTime.Today).ToList();
            });

            sabTransactionsViewModel.LateTransactions = data.Select(Mapper.Map<LateTransactionsViewModel>).ToList();
            sabTransactionsViewModel.Count = data.Count;
            return sabTransactionsViewModel;
        }
    }
}
