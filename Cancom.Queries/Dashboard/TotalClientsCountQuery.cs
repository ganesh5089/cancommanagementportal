﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class TotalClientsCountQuery : CancomMessage, IAsyncRequest<DashboardClientCountViewModel>
    {
        
    }
}
