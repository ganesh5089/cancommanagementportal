﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class CurrentMonthSummaryReportQuery : CancomMessage, IAsyncRequest<SummaryReportViewModel>
    {
        public CurrentMonthSummaryReportQuery(IUserProvider user) : base(user)
        {
        }
    }
}
