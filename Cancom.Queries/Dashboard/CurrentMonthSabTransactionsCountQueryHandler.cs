﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Dashboard
{
    public class CurrentMonthSabTransactionsCountQueryHandler : IAsyncQueryRequestHandler<CurrentMonthSabTransactionsCountQuery, SabTransactionCountViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public CurrentMonthSabTransactionsCountQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SabTransactionCountViewModel> Handle(CurrentMonthSabTransactionsCountQuery message)
        {
            var data = new SabTransactionCountViewModel();

            DateTime thisMonthFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            await AsyncHelper.MakeItAsync(() =>
            {
                data.ChartHeading = $"{DateTime.Now.ToString("MMMM").ToUpper()} {DateTime.Now.ToString("yyyy")} TRANSACTION COUNT";
                data.LateTransaction = context.SABTransactionsForlast30Day_Portal(thisMonthFirstDate, DateTime.Now).FirstOrDefault();
                data.OnTimeTransaction = context.SABOnTimeTransactionsWithDateDiff_Portal(thisMonthFirstDate, DateTime.Now).FirstOrDefault();
            });
            
            return data;
        }
    }
}
