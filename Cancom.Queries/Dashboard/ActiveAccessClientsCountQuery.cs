﻿using Cancom.Common;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class ActiveAccessClientsCountQuery : CancomMessage, IAsyncRequest<int>
    { 
    }
}