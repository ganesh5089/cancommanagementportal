﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Dashboard
{
    public class SabYesterdayLateTransactionsQueryHandler : IAsyncQueryRequestHandler<SabYesterdayLateTransactionsQuery, SabTransactionsViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public SabYesterdayLateTransactionsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SabTransactionsViewModel> Handle(SabYesterdayLateTransactionsQuery query)
        {
            SabTransactionsViewModel sabTransactionsViewModel = new SabTransactionsViewModel();
            var data = new List<SABTransactions_Portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.SABTransactions_Portal(DateTime.Today.AddDays(-1)).ToList();
            });

            sabTransactionsViewModel.LateTransactions = data.Select(Mapper.Map<LateTransactionsViewModel>).ToList();
            sabTransactionsViewModel.Count = data.Count;
            return sabTransactionsViewModel;
        }
    }
}
