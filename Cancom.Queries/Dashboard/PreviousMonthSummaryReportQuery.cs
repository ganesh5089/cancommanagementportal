﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class PreviousMonthSummaryReportQuery : CancomMessage, IAsyncRequest<SummaryReportViewModel>
    {
        public PreviousMonthSummaryReportQuery(IUserProvider user) : base(user)
        {
        }
    }
}
