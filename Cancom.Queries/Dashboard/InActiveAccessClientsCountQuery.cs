﻿using Cancom.Common;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class InActiveAccessClientsCountQuery : CancomMessage, IAsyncRequest<int>
    {
        
    }
}