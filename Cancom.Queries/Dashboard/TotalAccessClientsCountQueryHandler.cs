﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Dashboard
{
    
    public class TotalAccessClientsCountQueryHandler : IAsyncQueryRequestHandler<TotalAccessClientsCountQuery, int>
    {
        private readonly IDTechAccessV2Entities context;

        public TotalAccessClientsCountQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<int> Handle(TotalAccessClientsCountQuery message)
        {
            var data = await context.admin_csspm
                .AsNoTracking()
                .ToListAsync();

            return data.Count;
        }
    }
}
