﻿using System.Data.Entity;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class TotalClientsCountQueryHandler : IAsyncQueryRequestHandler<TotalClientsCountQuery, DashboardClientCountViewModel>
    {
        private readonly loginDBDataContext context;
        private readonly IMediator mediatr;

        public TotalClientsCountQueryHandler(IMediator mediatr, loginDBDataContext context)
        {
            this.mediatr = mediatr;
            this.context = context;
        }

        public async Task<DashboardClientCountViewModel> Handle(TotalClientsCountQuery message)
        {
            var dashboardClientCountViewModel = new DashboardClientCountViewModel();
            var webClient = await context.Clients
                .AsNoTracking()
                .ToListAsync();

            dashboardClientCountViewModel.TotalWebClient = webClient.Count;
            dashboardClientCountViewModel.TotalAccessClient = await mediatr.SendAsync(new TotalAccessClientsCountQuery());
            dashboardClientCountViewModel.TotalActiveAccessClient = await mediatr.SendAsync(new ActiveAccessClientsCountQuery());
            dashboardClientCountViewModel.TotalInActiveAccessClient = await mediatr.SendAsync(new InActiveAccessClientsCountQuery());

            return dashboardClientCountViewModel;
        }
    }
}
