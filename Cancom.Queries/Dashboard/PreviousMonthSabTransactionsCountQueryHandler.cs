﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Dashboard
{
    public class PreviousMonthSabTransactionsCountQueryHandler : IAsyncQueryRequestHandler<PreviousMonthSabTransactionsCountQuery, SabTransactionCountViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public PreviousMonthSabTransactionsCountQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SabTransactionCountViewModel> Handle(PreviousMonthSabTransactionsCountQuery message)
        {
            var data = new SabTransactionCountViewModel();

            DateTime lastMonthLastDate = DateTime.Today.AddDays(0 - DateTime.Today.Day);
            DateTime lastMonthFirstDate = lastMonthLastDate.AddDays(1 - lastMonthLastDate.Day);

            await AsyncHelper.MakeItAsync(() =>
            {
                data.ChartHeading = $"{DateTime.Now.AddMonths(-1).ToString("MMMM").ToUpper()} {DateTime.Now.ToString("yyyy")} TRANSACTION COUNT";
                data.LateTransaction = context.SABTransactionsForlast30Day_Portal(lastMonthFirstDate, lastMonthLastDate.AddDays(1).AddSeconds(-1)).FirstOrDefault();
                data.OnTimeTransaction = context.SABOnTimeTransactionsWithDateDiff_Portal(lastMonthFirstDate, lastMonthLastDate.AddDays(1).AddSeconds(-1)).FirstOrDefault();
            });

            return data;
        }
    }
}
