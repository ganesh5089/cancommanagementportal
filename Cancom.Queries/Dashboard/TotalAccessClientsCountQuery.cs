﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class TotalAccessClientsCountQuery : CancomMessage, IAsyncRequest<int>
    {
        
    }
}
