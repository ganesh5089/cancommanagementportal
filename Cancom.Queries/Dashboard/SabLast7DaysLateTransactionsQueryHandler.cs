﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Dashboard
{
    public class SabLast7DaysLateTransactionsQueryHandler : IAsyncQueryRequestHandler<SabLast7DaysLateTransactionsQuery, SabTransactionsViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public SabLast7DaysLateTransactionsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SabTransactionsViewModel> Handle(SabLast7DaysLateTransactionsQuery query)
        {
            SabTransactionsViewModel sabTransactionsViewModel = new SabTransactionsViewModel();
            var data = new List<SABTransactions_DaysDiff_Portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.SABTransactions_DaysDiff_Portal(DateTime.Today, DateTime.Today.AddDays(-7)).ToList();
            });

            sabTransactionsViewModel.LateTransactions = data.Select(Mapper.Map<LateTransactionsViewModel>).ToList();
            sabTransactionsViewModel.Count = data.Count;
            return sabTransactionsViewModel;
        }
    }
}
