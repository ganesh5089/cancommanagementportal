﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class SabYesterdayLateTransactionsQuery : CancomMessage, IAsyncRequest<SabTransactionsViewModel>
    {
        public SabYesterdayLateTransactionsQuery(IUserProvider user) : base(user)
        {
        }
    }
}
