﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Dashboard
{
    public class SabLast7DaysLateTransactionsQuery : CancomMessage, IAsyncRequest<SabTransactionsViewModel>
    {
        public SabLast7DaysLateTransactionsQuery(IUserProvider user) : base(user)
        {
        }
    }
}
