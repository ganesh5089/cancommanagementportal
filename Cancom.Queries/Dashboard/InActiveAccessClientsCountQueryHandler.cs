﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Dashboard
{
    
    public class InActiveAccessClientsCountQueryHandler : IAsyncQueryRequestHandler<InActiveAccessClientsCountQuery, int>
    {
        private readonly IDTechAccessV2Entities context;

        public InActiveAccessClientsCountQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<int> Handle(InActiveAccessClientsCountQuery message)
        {
            var data = await context.admin_csspm
                .AsNoTracking()
                .Where(i => i.Active== false)
                .ToListAsync();

            return data.Count;
        }
    }
}
