﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Dashboard
{
    public class PreviousMonthSummaryReportQueryHandler : IAsyncQueryRequestHandler<PreviousMonthSummaryReportQuery, SummaryReportViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public PreviousMonthSummaryReportQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<SummaryReportViewModel> Handle(PreviousMonthSummaryReportQuery query)
        {
            SummaryReportViewModel summaryReportViewModel = new SummaryReportViewModel();

            List<LastMonth_Average_Summary_Report_Result> data = new List<LastMonth_Average_Summary_Report_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.LastMonth_Average_Summary_Report().ToList();
            });

            summaryReportViewModel.ChartHeading = $"{DateTime.Now.AddMonths(-1).ToString("MMMM").ToUpper()} {DateTime.Now.ToString("yyyy")} MONTHLY AVERAGE REPORT";
            summaryReportViewModel.ChartData = data.Select(Mapper.Map<AverageSummaryReportViewModel>).ToList();

            return summaryReportViewModel;
        }
    }
}
