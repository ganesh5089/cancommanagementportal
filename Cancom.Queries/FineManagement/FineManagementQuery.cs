﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;
using System.Collections.Generic;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementQuery : CancomMessage, IAsyncRequest<IEnumerable<FineManagementPropertyViewModel>>
    {
        public int ClientId { get; set; }
        public string CarRegistration { get; set; }
        public string ReferenceNo { get; set; }
        public int? SearchCriteria { get; set; }
    }
}
