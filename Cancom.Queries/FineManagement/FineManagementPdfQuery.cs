﻿using System;
using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementPdfQuery : CancomMessage, IAsyncRequest<Tuple<MemoryStream, string>>
    {
        public FineManagementPdfQuery(IUserProvider user, int id): base(user)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
