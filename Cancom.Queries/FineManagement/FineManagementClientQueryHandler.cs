﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.CFMSv3;
using Cancom.ViewModel;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementClientQueryHandler : IAsyncQueryRequestHandler<FineManagementClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly CFMSv3DataContext context;

        public FineManagementClientQueryHandler(CFMSv3DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(FineManagementClientQuery message)
        {
            var data = await context.clients
                .AsNoTracking()
                .OrderBy(item => item.client_name)
                .ToListAsync();
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
