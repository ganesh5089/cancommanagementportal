﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementViewQuery : CancomMessage, IAsyncRequest<FineManagementViewViewModel>
    {
        public FineManagementViewQuery(int ticketId)
        {
            TicketId = ticketId;
        }

        public int TicketId { get; }
    }
}
