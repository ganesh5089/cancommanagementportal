﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementClientQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public FineManagementClientQuery(IUserProvider user) : base(user)
        {
        }
    }
}
