﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.CFMSv3;
using Cancom.ViewModel;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementQueryHandler : IAsyncQueryRequestHandler<FineManagementQuery, IEnumerable<FineManagementPropertyViewModel>>
    {
        private readonly CFMSv3DataContext context;

        public FineManagementQueryHandler(CFMSv3DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<FineManagementPropertyViewModel>> Handle(FineManagementQuery message)
        {
            var ticketList = new List<int?>();

            if (!string.IsNullOrEmpty(message.ReferenceNo))
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    ticketList = context.CFMS_WEB_getTickets_AwaitingData_IDs(message.ClientId, null, message.ReferenceNo).ToList();
                });

            }
            else if (!string.IsNullOrEmpty(message.CarRegistration))
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    ticketList = context.CFMS_WEB_getTickets_AwaitingData_IDs(message.ClientId, message.CarRegistration, null).ToList();
                });
            }
            else
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    ticketList = context.CFMS_WEB_getTickets_AwaitingData_IDs(message.ClientId, null, null).ToList();
                });
            }

            if (!ticketList.Any()) return new List<FineManagementPropertyViewModel>();

            var result = (from ticket in ticketList
                          let data = context.CFMS_WEB_getTickets_AwaitingData_Details(Convert.ToInt32(ticket)).SingleOrDefault()
                          where data != null
                          select new FineManagementPropertyViewModel
                          {
                              TicketId = ticket,
                              NoticeNo = data.Notice_No,
                              RaNumber = data.RA_Number,
                              CarRegistration = data.Car_Reg,
                              Region = data.Region,
                              OffenceDate = Convert.ToDateTime(data.Offence_Date),
                              MetroStatus = data.metro_status,
                              Status = data.Status
                          }).ToList();

            if (!message.SearchCriteria.HasValue)
                return result;

            switch (message.SearchCriteria)
            {
                case 1:
                    result = result.Where(item => item.Status == "Sent").ToList();
                    break;
                case 2:
                    result = result.Where(item => item.Status == "Bad ID").ToList();
                    break;
                case 3:
                    result = result.Where(item => item.Status == "Bad Name").ToList();
                    break;
                case 4:
                    result = result.Where(item => item.Status == "Bad Address").ToList();
                    break;
                case 5:
                    result = result.Where(item => item.Status == "Electronic Upload").ToList();
                    break;
                case 6:
                    result = result.Where(item => item.Status == "Taken for Representation").ToList();
                    break;
            }
            return result;
        }
    }
}
