﻿using System;
using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using Cancom.Data.CFMSv3_FileRepo;
using MediatR;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementPdfQueryHandler : IAsyncQueryRequestHandler<FineManagementPdfQuery, Tuple<MemoryStream, string>>
    {
        private readonly IMediator mediatr;
        private readonly CFMSv3_FileRepoEntities cfmSv3FileRepoEntities;

        public FineManagementPdfQueryHandler(IMediator mediatr, CFMSv3_FileRepoEntities cfmSv3FileRepoEntities)
        {
            this.mediatr = mediatr;
            this.cfmSv3FileRepoEntities = cfmSv3FileRepoEntities;
        }

        public async Task<Tuple<MemoryStream, string>> Handle(FineManagementPdfQuery msg)
        {
            var result = await cfmSv3FileRepoEntities.uploaded_files.AsNoTracking().FirstOrDefaultAsync(a => a.id == msg.Id);
            var stream = new MemoryStream();
            stream.Write(result.uploaded_file, 0, result.uploaded_file.Length);
            var fileName = result.filename;
            return Tuple.Create(stream, fileName);
        }
    }
}
