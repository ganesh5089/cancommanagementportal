﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.CFMSv3;
using Cancom.ViewModel;
using System.Data.Entity;

namespace Cancom.Queries.FineManagement
{
    public class FineManagementViewQueryHandler : IAsyncQueryRequestHandler<FineManagementViewQuery, FineManagementViewViewModel>
    {
        private readonly CFMSv3DataContext context;

        public FineManagementViewQueryHandler(CFMSv3DataContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<FineManagementViewViewModel> Handle(FineManagementViewQuery message)
        {
            var result = new FineManagementViewViewModel
            {
                FineInfo = new FineManagementFineInfoViewModel(),
                FineImages = new FineManagementFineImageViewModel(),
                ReturnInfo = new FineManagementReturnInfoViewModel(),
                ItcInfo = new FineManagementItcInfoViewModel(),
                RedirectionInfo = new FineManagementRedirectionInfoViewModel()
            };

            if (message.TicketId > 0)
            {
                var fineInfo = new CFMS_WEB_get_ticket_details_by_id_MVC5_New_Result();

                await AsyncHelper.MakeItAsync(() =>
                {
                    fineInfo = context.CFMS_WEB_get_ticket_details_by_id_MVC5_New(message.TicketId).FirstOrDefault();
                });

                if (fineInfo == null) return result;

                #region Fine Info

                result.FineInfo.TicketId = message.TicketId;
                result.FineInfo.ReferenceNo = fineInfo.reference_no;
                result.FineInfo.CourtRegion = fineInfo.court_region;
                result.FineInfo.OffenceDate = fineInfo.offence_date.ToString("yyyy-MM-dd HH:mm:ss");
                result.FineInfo.Registration = fineInfo.registration;
                result.FineInfo.OffenceAmount = fineInfo.offence_amount;
                result.FineInfo.DiscountAmount = fineInfo.discount_amount;
                result.FineInfo.DemeritPoints = fineInfo.demerit_points;
                result.FineInfo.ChargeCode = fineInfo.charge_code;
                result.FineInfo.IsAarto = fineInfo.isAARTO;
                result.FineInfo.IsNag = fineInfo.isNAG;
                result.FineInfo.TicketTypeDescription = fineInfo.ticket_type_description;
                result.FineInfo.CityOrTown = fineInfo.cityOrTown;
                result.FineInfo.Suburb = fineInfo.suburb;
                result.FineInfo.SteetName = fineInfo.steetNameA;
                result.FineInfo.OtherLocationInfo = fineInfo.otherLocationInfo;

                var metroResult = new CFMS_fine_admin_list_metro_status_history_Result();
                await AsyncHelper.MakeItAsync(() =>
                {
                    metroResult = context.CFMS_fine_admin_list_metro_status_history(message.TicketId).FirstOrDefault();
                });

                result.FineInfo.MetroStatus = metroResult == null ? string.Empty : metroResult.metro_status;

                int? cancomStatusId = await context.cancom_status_tracking.Where(x => x.ticket_id == message.TicketId).AsNoTracking().Select(x => x.cancom_status_id).FirstOrDefaultAsync();
                result.FineInfo.CurrentCancomStatus = await context.cancom_statuses.Where(x => x.id == cancomStatusId).Select(x => x.cancom_status).FirstOrDefaultAsync();

                var statusList = new List<CFMS_fine_admin_list_status_history_portal_Result>();
                await AsyncHelper.MakeItAsync(() =>
                {
                    statusList = context.CFMS_fine_admin_list_status_history_portal(message.TicketId).ToList();
                });

                result.FineInfo.FineStatusList = statusList.Select(Mapper.Map<FineManagementFineInfoStatusViewModel>).ToList();

                #endregion

                #region Return Info

                result.ReturnInfo.Id = message.TicketId;
                result.ReturnInfo.OffenderName = fineInfo.offender_name;
                result.ReturnInfo.OffendeSurname = fineInfo.offender_surname;
                result.ReturnInfo.OffenderId = fineInfo.offender_id;

                result.ReturnInfo.IdDocType = Convert.ToByte(fineInfo.id_doc_type) == 0 ? "Passport" : "ID";

                //if (fineInfo.id_doc_type == null)
                //{
                //    PopulateIDType(1);
                //}
                //else
                //{
                //    int selectedval = Convert.ToInt32(fineInfo.id_doc_type);
                //    //PopulateIDType(selectedval);
                //}

                result.ReturnInfo.OffenderCell = fineInfo.offender_cell;
                result.ReturnInfo.OffenderEmail = fineInfo.offender_email;
                result.ReturnInfo.PhysAddr1 = fineInfo.phys_addr1;

                if (!string.IsNullOrEmpty(fineInfo.phys_addr1))
                {
                    string[] phyaddarr = fineInfo.phys_addr1.Split('|');
                    result.ReturnInfo.PhysAddrUnitNumber = phyaddarr.Length > 0 ? phyaddarr[0] : String.Empty;
                    result.ReturnInfo.PhysAddrComplex = phyaddarr.Length > 1 ? phyaddarr[1] : String.Empty;
                    result.ReturnInfo.PhysAddrStreetnumber = phyaddarr.Length > 2 ? phyaddarr[2] : String.Empty;
                    result.ReturnInfo.PhysAddrStreetname = phyaddarr.Length > 3 ? phyaddarr[3] : String.Empty;
                    result.ReturnInfo.PhysAddrSuburb = phyaddarr.Length > 4 ? phyaddarr[4] : String.Empty;
                    result.ReturnInfo.PhysAddrCity = phyaddarr.Length > 5 ? phyaddarr[5] : String.Empty;
                }

                result.ReturnInfo.PhysAddr2 = fineInfo.phys_addr2;
                result.ReturnInfo.PhysAddr3 = fineInfo.phys_addr3;
                result.ReturnInfo.PhysAddr4 = fineInfo.phys_addr4;
                result.ReturnInfo.PostAddr1 = fineInfo.post_addr1;
                result.ReturnInfo.PostAddr2 = fineInfo.post_addr2;
                result.ReturnInfo.PostAddr3 = fineInfo.post_addr3;
                result.ReturnInfo.PostAddr4 = fineInfo.post_addr4;
                result.ReturnInfo.InfoSource = fineInfo.info_source;
                result.ReturnInfo.PhysCode = fineInfo.phys_code;
                result.ReturnInfo.PostCode = fineInfo.post_code;
                result.ReturnInfo.Country = fineInfo.country;

                result.ReturnInfo.RaNumber = fineInfo.ra_number;
                result.ReturnInfo.ClientLicenceNo = fineInfo.client_licence_no;
                result.ReturnInfo.AssRentalComp = fineInfo.ass_rental_comp;
                result.ReturnInfo.AssClientNo = fineInfo.ass_client_no;
                result.ReturnInfo.AssClientName = fineInfo.ass_client_name;
                result.ReturnInfo.BranchCode = fineInfo.branch_code;
                result.ReturnInfo.DateRented = fineInfo.date_rented;
                result.ReturnInfo.TimeRented = fineInfo.time_rented;
                result.ReturnInfo.BranchRented = fineInfo.branch_rented;
                result.ReturnInfo.DateReturned = fineInfo.date_returned;
                result.ReturnInfo.TimeReturned = fineInfo.time_returned;
                result.ReturnInfo.BranchReturned = fineInfo.branch_returned;

                #endregion

                #region ITC Info

                var itcInfo = new CFMS_misc_get_person_Result();
                await AsyncHelper.MakeItAsync(() =>
                {
                    itcInfo = context.CFMS_misc_get_person(fineInfo.offender_id).FirstOrDefault();
                });

                if (itcInfo != null)
                {
                    result.ItcInfo = Mapper.Map<FineManagementItcInfoViewModel>(itcInfo);
                }

                #endregion

                #region Redirection Info

                var redirectionInfo =
                    context.retail_return_info.Where(x => x.ticket_id == message.TicketId).AsNoTracking()
                        .OrderByDescending(x => x.modify_date)
                        .ToList()
                        .Take(1).FirstOrDefault();
                if (redirectionInfo != null)
                {
                    result.RedirectionInfo = Mapper.Map<FineManagementRedirectionInfoViewModel>(redirectionInfo);
                }

                #endregion

                #region Fine Images

                result.FineImages.FineImageDetailList = new List<FineManagementFineImageDetailsViewModel>();

                var imageDetailsList = new List<CFMS_fine_admin_list_metro_status_history_Result>();
                await AsyncHelper.MakeItAsync(() =>
                {
                    imageDetailsList = context.CFMS_fine_admin_list_metro_status_history(message.TicketId).ToList();
                });

                foreach (var item in imageDetailsList)
                {
                    result.FineImages.FineImageDetailList.Add(Mapper.Map<FineManagementFineImageDetailsViewModel>(item));
                }

                #endregion
            }
            return result;
        }
    }
}
