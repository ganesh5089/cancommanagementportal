﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.Product
{
    public class ProductQueryHandler : IAsyncQueryRequestHandler<ProductQuery, IEnumerable<ProductViewModel>>
    {
        private readonly loginDBDataContext context;

        public ProductQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<ProductViewModel>> Handle(ProductQuery message)
        {
            var data = await context.Products
                .OrderBy(i => i.Name)
                .AsNoTracking()
                .ToListAsync();

            return data.Select(Mapper.Map<ProductViewModel>);
        }
    }
}
