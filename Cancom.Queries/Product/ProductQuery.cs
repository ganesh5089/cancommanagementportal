﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Product
{
    public class ProductQuery : CancomMessage, IAsyncRequest<IEnumerable<ProductViewModel>>
    {
        public ProductQuery(IUserProvider user) : base(user)
        {
        }
    }
}
