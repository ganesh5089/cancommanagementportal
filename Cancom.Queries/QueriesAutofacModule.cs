﻿using System.Linq;
using Autofac;
using Autofac.Core;
using MediatR;

namespace Cancom.Queries
{
    public class QueriesAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            //register all async command handlers
            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces()
                   .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncQueryRequestHandler<,>)))
                   .Select(interfaceType => new KeyedService("IAsyncQueryRequestHandler", interfaceType.GetInterfaces().Single(i => i.Name.StartsWith("IAsyncRequestHandler")))));

            builder.RegisterGenericDecorator(typeof(AsyncQueryPipeline<,>), typeof(IAsyncRequestHandler<,>), "IAsyncQueryRequestHandler");

            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces().Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncPreQueryHandler<>))));

            builder.RegisterAssemblyTypes(GetType().Assembly)
               .As(type => type.GetInterfaces().Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IAsyncPostQueryHandler<,>))));

        }
    }
}
