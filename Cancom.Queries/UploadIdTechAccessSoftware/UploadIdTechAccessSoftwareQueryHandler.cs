﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.UploadIdTechAccessSoftware
{
    public class UploadIdTechAccessSoftwareQueryHandler : IAsyncQueryRequestHandler<UploadIdTechAccessSoftwareQuery, IEnumerable<UploadIdTechAccessSoftwareViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public UploadIdTechAccessSoftwareQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<UploadIdTechAccessSoftwareViewModel>> Handle(UploadIdTechAccessSoftwareQuery message)
        {
            var data = new List<new_get_all_idtechsoftwarerepository_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.new_get_all_idtechsoftwarerepository(message.IsActive, (int)message.FileType).ToList();
            });

            return data.Select(Mapper.Map<UploadIdTechAccessSoftwareViewModel>);
        }
    }
}
