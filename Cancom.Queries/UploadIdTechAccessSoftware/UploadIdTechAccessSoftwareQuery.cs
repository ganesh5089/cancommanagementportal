﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.Common.Enums;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.UploadIdTechAccessSoftware
{
    public class UploadIdTechAccessSoftwareQuery : CancomMessage, IAsyncRequest<IEnumerable<UploadIdTechAccessSoftwareViewModel>>
    {
        public UploadIdTechAccessSoftwareQuery(bool isActive, IdTechSoftwareFileType fileType)
        {
            IsActive = isActive;
            FileType = fileType;
        }

        public bool IsActive { get; }

        public IdTechSoftwareFileType FileType { get; set; }
    }
}
