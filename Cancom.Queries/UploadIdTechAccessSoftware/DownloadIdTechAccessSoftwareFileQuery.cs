﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.UploadIdTechAccessSoftware
{
    public class DownloadIdTechAccessSoftwareFileQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public int Id { get; set; }
        public string FileName { get; set; }
    }
}
