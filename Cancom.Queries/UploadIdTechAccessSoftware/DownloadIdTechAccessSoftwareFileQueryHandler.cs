﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.UploadIdTechAccessSoftware
{
    public class DownloadIdTechAccessSoftwareFileQueryHandler : IAsyncQueryRequestHandler<DownloadIdTechAccessSoftwareFileQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadIdTechAccessSoftwareFileQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadIdTechAccessSoftwareFileQuery query)
        {
            var idTechSoftwareRepository = context.IdTechSoftwareRepository.FirstOrDefault(item => item.Id == query.Id);

            if (idTechSoftwareRepository == null)
                return new MemoryStream();

            var result = idTechSoftwareRepository.FileData;
            var stream = new MemoryStream(result);
            return stream;
        }
    }
}
