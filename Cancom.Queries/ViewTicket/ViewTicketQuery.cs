﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.ViewTicket
{
    public class ViewTicketQuery : IAsyncRequest<IEnumerable<ViewTicketViewModel>>
    {
        public int ClientId { get; set; }

        public string ReferenceNumber { get; set; }

        public string CarReg { get; set; }

        public string RaNumber { get; set; }
    }
}
