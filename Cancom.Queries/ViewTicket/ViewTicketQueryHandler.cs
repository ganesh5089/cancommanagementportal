﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.CFMSv3;
using Cancom.ViewModel;
using Cancom.Common.Helpers;

namespace Cancom.Queries.ViewTicket
{
    public class ViewTicketQueryHandler : IAsyncQueryRequestHandler<ViewTicketQuery, IEnumerable<ViewTicketViewModel>>
    {
        private readonly CFMSv3DataContext context;

        public ViewTicketQueryHandler(CFMSv3DataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<ViewTicketViewModel>> Handle(ViewTicketQuery message)
        {
            var data = new List<CFMS_WEB_getTickets_Result>();
            if (!string.IsNullOrEmpty(message.ReferenceNumber))
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.CFMS_WEB_getTickets(message.ClientId, message.ReferenceNumber.Trim(), null, null).ToList();
                });
                
            }
            else if (!string.IsNullOrEmpty(message.CarReg))
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.CFMS_WEB_getTickets(message.ClientId, null, message.CarReg.Trim(), null).ToList();
                });
            }
            else if (!string.IsNullOrEmpty(message.RaNumber))
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.CFMS_WEB_getTickets(message.ClientId, null, null, message.RaNumber.Trim()).ToList();
                });
            }
            else
            {
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.CFMS_WEB_getTickets(message.ClientId, null, null, null).ToList();
                });
            }
            return data.Select(Mapper.Map<ViewTicketViewModel>);
        }
    }
}
