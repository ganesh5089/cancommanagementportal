﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadFormFlowXmlQueryHandler : IAsyncQueryRequestHandler<DownloadFormFlowXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadFormFlowXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadFormFlowXmlQuery query)
        {
            var data = new List<sp_select_formflow_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_formflow(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument formflowXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("formflow",
                new XElement("ID", forms.ID),
                new XElement("FormName", forms.FormName),
                new XElement("TypeID", forms.TypeID),
                new XElement("CarTypeID", forms.CarTypeID),
                new XElement("Seq", forms.Seq),
                new XElement("ParentID", forms.ParentID)))
                .Where(xelement => formflowXdoc.Root != null))
            {
                formflowXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            formflowXdoc.Save(stream);
            return stream;
        }
    }
}
