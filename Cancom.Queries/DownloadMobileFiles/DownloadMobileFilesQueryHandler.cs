﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Cancom.Common;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadMobileFilesQueryHandler : IAsyncQueryRequestHandler<DownloadMobileFilesQuery, MemoryStream>
    {
        private readonly IMediator mediatr;

        public DownloadMobileFilesQueryHandler(IMediator mediatr)
        {
            this.mediatr = mediatr;
        }

        public async Task<MemoryStream> Handle(DownloadMobileFilesQuery query)
        {
            var settingXml = await mediatr.SendAsync(new DownloadSettingsXmlQuery(query.User, query.CsspmId));
            var formFlowXml = await mediatr.SendAsync(new DownloadFormFlowXmlQuery(query.User, query.CsspmId));
            var infoDataAdditionalTypesXml = await mediatr.SendAsync(new DownloadInfoDataAdditionalTypesXmlQuery(query.User, query.CsspmId));
            var infoDataAdditionalValuesXml = await mediatr.SendAsync(new DownloadInfoDataAdditionalValuesXmlQuery(query.User, query.CsspmId));
            var infoDataApartmentNoValuesXml = await mediatr.SendAsync(new DownloadInfoDataApartmentNoValuesXmlQuery(query.User, query.CsspmId));
            var infoDataCarTypeValuesXml = await mediatr.SendAsync(new DownloadInfoDataCarTypeTypesXmlQuery(query.User, query.CsspmId));
            var infoDataDepotValuesXml = await mediatr.SendAsync(new DownloadInfoDataDepotValuesXmlQuery(query.User, query.CsspmId));
            var infoDataStandNoValuesXml = await mediatr.SendAsync(new DownloadInfoDataStandNoValuesXmlQuery(query.User, query.CsspmId));
            var infoDataUnitNoValuesXml = await mediatr.SendAsync(new DownloadInfoDataUnitNoValuesXmlQuery(query.User, query.CsspmId));
            var transactionTypesXml = await mediatr.SendAsync(new DownloadTransactionTypesXmlQuery(query.User, query.CsspmId));
            var infoDataLaneValuesXml = await mediatr.SendAsync(new DownloadInfoDataLaneValuesXmlQuery(query.User, query.CsspmId));
            var infoDataReasonValuesXml = await mediatr.SendAsync(new DownloadInfoDataReasonValuesXmlQuery(query.User, query.CsspmId));
            var infoDataReason2ValuesXml = await mediatr.SendAsync(new DownloadInfoDataReason2ValuesXmlQuery(query.User, query.CsspmId));
            var infoDataLocationValuesXml = await mediatr.SendAsync(new DownloadInfoDataLocationValuesXmlQuery(query.User, query.CsspmId));
            var infoDataTermsAndConditionXml = await mediatr.SendAsync(new DownloadInfoDataClientTermAndConditionXmlQuery(query.User, query.CsspmId));
            var infoDataSectorValuesXml = await mediatr.SendAsync(new DownloadInfoDataSectorValuesXmlQuery(query.User, query.CsspmId));

            Dictionary<string, byte[]> availableXmlFile = new Dictionary<string, byte[]>();

            if (formFlowXml.Length > 0)
                availableXmlFile.Add(Constant.FormFlowXmlName, formFlowXml.ToArray());

            if (settingXml.Length > 0)
                availableXmlFile.Add(Constant.SettingsXmlName, settingXml.ToArray());

            if (infoDataAdditionalTypesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataAdditionalTypesXmlName, infoDataAdditionalTypesXml.ToArray());

            if (infoDataAdditionalValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataAdditionalValuesXmlName, infoDataAdditionalValuesXml.ToArray());

            if (infoDataApartmentNoValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataApartmentNoValuesXmlName, infoDataApartmentNoValuesXml.ToArray());

            if (infoDataCarTypeValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataCarTypeValuesXmlName, infoDataCarTypeValuesXml.ToArray());

            if (infoDataDepotValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataDepotValuesXmlName, infoDataDepotValuesXml.ToArray());

            if (infoDataStandNoValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataStandNoValuesXmlName, infoDataStandNoValuesXml.ToArray());

            if (infoDataUnitNoValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataUnitNoValuesXmlName, infoDataUnitNoValuesXml.ToArray());

            if (transactionTypesXml.Length > 0)
                availableXmlFile.Add(Constant.TransactionTypesXmlName, transactionTypesXml.ToArray());

            if (infoDataLaneValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataLaneValuesXmlName, infoDataLaneValuesXml.ToArray());

            if (infoDataReasonValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataReasonValuesXmlName, infoDataReasonValuesXml.ToArray());

            if (infoDataReason2ValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataReason2ValuesXmlName, infoDataReason2ValuesXml.ToArray());

            if (infoDataLocationValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataLocationValuesXmlName, infoDataLocationValuesXml.ToArray());

            if (infoDataTermsAndConditionXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataTermsAndConditionXmlName, infoDataTermsAndConditionXml.ToArray());

            if (infoDataSectorValuesXml.Length > 0)
                availableXmlFile.Add(Constant.InfoDataSectorValuesXmlName, infoDataSectorValuesXml.ToArray());

            return ConvertToZip(availableXmlFile);
        }

        private static MemoryStream ConvertToZip(Dictionary<string, byte[]> availableXmlFile)
        {
            using (var outputMemStream = new MemoryStream())
            {
                using (var zipStream = new ZipOutputStream(outputMemStream))
                {
                    zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

                    foreach (var record in availableXmlFile)
                    {
                        var newEntry = new ZipEntry(record.Key)
                        {
                            DateTime = DateTime.Now
                        };

                        zipStream.PutNextEntry(newEntry);

                        var inStream = new MemoryStream(record.Value);
                        StreamUtils.Copy(inStream, zipStream, new byte[4096]);
                        inStream.Close();
                        zipStream.CloseEntry();
                    }
                    zipStream.IsStreamOwner = false; // False stops the Close also Closing the underlying stream.
                    zipStream.Close(); // Must finish the ZipOutputStream before using outputMemStream.
                    outputMemStream.Position = 0;
                    return outputMemStream;
                }
            }
        }
    }
}
