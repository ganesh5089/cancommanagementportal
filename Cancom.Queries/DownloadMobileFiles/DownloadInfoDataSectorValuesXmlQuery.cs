﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataSectorValuesXmlQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public DownloadInfoDataSectorValuesXmlQuery(IUserProvider user, int csspmId) : base(user)
        {
            CsspmId = csspmId;
        }

        public int CsspmId { get; set; }
    }
}
