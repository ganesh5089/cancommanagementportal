﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataDepotValuesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataDepotValuesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataDepotValuesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataDepotValuesXmlQuery query)
        {
            var data = new List<sp_select_info_data_depot_values_portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_depot_values_portal(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataDepotValuesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataDepotValues",
                new XElement("ID", forms.ID),
                new XElement("Value", forms.Value),
                new XElement("SiteID", forms.SiteID),
                new XElement("Active", forms.Active)))
                .Where(xelement => infoDataDepotValuesXdoc.Root != null))
            {
                infoDataDepotValuesXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            infoDataDepotValuesXdoc.Save(stream);
            return stream;
        }
    }
}
