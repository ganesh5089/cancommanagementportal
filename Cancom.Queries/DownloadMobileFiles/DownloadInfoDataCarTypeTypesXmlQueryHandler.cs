﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataCarTypeTypesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataCarTypeTypesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataCarTypeTypesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataCarTypeTypesXmlQuery query)
        {
            var data = new List<sp_select_info_data_cartype_types_portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_cartype_types_portal(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataCarTypeTypesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataCarTypeTypes",
                new XElement("ID", forms.ID),
                new XElement("Description", forms.Description),
                new XElement("Seq", forms.Seq),
                new XElement("SiteID", forms.SiteID),
                new XElement("TransactionTypeID", forms.TransactionTypeID),
                new XElement("Active", forms.Active)))
                .Where(xelement => infoDataCarTypeTypesXdoc.Root != null))
            {
                infoDataCarTypeTypesXdoc.Root?.Add(xelement);
            }

            MemoryStream stream = new MemoryStream();
            infoDataCarTypeTypesXdoc.Save(stream);
            return stream;
        }
    }
}
