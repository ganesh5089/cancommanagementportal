﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataLocationValuesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataLocationValuesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataLocationValuesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataLocationValuesXmlQuery query)
        {
            var data = new List<sp_select_info_data_location_values_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_location_values(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataLocationValuesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataLocationValues",
                new XElement("ID", forms.ID),
                new XElement("Value", forms.Value),
                new XElement("SiteID", forms.SiteID),
                new XElement("Active", forms.Active)))
                .Where(xelement => infoDataLocationValuesXdoc.Root != null))
            {
                infoDataLocationValuesXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            infoDataLocationValuesXdoc.Save(stream);
            return stream;
        }
    }
}
