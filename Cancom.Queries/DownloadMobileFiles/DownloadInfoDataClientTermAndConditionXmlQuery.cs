﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataClientTermAndConditionXmlQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public DownloadInfoDataClientTermAndConditionXmlQuery(IUserProvider user, int csspmId) : base(user)
        {
            CsspmId = csspmId;
        }

        public int CsspmId { get; set; }
    }
}
