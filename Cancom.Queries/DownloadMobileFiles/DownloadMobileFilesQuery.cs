﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadMobileFilesQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public int CsspmId { get; set; }
        public string ClientName { get; set; }
        public string MobileName { get; set; }
    }
}
