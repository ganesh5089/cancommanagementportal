﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataStandNoValuesXmlQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public DownloadInfoDataStandNoValuesXmlQuery(IUserProvider user, int csspmId) : base(user)
        {
            CsspmId = csspmId;
        }

        public int CsspmId { get; set; }
    }
}
