﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataAdditionalTypesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataAdditionalTypesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataAdditionalTypesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataAdditionalTypesXmlQuery query)
        {
            var data = new List<sp_select_info_data_additional_types_portal_Result>();
            
            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_additional_types_portal(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataAdditionalTypesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataAdditionalTypes",
                new XElement("ID", forms.ID),
                new XElement("Description", forms.Description),
                new XElement("Seq", forms.Seq),
                new XElement("SiteID", forms.SiteID),
                new XElement("Active", forms.Active)))
                .Where(xelement => infoDataAdditionalTypesXdoc.Root != null))
            {
                infoDataAdditionalTypesXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            infoDataAdditionalTypesXdoc.Save(stream);
            return stream;
        }
    }
}
