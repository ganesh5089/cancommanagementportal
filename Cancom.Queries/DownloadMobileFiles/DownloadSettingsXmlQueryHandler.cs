﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using AutoMapper;
using Cancom.Common.Extensions;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadSettingsXmlQueryHandler : IAsyncQueryRequestHandler<DownloadSettingsXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadSettingsXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadSettingsXmlQuery query)
        {
            var mobileDeviceSetting = new getCsspmInfoAccordingtoCsspmId_Result();
            var adminUserSetting = new sp_select_admin_users_and_roles_new_Result();
            var version = string.Empty;

            await AsyncHelper.MakeItAsync(() =>
            {
                mobileDeviceSetting = context.getCsspmInfoAccordingtoCsspmId(query.CsspmId).FirstOrDefault();
            });

            await AsyncHelper.MakeItAsync(() =>
            {
                adminUserSetting = context.sp_select_admin_users_and_roles_new(query.CsspmId)
                    .FirstOrDefault(i => i.username.Equals("user", StringComparison.OrdinalIgnoreCase));
            });

            await AsyncHelper.MakeItAsync(() =>
            {
                version = context.new_get_allclientversionnumber().FirstOrDefault();
            });

            var generalSetting = await context.AccessGeneralSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessGeneralSetting();

            var clientSetting = await context.AccessClientSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessClientSetting();

            var enterSetting = await context.AccessEnterSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessEnterSetting();

            var exitSetting = await context.AccessExitSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessExitSetting();

            var networkSetting = await context.AccessNetworkSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessNetworkSetting();

            var formSetting = await context.AccessFormSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessFormSetting();

            var optionsSetting = await context.AccessOptionsSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessOptionsSetting();


            var result = new AccessSettingsXmlViewModel
            {
                CsspmId = query.CsspmId,
                Version = version,
                MobileDeviceSettings = Mapper.Map<MobileDeviceSettingsXmlViewModel>(mobileDeviceSetting),
                AdminUserSettings = Mapper.Map<AdminUserSettingsXmlViewModel>(adminUserSetting),
                GeneralSettings = Mapper.Map<AccessGeneralSettingsXmlViewModel>(generalSetting),
                ClientSettings = Mapper.Map<AccessClientSettingsXmlViewModel>(clientSetting),
                EnterSettings = Mapper.Map<AccessEnterSettingsXmlViewModel>(enterSetting),
                ExitSettings = Mapper.Map<AccessExitSettingsXmlViewModel>(exitSetting),
                NetworkSettings = Mapper.Map<AccessNetworkSettingsXmlViewModel>(networkSetting),
                FormSettings = Mapper.Map<AccessFormSettingsXmlViewModel>(formSetting),
                OptionsSettings = Mapper.Map<AccessOptionsSettingsXmlViewModel>(optionsSetting)
            };

            var clientXmlString = result.ToXml();
            var doc = XDocument.Parse(clientXmlString);

            var csspmId = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "CsspmId");
            var tempVersion = doc.Descendants().FirstOrDefault(p => p.Name.LocalName == "Version");

            var mobileDeviceSettings = doc.Descendants().Where(p => p.Name.LocalName == "MobileDeviceSettings");
            var adminUserSettings = doc.Descendants().Where(p => p.Name.LocalName == "AdminUserSettings");
            var clientSettings = doc.Descendants().Where(p => p.Name.LocalName == "ClientSettings");
            var generalSettings = doc.Descendants().Where(p => p.Name.LocalName == "GeneralSettings");
            var enterSettings = doc.Descendants().Where(p => p.Name.LocalName == "EnterSettings");
            var exitSettings = doc.Descendants().Where(p => p.Name.LocalName == "ExitSettings");
            var networkSettings = doc.Descendants().Where(p => p.Name.LocalName == "NetworkSettings");
            var formSettings = doc.Descendants().Where(p => p.Name.LocalName == "FormSettings");
            var optionsSettings = doc.Descendants().Where(p => p.Name.LocalName == "OptionsSettings");

            XDocument rootFormFlowXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));
            XElement settingsFormFlowXdoc = new XElement("Settings");
            rootFormFlowXdoc.Root?.Add(settingsFormFlowXdoc);
            settingsFormFlowXdoc.Add(csspmId);
            settingsFormFlowXdoc.Add(tempVersion);
            settingsFormFlowXdoc.Add(mobileDeviceSettings);
            settingsFormFlowXdoc.Add(adminUserSettings);
            settingsFormFlowXdoc.Add(clientSettings);
            settingsFormFlowXdoc.Add(generalSettings);
            settingsFormFlowXdoc.Add(enterSettings);
            settingsFormFlowXdoc.Add(exitSettings);
            settingsFormFlowXdoc.Add(networkSettings);
            settingsFormFlowXdoc.Add(formSettings);
            settingsFormFlowXdoc.Add(optionsSettings);

            MemoryStream stream = new MemoryStream();
            rootFormFlowXdoc.Save(stream);
            return stream;
        }
    }
}