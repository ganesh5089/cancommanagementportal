﻿using System.Data.Entity;
using System.Threading.Tasks;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    class HasMobileFileQueryHandler : IAsyncQueryRequestHandler<HasMobileFileQuery, bool>
    {
        private readonly IDTechAccessV2Entities context;

        public HasMobileFileQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<bool> Handle(HasMobileFileQuery query)
        {
            var generalSetting = await context.AccessGeneralSetting
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId);

            return generalSetting != null;
        }
    }
}