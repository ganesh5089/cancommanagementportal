﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataUnitNoValuesXmlQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public DownloadInfoDataUnitNoValuesXmlQuery(IUserProvider user, int csspmId) : base(user)
        {
            CsspmId = csspmId;
        }

        public int CsspmId { get; set; }
    }
}
