﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadTransactionTypesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadTransactionTypesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadTransactionTypesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadTransactionTypesXmlQuery query)
        {
            var data = new List<sp_select_transaction_types_portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_transaction_types_portal(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument transactionTypesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("TransactionTypes",
                new XElement("ID", forms.ID),
                new XElement("Description", forms.Description),
                new XElement("SiteID", forms.SiteID)))
                .Where(xelement => transactionTypesXdoc.Root != null))
            {
                transactionTypesXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            transactionTypesXdoc.Save(stream);
            return stream;
        }
    }
}
