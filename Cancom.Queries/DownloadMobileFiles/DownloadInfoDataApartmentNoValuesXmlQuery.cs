﻿using System.IO;
using Cancom.Common;
using MediatR;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataApartmentNoValuesXmlQuery : CancomMessage, IAsyncRequest<MemoryStream>
    {
        public DownloadInfoDataApartmentNoValuesXmlQuery(IUserProvider user, int csspmId) : base(user)
        {
            CsspmId = csspmId;
        }

        public int CsspmId { get; set; }
    }
}
