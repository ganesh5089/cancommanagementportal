﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataApartmentNoValuesXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataApartmentNoValuesXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataApartmentNoValuesXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataApartmentNoValuesXmlQuery query)
        {
            var data = new List<sp_select_info_data_apartmentno_values_portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_info_data_apartmentno_values_portal(true, query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataApartmentNoValuesXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataApartmentNoValues",
                new XElement("ID", forms.ID),
                new XElement("Value", forms.Value),
                new XElement("SiteID", forms.SiteID),
                new XElement("Active", forms.Active)))
                .Where(xelement => infoDataApartmentNoValuesXdoc.Root != null))
            {
                infoDataApartmentNoValuesXdoc.Root?.Add(xelement);
            }

            MemoryStream stream = new MemoryStream();
            infoDataApartmentNoValuesXdoc.Save(stream);
            return stream;
        }
    }
}
