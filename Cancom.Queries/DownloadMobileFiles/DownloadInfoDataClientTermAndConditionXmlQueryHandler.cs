﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.DownloadMobileFiles
{
    public class DownloadInfoDataClientTermAndConditionXmlQueryHandler : IAsyncQueryRequestHandler<DownloadInfoDataClientTermAndConditionXmlQuery, MemoryStream>
    {
        private readonly IDTechAccessV2Entities context;

        public DownloadInfoDataClientTermAndConditionXmlQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<MemoryStream> Handle(DownloadInfoDataClientTermAndConditionXmlQuery query)
        {
            var data = new List<admin_csspm_TermAndCondition>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.admin_csspm_TermAndCondition.AsNoTracking().Where(item => item.CSSPMID == query.CsspmId).ToList();
            });

            if (!data.Any())
                return new MemoryStream();

            XDocument infoDataTermsAndConditionsXdoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Root"));

            foreach (XElement xelement in data.Select(forms => new XElement("InfoDataTermsAndConditions",
                new XElement("Value", forms.TermAndCondition)))
                .Where(xelement => infoDataTermsAndConditionsXdoc.Root != null))
            {
                infoDataTermsAndConditionsXdoc.Root?.Add(xelement);
            }

            var stream = new MemoryStream();
            infoDataTermsAndConditionsXdoc.Save(stream);
            return stream;
        }
    }
}
