﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.TransactionType
{
    public class TransactionTypeDetailsQueryHandler : IAsyncQueryRequestHandler<TransactionTypeDetailsQuery, TransactionTypeViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public TransactionTypeDetailsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<TransactionTypeViewModel> Handle(TransactionTypeDetailsQuery command)
        {
            var data = new sp_select_transaction_types_by_transtypeID_Result();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_transaction_types_by_transtypeID(command.Id).FirstOrDefault();
            });
            
            return Mapper.Map<TransactionTypeViewModel>(data);
        }
    }
}
