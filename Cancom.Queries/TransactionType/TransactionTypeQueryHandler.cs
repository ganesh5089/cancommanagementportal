﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.TransactionType
{
    public class TransactionTypeQueryHandler : IAsyncQueryRequestHandler<TransactionTypeQuery, IEnumerable<TransactionTypeViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public TransactionTypeQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<TransactionTypeViewModel>> Handle(TransactionTypeQuery message)
        {
            var data = new List<sp_select_transaction_types_view_portal_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_transaction_types_view_portal(true).ToList();
            });
            
            return data.Select(Mapper.Map<TransactionTypeViewModel>);
        }
    }
}
