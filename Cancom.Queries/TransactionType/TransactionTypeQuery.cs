﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.TransactionType
{
    public class TransactionTypeQuery : CancomMessage, IAsyncRequest<IEnumerable<TransactionTypeViewModel>>
    {
        public TransactionTypeQuery(IUserProvider user) : base(user)
        {

        }
    }
}
