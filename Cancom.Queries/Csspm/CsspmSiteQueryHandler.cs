﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Csspm
{
    public class CsspmSiteQueryHandler : IAsyncQueryRequestHandler<CsspmSiteQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CsspmSiteQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CsspmSiteQuery query)
        {
            var data = new List<sp_select_csspm_site_by_clientid_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_site_by_clientid(query.Id).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
