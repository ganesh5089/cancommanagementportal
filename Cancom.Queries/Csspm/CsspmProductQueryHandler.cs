﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Csspm
{
    public class CsspmProductQueryHandler : IAsyncQueryRequestHandler<CsspmProductQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CsspmProductQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CsspmProductQuery query)
        {
            var data = new List<sp_select_csspm_product_by_cid_sid_stationid_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_product_by_cid_sid_stationid(query.ClientId, query.SiteId, query.StationId).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
