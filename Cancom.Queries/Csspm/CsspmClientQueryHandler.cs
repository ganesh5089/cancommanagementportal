﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.Csspm
{
    public class CsspmClientQueryHandler : IAsyncQueryRequestHandler<CsspmClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CsspmClientQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CsspmClientQuery message)
        {
            var data = new List<sp_select_csspm_clients_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_clients(true).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
