﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Csspm
{
    public class CsspmSiteQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public CsspmSiteQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
