﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.Csspm
{
    public class CsspmStationQueryHandler : IAsyncQueryRequestHandler<CsspmStationQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public CsspmStationQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(CsspmStationQuery query)
        {
            var data = new List<sp_select_csspm_station_by_clientid_siteid_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_csspm_station_by_clientid_siteid(query.ClientId, query.SiteId).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
