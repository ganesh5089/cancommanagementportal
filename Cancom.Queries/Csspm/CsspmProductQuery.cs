﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Csspm
{
    public class CsspmProductQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public int ClientId { get; set; }
        public int SiteId { get; set; }
        public int StationId { get; set; }
    }
}
