﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Csspm
{
    public class CsspmClientQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public CsspmClientQuery(IUserProvider user) : base(user)
        {
        }
    }
}
