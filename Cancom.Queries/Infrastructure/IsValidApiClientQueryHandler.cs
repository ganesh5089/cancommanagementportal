﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Data;

namespace Cancom.Queries.Infrastructure
{
    public class IsValidApiClientQueryHandler : IAsyncQueryRequestHandler<IsValidApiClientQuery, bool>
    {
        private readonly IDataCache cache;

        public IsValidApiClientQueryHandler(IDataCache cache)
        {
            this.cache = cache;
        }

        public async Task<bool> Handle(IsValidApiClientQuery message)
        {
            //TODO if we going to implement with api client
            //if (message.ApiClientId == Guid.Empty) 
            //        return false;

            //var clients = await cache.ApiClients();
            //var client = clients.SingleOrDefault(c => c.id == message.ApiClientId);

            //return (client != null && client.IsValid);

            return true;
        }
    }
}
