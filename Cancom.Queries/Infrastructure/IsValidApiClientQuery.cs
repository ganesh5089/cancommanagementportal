﻿using System;
using MediatR;

namespace Cancom.Queries.Infrastructure
{
    public class IsValidApiClientQuery : IAsyncRequest<bool>
    {
        public IsValidApiClientQuery(Guid apiClientId)
        {
            ApiClientId = apiClientId;
        }

        public Guid ApiClientId { get; set; }
    }
}
