﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.ManageMobileSettings
{
    public class ManageMobileSettingsCsspmQuery : CancomMessage, IAsyncRequest<IEnumerable<ManageMobileSettingsCsspmViewModel>>
    {
        public ManageMobileSettingsCsspmQuery(IUserProvider user) : base(user)
        {
        }
    }
}
