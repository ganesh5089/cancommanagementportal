﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.Queries.AccessClient;
using Cancom.Queries.DownloadMobileFiles;
using Cancom.ViewModel;
using MediatR;
using NExtensions;

namespace Cancom.Queries.ManageMobileSettings
{
    public class ManageMobileSettingsCsspmQueryHandler : IAsyncQueryRequestHandler<ManageMobileSettingsCsspmQuery, IEnumerable<ManageMobileSettingsCsspmViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        private readonly IMediator mediatr;

        public ManageMobileSettingsCsspmQueryHandler(IDTechAccessV2Entities context, IMediator mediatr)
        {
            this.context = context;
            this.mediatr = mediatr;
        }

        public async Task<IEnumerable<ManageMobileSettingsCsspmViewModel>> Handle(ManageMobileSettingsCsspmQuery query)
        {
            var data = new List<sp_select_admin_csspm_view_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_admin_csspm_view(true).ToList();
            });

            var result = data.Select(Mapper.Map<ManageMobileSettingsCsspmViewModel>).ToList();

            foreach (var item in result)
            {
                item.IsDownloadMobileFile = await mediatr.SendAsync(new HasMobileFileQuery(query.User, item.Id));
            }

            return result;
        }
    }
}
