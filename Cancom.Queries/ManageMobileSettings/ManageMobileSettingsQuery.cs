﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.ManageMobileSettings
{
    public class ManageMobileSettingsQuery : CancomMessage, IAsyncRequest<AccessSettingsViewModel>
    {
        public int CsspmId { get; set; }

        public ManageMobileSettingsQuery(int csspmId)
        {
            CsspmId = csspmId;
        }
    }
}
