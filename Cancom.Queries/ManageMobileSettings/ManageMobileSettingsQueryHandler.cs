﻿using System.Data.Entity;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.ManageMobileSettings
{
    public class ManageMobileSettingsQueryHandler : IAsyncQueryRequestHandler<ManageMobileSettingsQuery, AccessSettingsViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public ManageMobileSettingsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<AccessSettingsViewModel> Handle(ManageMobileSettingsQuery query)
        {
            var result = new AccessSettingsViewModel();

            if (query.CsspmId == 0)
                return result;

            result.CsspmId = query.CsspmId;

            var generalSetting = await context.AccessGeneralSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessGeneralSetting();
            var clientSetting = await context.AccessClientSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessClientSetting();
            var enterSetting = await context.AccessEnterSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessEnterSetting();
            var exitSetting = await context.AccessExitSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessExitSetting();
            var networkSetting = await context.AccessNetworkSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessNetworkSetting();
            var formSetting = await context.AccessFormSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessFormSetting();
            var optionsSetting = await context.AccessOptionsSetting.FirstOrDefaultAsync(i => i.CsspmId == query.CsspmId) ?? new AccessOptionsSetting();

            result.GeneralSettings = Mapper.Map<AccessGeneralSettingsViewModel>(generalSetting);
            result.ClientSettings = Mapper.Map<AccessClientSettingsViewModel>(clientSetting);
            result.EnterSettings = Mapper.Map<AccessEnterSettingsViewModel>(enterSetting);
            result.ExitSettings = Mapper.Map<AccessExitSettingsViewModel>(exitSetting);
            result.NetworkSettings = Mapper.Map<AccessNetworkSettingsViewModel>(networkSetting);
            result.FormSettings = Mapper.Map<AccessFormSettingsViewModel>(formSetting);
            result.OptionsSettings = Mapper.Map<AccessOptionsSettingsViewModel>(optionsSetting);

            return result;
        }
    }
}