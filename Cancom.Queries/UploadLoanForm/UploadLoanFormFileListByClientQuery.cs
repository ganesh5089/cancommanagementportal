﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.UploadLoanForm
{
    public class UploadLoanFormFileListByClientQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public UploadLoanFormFileListByClientQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
