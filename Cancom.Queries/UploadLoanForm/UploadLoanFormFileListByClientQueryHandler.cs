﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;
using System.Data.Entity;

namespace Cancom.Queries.UploadLoanForm
{
    public class UploadLoanFormFileListByClientQueryHandler : IAsyncQueryRequestHandler<UploadLoanFormFileListByClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public UploadLoanFormFileListByClientQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(UploadLoanFormFileListByClientQuery query)
        {
            var data = await context.loanforms.Where(item => item.ClientID == query.Id).AsNoTracking().OrderBy(item => item.LoanFormName).ToListAsync();
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
