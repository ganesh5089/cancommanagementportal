﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;
using System.Data.Entity;

namespace Cancom.Queries.UploadLoanForm
{
    public class UploadLoanFormUserListByClientQueryHandler : IAsyncQueryRequestHandler<UploadLoanFormUserListByClientQuery, IEnumerable<UploadLoanFormViewModel>>
    {
        private readonly loginDBDataContext context;

        public UploadLoanFormUserListByClientQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<UploadLoanFormViewModel>> Handle(UploadLoanFormUserListByClientQuery query)
        {
            var data = await context.Users.Where(item => item.ClientID == query.Id && item.isActive).AsNoTracking().OrderBy(item => item.UserID).ToListAsync();
            return data.Select(Mapper.Map<UploadLoanFormViewModel>);
        }
    }
}
