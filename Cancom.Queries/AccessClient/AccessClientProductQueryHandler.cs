﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.ViewModel;
using AutoMapper;
using Cancom.Data.IDTechAccessV2;

namespace Cancom.Queries.AccessClient
{
    public class AccessClientProductQueryHandler : IAsyncQueryRequestHandler<AccessClientProductQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessClientProductQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(AccessClientProductQuery message)
        {
            var data = await context.admin_products
                .OrderBy(i => i.ProductName)
                .AsNoTracking()
                .ToListAsync(); 
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
