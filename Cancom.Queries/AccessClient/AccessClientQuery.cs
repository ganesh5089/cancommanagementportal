﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.AccessClient
{
    public class AccessClientQuery : CancomMessage, IAsyncRequest<IEnumerable<AccessClientViewModel>>
    {
        public AccessClientQuery(IUserProvider user) : base(user)
        {
        }
    }
}
