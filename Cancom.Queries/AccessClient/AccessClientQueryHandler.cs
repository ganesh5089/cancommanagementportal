﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using AutoMapper;
using System.Threading.Tasks;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.AccessClient
{
    public class AccessClientQueryHandler : IAsyncQueryRequestHandler<AccessClientQuery, IEnumerable<AccessClientViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessClientQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<AccessClientViewModel>> Handle(AccessClientQuery message)
        {
            var data = new List<sp_select_admin_csspm_view_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_admin_csspm_view(true).ToList();
            });

            return data.Select(Mapper.Map<AccessClientViewModel>);
        }
    }
}
