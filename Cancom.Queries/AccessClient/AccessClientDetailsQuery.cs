﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.AccessClient
{
    public class AccessClientDetailsQuery : CancomMessage, IAsyncRequest<AccessClientViewModel>
    {
        public AccessClientDetailsQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}
