﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.AccessClient
{
    public class AccessClientDetailsQueryHandler : IAsyncQueryRequestHandler<AccessClientDetailsQuery, AccessClientViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessClientDetailsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<AccessClientViewModel> Handle(AccessClientDetailsQuery command)
        {
            var data = new sp_select_admin_csspm_view_csspmid_Result();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_select_admin_csspm_view_csspmid(true, command.Id).FirstOrDefault();
            });

            return Mapper.Map<AccessClientViewModel>(data);
        }
    }
}
