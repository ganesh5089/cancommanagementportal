﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;


namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsClientQuery : CancomMessage, IAsyncRequest<IEnumerable<LookupViewModel>>
    {
        public AccessV2TransactionsClientQuery(IUserProvider user) : base(user)
        {
        }
    }
}
