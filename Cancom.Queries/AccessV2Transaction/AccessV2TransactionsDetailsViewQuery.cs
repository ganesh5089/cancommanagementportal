﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsDetailsViewQuery : CancomMessage, IAsyncRequest<AccessV2TransactionsDetailsViewModel>
    {
        public int Id { get; set; }
        public string RegNo { get; set; }
        public string DriverId { get; set; }
    }
}
