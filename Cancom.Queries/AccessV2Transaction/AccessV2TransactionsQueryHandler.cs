﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Extensions;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsQueryHandler : IAsyncQueryRequestHandler<AccessV2TransactionsQuery, IEnumerable<AccessV2TransactionsViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessV2TransactionsQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
            this.context.Database.CommandTimeout = 0;
        }

        public async Task<IEnumerable<AccessV2TransactionsViewModel>> Handle(AccessV2TransactionsQuery message)
        {
            if (string.IsNullOrEmpty(message.DriverId) && string.IsNullOrEmpty(message.Surname) && string.IsNullOrEmpty(message.VehicleRegistration))
            {
                if (message.FromDate != null && message.ToDate != null)
                {
                    message.ToDate = message.ToDate.EndOfDay();

                    var data = new List<sp_web_select_transactions_views_date_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_views_date_portal(true, true, true, message.FromDate, message.ToDate, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
                else
                {
                    var data = new List<sp_web_select_transactions_views_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_views_portal(true, true, true, message.ClientId).ToList();
                    });

                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
            }
            if (!string.IsNullOrEmpty(message.DriverId))
            {
                if (message.FromDate != null && message.ToDate != null)
                {
                    message.ToDate = message.ToDate.EndOfDay();

                    var data = new List<sp_web_select_transactions_view_idnumber_date_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_view_idnumber_date_portal(true, message.DriverId, true, true, message.FromDate, message.ToDate, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
                else
                {
                    var data = new List<sp_web_select_transactions_view_idnumber_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_view_idnumber_portal(true, message.DriverId, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
            }
            if (!string.IsNullOrEmpty(message.Surname))
            {
                if (message.FromDate != null && message.ToDate != null)
                {
                    message.ToDate = message.ToDate.EndOfDay();

                    var data = new List<sp_web_select_transactions_view_surname_date_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_view_surname_date_portal(true, message.Surname, true, true, message.FromDate, message.ToDate, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
                else
                {
                    var data = new List<sp_web_select_transactions_surname_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_surname_portal(true, message.Surname, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }

            }
            if (!string.IsNullOrEmpty(message.VehicleRegistration))
            {
                if (message.FromDate != null && message.ToDate != null)
                {
                    message.ToDate = message.ToDate.EndOfDay();

                    var data = new List<sp_web_select_transactions_view_registration_date_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_view_registration_date_portal(true, message.VehicleRegistration, true, true, message.FromDate, message.ToDate, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
                else
                {
                    var data = new List<sp_web_select_transactions_view_registration_portal_Result>();

                    await AsyncHelper.MakeItAsync(() =>
                    {
                        data = context.sp_web_select_transactions_view_registration_portal(true, message.VehicleRegistration, true, true, message.ClientId).ToList();
                    });
                    
                    return data.Select(Mapper.Map<AccessV2TransactionsViewModel>);
                }
            }
            return new List<AccessV2TransactionsViewModel>();
        }
    }
}
