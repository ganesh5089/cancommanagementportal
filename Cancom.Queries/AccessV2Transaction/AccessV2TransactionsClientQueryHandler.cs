﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsClientQueryHandler : IAsyncQueryRequestHandler<AccessV2TransactionsClientQuery, IEnumerable<LookupViewModel>>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessV2TransactionsClientQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<LookupViewModel>> Handle(AccessV2TransactionsClientQuery message)
        {
            var data = new List<sp_web_list_clientsite_webclientID_Result>();

            await AsyncHelper.MakeItAsync(() =>
            {
                data = context.sp_web_list_clientsite_webclientID().OrderBy(i => i.ClientSite).ToList();
            });
            
            return data.Select(Mapper.Map<LookupViewModel>);
        }
    }
}
