﻿using System;
using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsQuery : CancomMessage, IAsyncRequest<IEnumerable<AccessV2TransactionsViewModel>>
    {
        public int ClientId { get; set; }
        public string DriverId { get; set; }
        public string Surname { get; set; }
        public string VehicleRegistration { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
