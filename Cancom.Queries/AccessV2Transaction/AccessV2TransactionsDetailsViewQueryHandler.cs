﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Common.Helpers;
using Cancom.Data.IDTechAccessV2;
using Cancom.ViewModel;

namespace Cancom.Queries.AccessV2Transaction
{
    public class AccessV2TransactionsDetailsViewQueryHandler : IAsyncQueryRequestHandler<AccessV2TransactionsDetailsViewQuery, AccessV2TransactionsDetailsViewModel>
    {
        private readonly IDTechAccessV2Entities context;

        public AccessV2TransactionsDetailsViewQueryHandler(IDTechAccessV2Entities context)
        {
            this.context = context;
        }

        public async Task<AccessV2TransactionsDetailsViewModel> Handle(AccessV2TransactionsDetailsViewQuery query)
        {
            AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel = new AccessV2TransactionsDetailsViewModel();

            var transaction = context.transactions
                .AsNoTracking()
                .FirstOrDefault(x => x.ID == query.Id);

            if (transaction != null)
                accessV2TransactionsDetailsViewModel.TransactionStatus = transaction.Signature != null ? "Complete" : "Cancelled";

            await GetDriverLicenseDetail(query, accessV2TransactionsDetailsViewModel);
            await GetVehicleLicenseDetail(query, accessV2TransactionsDetailsViewModel);
            await GetAdditionalInfo(query, accessV2TransactionsDetailsViewModel);

            return accessV2TransactionsDetailsViewModel;
        }

        private async Task GetDriverLicenseDetail(AccessV2TransactionsDetailsViewQuery query, AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel)
        {
            if (!string.IsNullOrEmpty(query.DriverId))
            {
                sp_web_getDriverLicence_Result data = null;
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.sp_web_getDriverLicence(query.DriverId).FirstOrDefault();
                });


                accessV2TransactionsDetailsViewModel.DriverLicense = new AccessV2TransactionDriverLicenseViewModel();
                if (data != null)
                {
                    accessV2TransactionsDetailsViewModel.DriverLicense.DriverName = data.p_Surname;
                    accessV2TransactionsDetailsViewModel.DriverLicense.IdNumber = data.id_Number;
                    accessV2TransactionsDetailsViewModel.DriverLicense.Dob = data.p_DateOfBirth;
                    accessV2TransactionsDetailsViewModel.DriverLicense.Gender = data.p_Gender;
                    accessV2TransactionsDetailsViewModel.DriverLicense.CertificateNo = data.CertificateNo;
                    accessV2TransactionsDetailsViewModel.DriverLicense.DateValidTill = data.c_DateValidTo;

                    if (data.ph_Image != null)
                    {
                        string base64String = Convert.ToBase64String(data.ph_Image, 0, data.ph_Image.Length);
                        accessV2TransactionsDetailsViewModel.DriverLicense.Image = "data:image/jpg;base64," + base64String;
                    }
                }
                else
                {
                    LoadEmptyValuesForDriverLicense(accessV2TransactionsDetailsViewModel);
                }
            }
            else
            {
                accessV2TransactionsDetailsViewModel.DriverLicense = LoadEmptyValuesForDriverLicense(accessV2TransactionsDetailsViewModel);
            }
        }

        private async Task GetVehicleLicenseDetail(AccessV2TransactionsDetailsViewQuery query, AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel)
        {
            if (!string.IsNullOrEmpty(query.DriverId))
            {
                sp_webGetVehicleLicense_Result data = null;
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.sp_webGetVehicleLicense(query.RegNo).FirstOrDefault();
                });

                accessV2TransactionsDetailsViewModel.VehicleLicense = data != null
                    ? Mapper.Map<AccessV2TransactionVehicleLicenseViewModel>(data)
                    : LoadEmptyValuesForVehicleLicense(accessV2TransactionsDetailsViewModel);
            }
            else
            {
                accessV2TransactionsDetailsViewModel.VehicleLicense = LoadEmptyValuesForVehicleLicense(accessV2TransactionsDetailsViewModel);
            }
        }

        private async Task GetAdditionalInfo(AccessV2TransactionsDetailsViewQuery query, AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel)
        {
            if (query.Id != 0)
            {
                List<sp_select_transactions_information_Result> data = null;
                await AsyncHelper.MakeItAsync(() =>
                {
                    data = context.sp_select_transactions_information(true, query.Id, 0).ToList();
                });

                accessV2TransactionsDetailsViewModel.AdditionalInfo = data != null
                    ? data.Select(Mapper.Map<AccessV2TransactionAdditionalInfoViewModel>)
                    : new List<AccessV2TransactionAdditionalInfoViewModel>();
            }
            else
            {
                accessV2TransactionsDetailsViewModel.AdditionalInfo = new List<AccessV2TransactionAdditionalInfoViewModel>();
            }
        }

        private static AccessV2TransactionDriverLicenseViewModel LoadEmptyValuesForDriverLicense(AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel)
        {
            accessV2TransactionsDetailsViewModel.DriverLicense = new AccessV2TransactionDriverLicenseViewModel
            {
                DriverName = "N/A",
                IdNumber = "N/A",
                Dob = "N/A",
                Gender = "N/A",
                CertificateNo = "N/A",
                DateValidTill = "N/A"
            };
            return accessV2TransactionsDetailsViewModel.DriverLicense;
        }

        private static AccessV2TransactionVehicleLicenseViewModel LoadEmptyValuesForVehicleLicense(AccessV2TransactionsDetailsViewModel accessV2TransactionsDetailsViewModel)
        {
            accessV2TransactionsDetailsViewModel.VehicleLicense = new AccessV2TransactionVehicleLicenseViewModel
            {
                RegistrationNo = "N/A",
                VehicleRegistrationNo = "N/A",
                Make = "N/A",
                Model = "N/A",
                Colour = "N/A",
                ExpiryDate = "N/A",
                VinNo = "N/A",
                EngineNo = "N/A",
                LicenseNo = "N/A"
            };
            return accessV2TransactionsDetailsViewModel.VehicleLicense;
        }
    }
}
