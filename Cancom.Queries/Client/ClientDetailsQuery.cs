﻿using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Client
{
    public class ClientDetailsQuery : CancomMessage, IAsyncRequest<ClientDetailsViewModel>
    {
        public ClientDetailsQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }

    }
}
