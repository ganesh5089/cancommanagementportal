﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.CFMSv3;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.Client
{
    public class ClientDetailsQueryHandler : IAsyncQueryRequestHandler<ClientDetailsQuery, ClientDetailsViewModel>
    {
        private readonly CFMSv3DataContext context;
        private readonly loginDBDataContext loginDbContext;

        public ClientDetailsQueryHandler(CFMSv3DataContext context, loginDBDataContext loginDbContext)
        {
            this.context = context;
            this.loginDbContext = loginDbContext;
        }

        public async Task<ClientDetailsViewModel> Handle(ClientDetailsQuery query)
        {
            var data = new ClientDetailsViewModel();
            var client = await context.clients.FirstOrDefaultAsync(i => i.id == query.Id);
            if (client != null)
            {
                data = new ClientDetailsViewModel
                {
                    ClientId = client.id,
                    ClientName = client.client_name,
                    Products = await GetProducts(client.id)
                };
            }
            return data;
        }

        public async Task<List<ProductViewModel>> GetProducts(int id)
        {
            var productList = new List<ProductViewModel>();
            var productIds = await loginDbContext.ClientProducts.AsNoTracking()
                                    .Where(item => item.ClientID == id)
                                    .Select(item => item.ProductID)
                                    .ToListAsync();
            if (productIds != null && productIds.Any())
            {
                var allProducts = await loginDbContext.Products.OrderBy(i => i.Name).AsNoTracking().ToListAsync();

                foreach (var product in productIds)
                {
                    var productItem = allProducts.FirstOrDefault(item => item.ProductID == product);
                    productList.Add(Mapper.Map<ProductViewModel>(productItem));
                }
                //productList.AddRange(from product in productIds
                //    select allProducts.FirstOrDefault(item => item.ProductID == product)
                //    into productItem
                //    where productItem != null
                //    select productItem.Name);
            }
            return productList;
        }
    }
}
