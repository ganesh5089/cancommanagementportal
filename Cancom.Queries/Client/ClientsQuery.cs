﻿using System.Collections.Generic;
using Cancom.Common;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.Queries.Client
{
    public class ClientsQuery : CancomMessage, IAsyncRequest<IEnumerable<ClientsViewModel>>
    {
        public ClientsQuery(IUserProvider user) : base(user)
        {
        }
    }
}
