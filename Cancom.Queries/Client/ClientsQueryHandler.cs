﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;

namespace Cancom.Queries.Client
{
    public class ClientsQueryHandler : IAsyncQueryRequestHandler<ClientsQuery, IEnumerable<ClientsViewModel>>
    {
        private readonly loginDBDataContext context;

        public ClientsQueryHandler(loginDBDataContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<ClientsViewModel>> Handle(ClientsQuery message)
        {
            var data = await context.Clients
                .OrderBy(i => i.Name)
                .AsNoTracking()
                .ToListAsync();

            return data.Select(Mapper.Map<ClientsViewModel>);
        }
    }
}
