﻿using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using NExtensions;

namespace Cancom.Common.Extensions
{
    public static class StringExtensions
    {
        static readonly Regex EmailValidationRegEx = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));

        public static bool IsValidEmailAddress(this string input)
        {
            return input.HasValue() && EmailValidationRegEx.IsMatch(input.Trim());
        }

        public static bool IsValidateIPv4(this string input)
        {
            if(!input.HasValue())
                return false;

            if (input.Count(c => c == '.') != 3)
                return false;

            IPAddress address;
            return IPAddress.TryParse(input, out address);
        }

        public static bool IsValidUrl(this string url)
        {
            if (string.IsNullOrEmpty(url))
                return false;

            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                url = "http://" + url;

            Uri uriResult;

            return Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                   && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps)
                   && uriResult.Host.Replace("www.", "").Split('.').Length > 1
                   && uriResult.HostNameType == UriHostNameType.Dns;
        }

        public static string ToHash(this string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }

    }
}
