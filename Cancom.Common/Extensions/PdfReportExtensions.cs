﻿using System.IO;
using NExtensions;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;

namespace Cancom.Common.Extensions
{
    public static class PdfReportExtensions
    {
        public static byte[] EncryptPdf(this byte[] report, bool hasPassword = false, string userPassword = null)
        {
            MemoryStream output;
            using (Stream input = new MemoryStream(report))
            {
                using (output = new MemoryStream())
                {
                    PdfDocument document = PdfReader.Open(input);

                    PdfSecuritySettings securitySettings = document.SecuritySettings;

                    // Setting one of the passwords automatically sets the security level to
                    // PdfDocumentSecurityLevel.Encrypted128Bit.
                    securitySettings.OwnerPassword = "a0b4adb7-22a2-402b-80e4-cab2ac5666a0";

                    if (hasPassword && userPassword.HasValue())
                        securitySettings.UserPassword = userPassword;

                    // Don't use 40 bit encryption unless needed for compatibility
                    securitySettings.DocumentSecurityLevel = PdfDocumentSecurityLevel.Encrypted128Bit;

                    // Restrict some rights.
                    securitySettings.PermitPrint = true;
                    securitySettings.PermitExtractContent = true;
                    securitySettings.PermitAccessibilityExtractContent = false;
                    securitySettings.PermitAnnotations = false;
                    securitySettings.PermitFormsFill = false;
                    securitySettings.PermitModifyDocument = false;

                    // Save the document...
                    document.Save(output);
                }
            }
            return output.GetBuffer();
        }
    }
}
