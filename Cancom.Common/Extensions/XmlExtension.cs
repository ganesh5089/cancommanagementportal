﻿using System.IO;
using System.Xml.Serialization;

namespace Cancom.Common.Extensions
{
    public static class XmlExtension
    {
        public static string ToXml<T>(this T dataToSerialize)
        {
            var stringwriter = new StringWriter();
            var serializer = new XmlSerializer(typeof (T));
            serializer.Serialize(stringwriter, dataToSerialize);
            return stringwriter.ToString();
        }
    }
}
