﻿using System;

namespace Cancom.Common.Extensions
{
    public static class DateExtensions
    {
        public static DateTime? EndOfDay(this DateTime? dateTime)
        {
            return dateTime?.AddHours(23).AddMinutes(59).AddSeconds(59);
        }
    }
}
