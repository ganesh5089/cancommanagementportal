﻿namespace Cancom.Common
{
    public class Constant
    {
        public const string FormFlowXmlName = "formflow.xml";
        public const string SettingsXmlName = "settings.xml";
        public const string InfoDataAdditionalTypesXmlName = "infodataadditionaltypes.xml";
        public const string InfoDataAdditionalValuesXmlName = "infodataadditionalvalues.xml";
        public const string InfoDataApartmentNoValuesXmlName = "infodataapartmentnovalues.xml";
        public const string InfoDataCarTypeValuesXmlName = "infodatacartypevalues.xml";
        public const string InfoDataDepotValuesXmlName = "infodatadepotvalues.xml";
        public const string InfoDataStandNoValuesXmlName = "infodatastandnovalues.xml";
        public const string InfoDataUnitNoValuesXmlName = "infodataunitnovalues.xml";
        public const string TransactionTypesXmlName = "transactiontypes.xml";
        public const string InfoDataLaneValuesXmlName = "infodatalanevalues.xml";
        public const string InfoDataReasonValuesXmlName = "infodatareasonvalues.xml";
        public const string InfoDataReason2ValuesXmlName = "infodatareason2values.xml";
        public const string InfoDataLocationValuesXmlName = "infodatalocationvalues.xml";
        public const string InfoDataTermsAndConditionXmlName = "infodatatermsandcondition.xml";
        public const string InfoDataSectorValuesXmlName = "infodatasectorvalues.xml";
    }
}
