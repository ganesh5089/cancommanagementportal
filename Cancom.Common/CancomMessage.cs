﻿using System;

namespace Cancom.Common
{
    public class CancomMessage
    {
        public CancomMessage() { }

        public CancomMessage(CancomMessage parentMessage) : this(parentMessage.User)
        {
            MessageId = parentMessage.MessageId;
        }
        public CancomMessage(IUserProvider user)
        {
            SetUser(user);
        }

        public IUserProvider User { get; private set; }
      
        public int LoggedOnUserId { get; set; }
       
        public Guid MessageId { get; set; } = Guid.NewGuid();
         
        public DateTime TimeStamp { get; set; } = DateTime.UtcNow;
      
        public ICommandResult CommandResult { get; set; }
         
        public bool AuditThisMessage { get; private set; } = false;

        public void SetUser(IUserProvider user)
        {
            User = user;
            LoggedOnUserId = User?.UserId ?? 0;
        }

        public void DontAuditThisMessage()
        {
            AuditThisMessage = false;
        }
    }
}
