﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Cancom.Common
{
    public interface IUserProvider
    {
        int UserId { get; }
        string UserName { get; }
        string FullName { get; }
        void SetClaimsIdentity(IEnumerable<Claim> claims);
        IEnumerable<Claim> GetClaims();
        TimeZoneInfo TimeZoneInstance { get; }
        DateTime DisplayUserTimeFromUtc(DateTime? utcTime = null);
        DateTime SaveUserTimeAsUtc(DateTime localTime);
        DateTime GetUserTimeAsUtc(DateTime localTime);
    }
}
