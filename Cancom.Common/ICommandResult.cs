using System;
using System.Collections.Generic;

namespace Cancom.Common
{
    public interface ICommandResult
    {
        bool IsSuccess { get; }
        bool IsFailure { get; }
        IEnumerable<string> Failures { get; }
        string HtmlFormattedFailures { get; }

        Exception Exception { get; set; }
        bool HasException { get; }

        void SetFailures(IEnumerable<string> failures);
    }
}