﻿using System.ComponentModel;

namespace Cancom.Common.Enums
{
    public enum UserPermission : long
    {
        [Description ("System Lockout")]
        None = 0,

        [Description("Upload Loan form")]
        CanUploadLoanForm = 1,
        
    }
}
