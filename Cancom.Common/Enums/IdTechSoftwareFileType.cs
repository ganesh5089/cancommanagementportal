﻿using System.ComponentModel;

namespace Cancom.Common.Enums
{
    public enum IdTechSoftwareFileType : long
    {
        [Description("SAB")]
        Sab = 1,

        [Description("All Client")]
        AllClient = 2,
    }
}
