﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cancom.Common.Helpers
{
    public class AsyncHelper
    {
        public static async Task MakeItAsync(Action actionMethod)
        {
            var task = new Task(actionMethod.Invoke);
            task.Start();
            await task;
        }
    }
}
