﻿using System;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;
using Microsoft.AspNet.Identity;

namespace Cancom.WebApi.Infrastructure
{
    public class AuthUserStore : IUserStore<AuthUser, int>
    {
        private readonly loginDBDataContext dbContext;

        public AuthUserStore()
        {
            dbContext = new loginDBDataContext();
        }

        public void Dispose() { }

        public Task CreateAsync(AuthUser user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(AuthUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(AuthUser user)
        {
            throw new NotImplementedException();
        }

        public Task<AuthUser> FindByIdAsync(int userId)
        {
            throw new NotImplementedException();
        }

        public Task<AuthUser> FindByNameAsync(string userName)
        {
            throw new NotImplementedException();
        }
    }
}