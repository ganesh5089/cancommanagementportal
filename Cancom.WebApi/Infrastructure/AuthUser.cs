﻿using System.Linq;
using Cancom.Common.Enums;
using Microsoft.AspNet.Identity;
using NExtensions;

namespace Cancom.WebApi.Infrastructure
{
    public class AuthUser : IUser<int>
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PermissionSet { get; set; }
        public string TimeZone { get; set; }

        public bool HasPermissions(UserPermission userPermission)
        {
            return PermissionSet.SplitByComma().Select(long.Parse).Contains((long)userPermission);
        }

    }
}