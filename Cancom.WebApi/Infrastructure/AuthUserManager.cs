﻿using Microsoft.AspNet.Identity;

namespace Cancom.WebApi.Infrastructure
{
    public class AuthUserManager : UserManager<AuthUser, int>
    {
        public AuthUserManager() : this(new AuthUserStore()) { }
        public AuthUserManager(IUserStore<AuthUser, int> store) : base(store) { }

        public static AuthUserManager Create()
        {
            return new AuthUserManager();
        }

    }

}