﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Cancom.Common;

namespace Cancom.WebApi.Infrastructure
{
    public class CancomCommandHydratorAttribute : ActionFilterAttribute
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var user = actionContext.GetService<IUserProvider>();

            if (user == null)
                return;

            foreach (var cmd in actionContext.ActionArguments.Select(arg => arg.Value as CancomMessage))
            {
                cmd?.SetUser(user);
            }
        }
    }
}