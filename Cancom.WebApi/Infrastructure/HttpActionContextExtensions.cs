﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using NExtensions;

namespace Cancom.WebApi.Infrastructure
{
    public static class HttpActionContextExtensions
    {
        //because FilterAttributes are singletons in WebApi, when using AutoFac the easiest way to resolve per-request dependencies is to user the service locator pattern
        public static T GetService<T>(this HttpActionContext actionContext)
        {
            // Get the request lifetime scope so you can resolve services.
            var requestScope = actionContext.Request.GetDependencyScope();

            // Resolve the service you want to use.
            var service = requestScope.GetService(typeof(T));

            if (service == null) throw new Exception("Can not resolve {0} in RequiredPermissionsAttribute".FormatWith(typeof(T).Name));

            return (T)service;
        }

    }
}