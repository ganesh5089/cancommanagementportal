﻿using System.Configuration;

namespace Cancom.WebApi.Infrastructure
{
    public static class AppSettings
    {
        public static string OAuthAudienceId => ConfigurationManager.AppSettings["as:AudienceId"];
        public static string OAuthAudienceSecret => ConfigurationManager.AppSettings["as:AudienceSecret"];
        public static int OAuthAccessTokenExpirationInHours => int.Parse(ConfigurationManager.AppSettings["oauth:AccessTokenExpirationInHours"]);
        public static string OAuthTokenEndpoint => ConfigurationManager.AppSettings["oauth:TokenEndpoint"];
        public static string JwtIssuer => ConfigurationManager.AppSettings["jwt:Issuer"];
        public static int OAutRefreshTokenExpirationInMinutes => int.Parse(ConfigurationManager.AppSettings["oauth:RefreshTokenExpirationInMinutes"]);
    }
}