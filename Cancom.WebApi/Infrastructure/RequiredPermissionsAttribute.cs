﻿using System;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Cancom.Common;
using Cancom.Common.Enums;
using Cancom.Queries.Infrastructure;
using Cancom.WebApi.Providers;
using MediatR;
using NExtensions;

namespace Cancom.WebApi.Infrastructure
{
    public class RequiredPermissionsAttribute : AuthorizationFilterAttribute
    {
        public long[] RequiredPermissions { get; set; }

        public RequiredPermissionsAttribute(params UserPermission[] requiredPermissions)
        {
            RequiredPermissions = requiredPermissions.Cast<long>().ToArray();
        }

        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            //First thing: is the request coming from a trusted client
            var apiClientId = GetApiAuthTokenFromRequest(actionContext.Request);
            var isValidApiClient = await actionContext.GetService<IMediator>().SendAsync(new IsValidApiClientQuery(apiClientId));

            if (!isValidApiClient)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "invalid api_auth_token");
                return;
            }

            //now proceed to authorize the logged on user
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (principal == null || principal.Claims.None())
            {
                //user identity cant be converted to claims identity, try get user claims from authtoken querystring
                await TryGetUserClaimsFromAuthTokenQueryString(actionContext);
                return;
            }

            if (!principal.Identity.IsAuthenticated)
            {
                //user is not authenticated, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user not authenticated");
                return;
            }

            if (!(principal.HasClaim(x => x.Type == ClaimTypes.UserPermissions)))
            {
                //user doesnt have permissions claim, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user does not have permissions claim");
                return;
            }

            long userPermissions;
            if (!long.TryParse(principal.Claims.Single(x => x.Type == ClaimTypes.UserPermissions).Value, out userPermissions))
            {
                //couldnt convert user permissions claim to long, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user permissions claim not valid, try logging in again.");
                return;
            }

            if (RequiredPermissions.None(p => (userPermissions & p) == p))
            {
                //user does not have required permissions, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user does not have required permissions");
                return;
            }

            //User is Authenticated and Authorized, complete execution
        }

        private Guid GetApiAuthTokenFromRequest(HttpRequestMessage request)
        {
            //first look for request header "api_auth_key"
            if (request.Headers.Contains(Constants.ApiAuthKeyHeader))
                return new Guid(request.Headers.GetValues(Constants.ApiAuthKeyHeader).FirstOrDefault());

            //next look for query string paramert "api_auth_key"
            var token = request.GetQueryNameValuePairs().FirstOrDefault(kvp => kvp.Key == Constants.ApiAuthKeyHeader);

            if (token.Value.HasValue()) return new Guid(token.Value);

            //finally return empty guid
            return Guid.Empty;
        }

        protected Task TryGetUserClaimsFromAuthTokenQueryString(HttpActionContext actionContext)
        {
            var longDateTimeStringFormat = "dd MMM yyyy HH:mm:ss";
            var authToken = actionContext.Request.GetQueryNameValuePairs().SingleOrDefault(i => i.Key == "authtoken").Value;

            if (authToken.IsNullOrEmpty())
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "no auth token present, try logging in.");
                return Task.FromResult<object>(null);
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken st;
            var validationParams = new TokenValidationParameters
            {
                RequireExpirationTime = true,
                RequireSignedTokens = true,
                IssuerSigningKey = CustomJwtFormat.GetSigningCredential().SigningKey,
                ValidAudience = AppSettings.OAuthAudienceId,
                ValidIssuer = AppSettings.JwtIssuer
            };

            try
            {
                tokenHandler.ValidateToken(authToken, validationParams, out st);
            }
            catch (Exception ex)
            {
                //couldnt validate authtoken, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "could not validate auth token, try logging in again.");
                return Task.FromResult<object>(null);
            }

            var securityToken = st as JwtSecurityToken;

            if (securityToken == null)
            {
                //couldnt read authtoken, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "could not read auth token, try logging in again.");
                return Task.FromResult<object>(null);
            }

            if (securityToken.ValidTo < DateTime.UtcNow)
            {
                //token expired or not started yet, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "auth token expired, try logging in again. (auth token valid to: {0}, Now: {1})".FormatWith(securityToken.ValidTo.ToString(longDateTimeStringFormat), DateTime.Now.ToString(longDateTimeStringFormat)));
                return Task.FromResult<object>(null);
            }

            if (securityToken.ValidFrom > DateTime.UtcNow)
            {
                //token expired or not started yet, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "auth token not valid yet, try logging in again. (auth token valid from: {0}, Now: {1})".FormatWith(securityToken.ValidFrom.ToString(longDateTimeStringFormat), DateTime.Now.ToString(longDateTimeStringFormat)));
                return Task.FromResult<object>(null);
            }

            long userPermissions;
            if (!long.TryParse(securityToken.Claims.Single(x => x.Type == ClaimTypes.UserPermissions).Value, out userPermissions))
            {
                //couldnt convert user permissions claim to long, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user permissions claim not valid, try logging in again.");
                return Task.FromResult<object>(null);
            }

            if (RequiredPermissions.None(p => (userPermissions & p) == p))
            {
                //user does not have required permissions, return 401
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "user does not have required permissions");
                return Task.FromResult<object>(null);
            }

            actionContext.GetService<IUserProvider>().SetClaimsIdentity(securityToken.Claims);

            //User is Authenticated and Authorized, complete execution
            return Task.FromResult<object>(null);            
        }

    }
}