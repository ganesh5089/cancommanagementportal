﻿namespace Cancom.WebApi.Infrastructure
{
    public static class ClaimTypes
    {
        public static readonly string UserId = "user.id";
        public static readonly string UserName = "user.name";
        public static readonly string FullName = "user.fullname";
        public static readonly string UserPermissions = "user.permissions";
        public static readonly string TimeZone = "user.timeZone";
        public static readonly string RefreshToken = "user.refreshtoken";
        public static readonly string RefreshTokenIssued = "user.refreshtokenissued";
        public static readonly string RefreshTokenExpired = "user.refreshtokenexpired";
    }
}