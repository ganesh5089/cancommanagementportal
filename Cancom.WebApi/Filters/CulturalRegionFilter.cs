﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Cancom.WebApi.Filters
{
    public class CulturalRegionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            OverrideServerCulture();
        }

        private void OverrideServerCulture()
        {
            //TODO this code will be used when moving to differenct countries
            //var culture = CultureInfo.CreateSpecificCulture("en-US");
            //Thread.CurrentThread.CurrentCulture = culture;
        }
    }
}