﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Cancom.WebApi.Extensions
{
    public static class PdfReportExtensions
    {
        public static HttpResponseMessage CreatePdfReportResponse(this HttpRequestMessage request, byte[] reportStream,
            string filename)
        {
            var result = request.CreateResponse(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(reportStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = filename
            };

            return result;
        }

    }
}