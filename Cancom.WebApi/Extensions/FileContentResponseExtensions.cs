﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Cancom.WebApi.Extensions
{
    public static class FileContentResponseExtensions
    {
        public static HttpResponseMessage CreateFileResponse(this HttpRequestMessage request, byte[] reportStream, string filename)
        {
            var result = request.CreateResponse(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(reportStream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = filename
            };

            return result;
        }
    }
}