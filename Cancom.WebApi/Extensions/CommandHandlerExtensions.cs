﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using Cancom.Commands;
using Elmah;

namespace Cancom.WebApi.Extensions
{
    public static class CommandHandlerExtensions
    {
        public static HttpResponseMessage ToHttpResponseMessage(this CommandResult commandResult)
        {
            if (!commandResult.IsFailure)
                return new HttpResponseMessage(HttpStatusCode.OK);

            if (commandResult.HasException)
            {
                ErrorSignal.FromCurrentContext().Raise(commandResult.Exception);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(commandResult.HtmlFormattedFailures)
            };
        }

        public static HttpResponseMessage ToHttpResponseMessage<T>(this CommandResult<T> commandResult)
        {
            if (!commandResult.IsFailure)
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = new ObjectContent<T>(commandResult.Value, new JsonMediaTypeFormatter()) };

            if (commandResult.HasException)
            {
                ErrorSignal.FromCurrentContext().Raise(commandResult.Exception);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(commandResult.HtmlFormattedFailures)
            };
        }
    }
}