﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.ViewModel;

namespace Cancom.WebApi.Extensions
{
    public static class UploadFileExtensions
    {
        public async static Task<KeyValuePair<HttpResponseMessage, FileViewModel>> GetUploadedFile(this HttpRequestMessage request)
        {
            var result = await GetUploadedFiles(request);
            if (result.Value.ToList().Count > 1)
                return new KeyValuePair<HttpResponseMessage, FileViewModel>(request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Multiple Files Not Allowed"), null);

            return new KeyValuePair<HttpResponseMessage, FileViewModel>(result.Key, result.Value.Single());
        }

        public async static Task<KeyValuePair<HttpResponseMessage, IEnumerable<FileViewModel>>> GetUploadedFiles(this HttpRequestMessage request)
        {
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                var memoryStreamProvider = await request.Content.ReadAsMultipartAsync();

                var files = await GetFiles(memoryStreamProvider);

                return new KeyValuePair<HttpResponseMessage, IEnumerable<FileViewModel>>(request.CreateResponse(HttpStatusCode.OK), files);
            }
            catch (Exception e)
            {
                return new KeyValuePair<HttpResponseMessage, IEnumerable<FileViewModel>>(request.CreateErrorResponse(HttpStatusCode.InternalServerError, e), null);
            }

        }

        private async static Task<IEnumerable<FileViewModel>> GetFiles(MultipartMemoryStreamProvider multipartMemoryStreamProvider)
        {
            var files = new List<FileViewModel>();
            foreach (var item in multipartMemoryStreamProvider.Contents.Where(a => a.Headers.ContentType != null)) //This is to exclude formData that is being passed up with the upload
            {
                files.Add(new FileViewModel()
                {
                    Name = item.Headers.ContentDisposition.FileName.Replace("\"", ""),
                    Type = item.Headers.ContentType.MediaType,
                    Length = item.Headers.ContentLength.Value,
                    Data = await item.ReadAsByteArrayAsync()
                });
            }

            return files;
        }
    }
}