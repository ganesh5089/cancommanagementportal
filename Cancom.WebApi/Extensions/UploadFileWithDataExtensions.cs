﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.ViewModel;

namespace Cancom.WebApi.Extensions
{
    public static class UploadFileWithDataExtensions
    {
        public async static Task<KeyValuePair<HttpResponseMessage, UploadDataViewModel>> GetUploadedFileWithData(this HttpRequestMessage request)
        {
            var result = await GetUploadedFiles(request);

            return new KeyValuePair<HttpResponseMessage, UploadDataViewModel>(result.Key, result.Value);
        }

        private async static Task<KeyValuePair<HttpResponseMessage, UploadDataViewModel>> GetUploadedFiles(this HttpRequestMessage request)
        {
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                var memoryStreamProvider = await request.Content.ReadAsMultipartAsync();

                if (memoryStreamProvider.Contents.Count(a => a.Headers.ContentType != null) > 1)
                    return new KeyValuePair<HttpResponseMessage, UploadDataViewModel>(request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Multiple Files Not Allowed"), null);

                var files = await GetFiles(memoryStreamProvider);

                return new KeyValuePair<HttpResponseMessage, UploadDataViewModel>(request.CreateResponse(HttpStatusCode.OK), files);
            }
            catch (Exception e)
            {
                return new KeyValuePair<HttpResponseMessage, UploadDataViewModel>(request.CreateErrorResponse(HttpStatusCode.InternalServerError, e), null);
            }

        }

        private async static Task<UploadDataViewModel> GetFiles(MultipartMemoryStreamProvider multipartMemoryStreamProvider)
        {


            var uploadDataViewModel = new UploadDataViewModel()
            {
                FileViewModel = new FileViewModel(),
                FormData = new List<KeyValuePair<string, string>>()
            };

            foreach (var item in multipartMemoryStreamProvider.Contents)
            {
                if (item.Headers.ContentType != null)//This is to exclude formData that is being passed up with the upload
                {
                    uploadDataViewModel.FileViewModel = new FileViewModel()
                    {
                        Name = item.Headers.ContentDisposition.FileName.Replace("\"", ""),
                        Type = item.Headers.ContentType.MediaType,
                        Length = item.Headers.ContentLength.Value,
                        Data = await item.ReadAsByteArrayAsync()
                    };
                }
                else if (item.Headers.ContentType == null)
                {
                    var formDataKey = item.Headers.ContentDisposition.Name.Replace("\"", "");
                    var formDataValue = await item.ReadAsStringAsync();

                    uploadDataViewModel.FormData.Add(new KeyValuePair<string, string>(formDataKey, formDataValue));
                }
            }

            return uploadDataViewModel;
        }
    }
}