﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using Cancom.Common;
using Cancom.WebApi.Providers;
using Module = Autofac.Module;

namespace Cancom.WebApi
{
    public class WebApiAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //per Http Request
            builder.RegisterType<LoggedOnUserProvider>().As<IUserProvider>().InstancePerRequest();
            builder.RegisterType<HttpContextUserClaimsIdentityProvider>().As<IClaimsIdentityProvider>().InstancePerRequest();
        }
    }
}