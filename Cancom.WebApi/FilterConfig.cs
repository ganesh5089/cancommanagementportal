﻿using System.Web.Http;
using Cancom.WebApi.Filters;
using Cancom.WebApi.Infrastructure;
using Elmah.Contrib.WebApi;

namespace Cancom.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.Filters.Add(new CulturalRegionFilter());
            httpConfiguration.Filters.Add(new ElmahHandleErrorApiAttribute());
            httpConfiguration.Filters.Add(new CancomCommandHydratorAttribute());
        }
    }
}