﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using Cancom.Common;
using Newtonsoft.Json;
using NExtensions;
using ClaimTypes = Cancom.WebApi.Infrastructure.ClaimTypes;

namespace Cancom.WebApi.Providers
{
    public class LoggedOnUserProvider : IUserProvider
    {
        private readonly IClaimsIdentityProvider claimsIdentityProvider;
        private ClaimsIdentity identity;

        public LoggedOnUserProvider(IClaimsIdentityProvider claimsIdentityProvider)
        {
            this.claimsIdentityProvider = claimsIdentityProvider;
            SetLoggedOnUserPropertiesFromHttpContext();
        }

        public int UserId { get; private set; }

        public string UserName { get; private set; }

        public string FullName { get; private set; }

        //This method can only be called from a context where the HttpContext exists
        public void SetClaimsIdentity(IEnumerable<Claim> claims)
        {
            claimsIdentityProvider.SetUser(new ClaimsPrincipal(new ClaimsIdentity(claims)));
            identity = claimsIdentityProvider.GetClaimsIdentity();
            SetLoggedOnUserPropertiesFromHttpContext();
        }

        public IEnumerable<Claim> GetClaims()
        {
            return identity?.Claims ?? new Claim[] { };
        }


        private void SetLoggedOnUserPropertiesFromHttpContext()
        {
            identity = claimsIdentityProvider.GetClaimsIdentity();
            UserId = GetIntClaimTypeValue(ClaimTypes.UserId);
            UserName = GetStringClaimTypeValue(ClaimTypes.UserName);
            FullName = GetStringClaimTypeValue(ClaimTypes.FullName);
        }

        private string GetStringClaimTypeValue(string claimType)
        {
            return identity.FindFirst(c => c.Type == claimType)?.Value ?? string.Empty;
        }

        private int GetIntClaimTypeValue(string claimType)
        {
            var claim = identity?.FindFirst(c => c.Type == claimType);
            return claim == null ? 0 : Convert.ToInt32(claim.Value);
        }

        public string TimeZone => GetStringClaimTypeValue(ClaimTypes.TimeZone);


        protected TimeZoneInfo _timeZoneInstance;

        [JsonIgnore]
        public TimeZoneInfo TimeZoneInstance => _timeZoneInstance ??
                                                TimeZoneInfo.FindSystemTimeZoneById(string.IsNullOrEmpty(TimeZone)
                                                    ? "South Africa Standard Time" : TimeZone);

        /// <summary>
        /// Returns a UTC time in the user's specified timezone.
        /// </summary>
        /// <param name="utcTime">The utc time to convert</param>
        /// <returns>New local time</returns>
        public DateTime DisplayUserTimeFromUtc(DateTime? utcTime = null)
        {
            if (utcTime == null)
                utcTime = DateTime.UtcNow;

            return TimeZoneInfo.ConvertTimeFromUtc(utcTime.Value, TimeZoneInstance);
        }

        /// <summary>
        /// Converts a local machine time to the user's timezone time
        /// by applying the difference between the two timezones.
        /// </summary>
        /// <param name="localTime">local machine time</param>
        /// <returns></returns>
        public DateTime GetUserTimeAsUtc(DateTime localTime)
        {
            return SaveUserTimeAsUtc(localTime);
        }

        /// <summary>
        /// Converts a local machine time to the user's timezone time
        /// by applying the difference between the two timezones.
        /// </summary>
        /// <param name="localTime">local machine time</param>
        /// <returns></returns>
        public DateTime SaveUserTimeAsUtc(DateTime localTime)
        {
            if (localTime.Kind == DateTimeKind.Utc)
            {
                return localTime;
            }

            var offset = TimeZoneInstance.GetUtcOffset(localTime).TotalHours;
            var offset2 = TimeZoneInfo.Local.GetUtcOffset(localTime).TotalHours;
            return localTime.AddHours(offset2 - offset).ToUniversalTime();
        }

    } 
}