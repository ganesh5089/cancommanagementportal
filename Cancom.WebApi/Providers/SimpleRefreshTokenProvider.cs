﻿using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Owin;
using AutoMapper;
using Cancom.Commands.RefreshToken;
using Cancom.Common.Extensions;
using Cancom.ViewModel;
using Cancom.WebApi.Infrastructure;
using MediatR;
using Microsoft.Owin.Security.Infrastructure;

namespace Cancom.WebApi.Providers
{
    public class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private IMediator mediatr;

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            mediatr = context.OwinContext.GetAutofacLifetimeScope().Resolve<IMediator>();
            if (mediatr == null)//double check
                throw new Exception("Could not resolve IMediator from context;");
         
            var refreshTokenId = Guid.NewGuid().ToString("n");

            var token = new RefreshTokenViewModel
            {
                Id = refreshTokenId.ToHash(),
                UserName = context.Ticket.Identity.Name,
                IssuedUtc = DateTime.Now,
                ExpiresUtc = DateTime.Now.AddMinutes(AppSettings.OAutRefreshTokenExpirationInMinutes)
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            token.ProtectedTicket = context.SerializeTicket();
            SaveRefreshTokenCommand command = Mapper.Map<SaveRefreshTokenCommand>(token);
            var result = await mediatr.SendAsync(command);
            if (result.IsSuccess)
            {
                context.SetToken(refreshTokenId);
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
           
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
         
        }
    }
}