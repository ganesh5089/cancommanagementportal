﻿using System;
using System.IdentityModel.Tokens;
using Cancom.WebApi.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;

namespace Cancom.WebApi.Providers
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {

        private readonly string issuer;

        public CustomJwtFormat(string issuer)
        {
            this.issuer = issuer;
        }

        /// <summary>
        /// This servers as BOTH resource and authorization server, different lines in this method relate to the resource or authorization server
        /// </summary>
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var issued = data.Properties.IssuedUtc;

            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(issuer, AppSettings.OAuthAudienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, GetSigningCredential());

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }

        public static SigningCredentials GetSigningCredential()
        {
            return new HmacSigningCredentials(TextEncodings.Base64Url.Decode(AppSettings.OAuthAudienceSecret));
        }
    }
}