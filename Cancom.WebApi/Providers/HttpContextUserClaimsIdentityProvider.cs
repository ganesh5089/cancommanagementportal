﻿using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace Cancom.WebApi.Providers
{
    public interface IClaimsIdentityProvider
    {
        ClaimsIdentity GetClaimsIdentity();
        void SetUser(IPrincipal user);
    }
    public class HttpContextUserClaimsIdentityProvider : IClaimsIdentityProvider
    {
        public ClaimsIdentity GetClaimsIdentity()
        {
            return HttpContext.Current?.User?.Identity as ClaimsIdentity;
        }

        public void SetUser(IPrincipal user)
        {
            HttpContext.Current.User = user;
        }
    }
}