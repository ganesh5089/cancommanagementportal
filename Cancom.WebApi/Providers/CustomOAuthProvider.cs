﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Owin;
using AutoMapper;
using Cancom.Commands.Login;
using Cancom.Queries.Infrastructure;
using Cancom.WebApi.Infrastructure;
using MediatR;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using NExtensions;
using ClaimTypes = Cancom.WebApi.Infrastructure.ClaimTypes;

namespace Cancom.WebApi.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private IMediator mediatr;
        private AuthUserManager userManager;

        public override Task  ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }
 

        public override async Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if (context.IsTokenEndpoint && context.Request.Method == "OPTIONS")
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "authorization", "api_auth_key" });
                context.RequestCompleted();
                return;
            }
            base.MatchEndpoint(context);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            mediatr = context.OwinContext.GetAutofacLifetimeScope().Resolve<IMediator>();
            if (mediatr == null)//double check
                throw new Exception("Could not resolve IMediator from context;");

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var apiAuthToken = GetApiAuthTokenFromRequest(context.Request);
            var isValidApiClient = await mediatr.SendAsync(new IsValidApiClientQuery(apiAuthToken));

            if (!isValidApiClient)
            {
                context.SetError("invalid_api_auth_key", "The provided api_auth_key is invalid.");
                return;
            }

            var loginResult = await mediatr.SendAsync(new LoginCommand(context.UserName, context.Password));

            if (loginResult.IsFailure)
            {
                context.SetError("invalid_grant", loginResult.Failures.First());
                return;
            }

            AuthUser user;
            try
            {
                user = Mapper.Map<AuthUser>(loginResult.Value);
            }
            catch (Exception ex)
            {
                var blah = ex;
                throw;
            }

            userManager = context.OwinContext.GetUserManager<AuthUserManager>();
            var ticket = await CreateAuthTicket(user);

            context.Validated(ticket);
        }

        private async Task<AuthenticationTicket> CreateAuthTicket(AuthUser user)
        {
            var oAuthIdentity = await userManager.CreateIdentityAsync(user, "JWT");

            //add custom claims here
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.UserId, user.Id.ToString()));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.UserName, user.UserName));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.FullName, user.FullName));

            //check if a user can have null permissions and what that signifies
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.UserPermissions, (user.PermissionSet ?? "")));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.TimeZone, user.TimeZone ?? ""));

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "as:client_id", user.UserName
                }
            });

            return new AuthenticationTicket(oAuthIdentity, props);
        }

        private Guid GetApiAuthTokenFromRequest(IOwinRequest request)
        {
            //first look for request header "api_auth_token"
            string token = request.Headers[Constants.ApiAuthKeyHeader];

            if (token.HasValue()) return new Guid(token);

            //next look for query string paramert "api_auth_token"
            token = request.Query.Get(Constants.ApiAuthKeyHeader);

            if (token.HasValue()) return new Guid(token);

            //finally return empty guid
            return Guid.Empty;
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}