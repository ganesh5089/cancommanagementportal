﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.Product;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class ProductController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public ProductController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/product/getallproducts")]
        public async Task<IEnumerable<ProductViewModel>> GetProducts()
        {
            return await mediatr.SendAsync(new ProductQuery(user));
        }
    }
}