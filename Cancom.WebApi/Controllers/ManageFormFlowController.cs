﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.ManageFormFlow;
using Cancom.Common;
using Cancom.Queries.ManageFormFlow;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class ManageFormFlowController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public ManageFormFlowController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/manageformflow/getallmanageformflowavailableforms")]
        public async Task<IEnumerable<LookupViewModel>> GetAllManageFormFlowAvailableForms()
        {
            return await mediatr.SendAsync(new ManageFormFlowAvailableFormsQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/manageformflow/getallmanageformflowallocatedforms")]
        public async Task<IEnumerable<LookupViewModel>> GetAllManageFormFlowAllocatedForms([FromUri] ManageFormFlowAllocatedFormsQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/manageformflow/getallocatedform")]
        public async Task<AllocatedFormsWithCsspmIdViewModel> GetAllocatedForm([FromUri] GetAllocatedFormQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpPost]
        [Authorize]
        [Route("api/manageformflow/formflowaddtoallocatedforms")]
        public async Task<HttpResponseMessage> AddFormsToAllocatedForms(FormFlowAddAllocatedCommand formFlowAddAllocatedCommand)
        {
            var result = await mediatr.SendAsync(formFlowAddAllocatedCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/manageformflow/formflowremovefromallocatedforms")]
        public async Task<HttpResponseMessage> RemoveFormsFromAllocatedForms(FormFlowRemoveAllocatedCommand formFlowRemoveAllocatedCommand)
        {
            var result = await mediatr.SendAsync(formFlowRemoveAllocatedCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/manageformflow/formflowupallocatedforms")]
        public async Task<HttpResponseMessage> AllocatedFormsMoveUp(FormFlowUpAllocatedCommand formFlowUpAllocatedCommand)
        {
            var result = await mediatr.SendAsync(formFlowUpAllocatedCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/manageformflow/formflowdownallocatedforms")]
        public async Task<HttpResponseMessage> AllocatedFormsMoveDown(FormFlowDownAllocatedCommand formFlowDownAllocatedCommand)
        {
            var result = await mediatr.SendAsync(formFlowDownAllocatedCommand);
            return result.ToHttpResponseMessage();
        }
    }
}
