﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.AccessV2Transaction;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class AccessV2TransactionsController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public AccessV2TransactionsController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessv2transaction/getallclients")]
        public async Task<IEnumerable<LookupViewModel>> GetAllWebClients()
        {
            return await mediatr.SendAsync(new AccessV2TransactionsClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessv2transaction/getaccessv2transactionlist")]
        public async Task<IEnumerable<AccessV2TransactionsViewModel>> GetAccessV2TransactionList([FromUri] AccessV2TransactionsQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessv2transaction/getaccessv2transactiondetailsbyid")]
        public async Task<AccessV2TransactionsDetailsViewModel> GetAccessV2TransactionDetailsById([FromUri] AccessV2TransactionsDetailsViewQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }
    }
}