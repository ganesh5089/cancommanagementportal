﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.UserAccess;
using Cancom.Common;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class UserAccessController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public UserAccessController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpPost]
        [Authorize]
        [Route("api/useraccess/delete")]
        public async Task<HttpResponseMessage> DeleteUserAccess(DeleteUserAccessCommand deleteUserAccessCommand)
        {
            var result = await mediatr.SendAsync(deleteUserAccessCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/useraccess/saveuseraccess")]
        public async Task<HttpResponseMessage> SaveUserAccess(SaveUserAccessCommand saveUserAccessCommand)
        {
            var result = await mediatr.SendAsync(saveUserAccessCommand);
            return result.ToHttpResponseMessage();
        }
    }
}