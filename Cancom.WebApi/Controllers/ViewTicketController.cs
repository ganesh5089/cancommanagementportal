﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.ViewTicket;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class ViewTicketController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public ViewTicketController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }


        [HttpGet]
        [Authorize]
        [Route("api/viewticket/getalltickets")]
        public async Task<IEnumerable<ViewTicketViewModel>> GetAllTicket([FromUri] ViewTicketQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

    }
}