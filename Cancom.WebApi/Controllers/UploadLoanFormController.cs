﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Cancom.Commands.UploadLoanForm;
using Cancom.Common;
using Cancom.Queries.UploadLoanForm;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class UploadLoanFormController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public UploadLoanFormController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/uploadloanform/getuserlistbyclient/{id}")]
        public async Task<IEnumerable<UploadLoanFormViewModel>> GetUserListByClient(int id)
        {
            return await mediatr.SendAsync(new UploadLoanFormUserListByClientQuery(id));
        }

        [HttpGet]
        [Authorize]
        [Route("api/uploadloanform/getfilelistbyclient/{id}")]
        public async Task<IEnumerable<LookupViewModel>> GetFileListByClient(int id)
        {
            return await mediatr.SendAsync(new UploadLoanFormFileListByClientQuery(id));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/uploadloanform/import")]
        public async Task<HttpResponseMessage> Import()
        {
            var command = new SaveLoanFormFileCommand();

            var uploadedFileWithDataViewModel = await Request.GetUploadedFileWithData();

            Mapper.Map(uploadedFileWithDataViewModel.Value, command);
            var result = await mediatr.SendAsync(command);
            return result.ToHttpResponseMessage();
        }
    }
}