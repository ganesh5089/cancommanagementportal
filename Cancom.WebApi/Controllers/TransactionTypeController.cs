﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.TransactionType;
using Cancom.Common;
using Cancom.Queries.TransactionType;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class TransactionTypeController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public TransactionTypeController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/transactiontype/getalltransactiontype")]
        public async Task<IEnumerable<TransactionTypeViewModel>> GetAllTransactionType()
        {
            return await mediatr.SendAsync(new TransactionTypeQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/transactiontype/gettransactiontypedetails/{id}")]
        public async Task<TransactionTypeViewModel> GetTransactionTypeDetails(int id)
        {
            return await mediatr.SendAsync(new TransactionTypeDetailsQuery(id));
        }

        [HttpPost]
        [Authorize]
        [Route("api/transactiontype/{id}/delete")]
        public async Task<HttpResponseMessage> DeleteAccessClient(int id)
        {
            var result = await mediatr.SendAsync(new DeleteTransactionTypeCommand(user, id));
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/transactiontype/savetransactiontype")]
        public async Task<HttpResponseMessage> SaveAccessClient(SaveTransactionTypeCommand saveTransactionTypeCommand)
        {
            var result = await mediatr.SendAsync(saveTransactionTypeCommand);
            return result.ToHttpResponseMessage();
        }
    }
}
