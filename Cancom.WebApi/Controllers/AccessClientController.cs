﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.AccessClient;
using Cancom.Common;
using Cancom.Queries.AccessClient;
using Cancom.Queries.ManageMobileSettings;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    /// <summary>
    /// Access Client Controller
    /// </summary>
    public class AccessClientController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        /// <summary>
        /// Access Client Controller Constructor
        /// </summary>
        /// <param name="user"></param>
        /// <param name="mediatr"></param>
        public AccessClientController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }


        /// <summary>
        /// This will return All Access Clients
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("api/accessclient/getallaccessclients")]
        public async Task<IEnumerable<AccessClientViewModel>> GetAllAccessClients()
        {
            return await mediatr.SendAsync(new AccessClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessclient/getmanagemobilesettingscsspm")]
        public async Task<IEnumerable<ManageMobileSettingsCsspmViewModel>> GetManageMobileSettingsCsspm()
        {
            return await mediatr.SendAsync(new ManageMobileSettingsCsspmQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessclient/getaccessclientdetails/{id}")]
        public async Task<AccessClientViewModel> GetAccessClientDetails(int id)
        {
            return await mediatr.SendAsync(new AccessClientDetailsQuery(id));
        }

        [HttpGet]
        [Authorize]
        [Route("api/accessclient/getallproducts")]
        public async Task<IEnumerable<LookupViewModel>> GetProducts()
        {
            return await mediatr.SendAsync(new AccessClientProductQuery(user));
        }

        [HttpPost]
        [Authorize]
        [Route("api/accessclient/{id}/delete")]
        public async Task<HttpResponseMessage> DeleteAccessClient(int id)
        {
            var result = await mediatr.SendAsync(new DeleteAccessClientCommand(user, id));
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/accessclient/saveaccessclient")]
        public async Task<HttpResponseMessage> SaveAccessClient(SaveAccessClientCommand saveAccessClientCommand)
        {
            var result = await mediatr.SendAsync(saveAccessClientCommand);
            return result.ToHttpResponseMessage();
        }
    }
}
