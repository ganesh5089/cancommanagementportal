﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.VehicleType;
using Cancom.Common;
using Cancom.Queries.VehicleType;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class VehicleTypeController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public VehicleTypeController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/vehicletype/getallvehicletype")]
        public async Task<IEnumerable<VehicleTypeViewModel>> GetAllVehicleType()
        {
            return await mediatr.SendAsync(new VehicleTypeQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/vehicletype/getvehicletypedetails/{id}")]
        public async Task<VehicleTypeViewModel> GetVehicleTypeDetails(int id)
        {
            return await mediatr.SendAsync(new VehicleTypeDetailsQuery(id));
        }

        [HttpGet]
        [Authorize]
        [Route("api/vehicletype/getvehicletypelistbytransactionid")]
        public async Task<IEnumerable<LookupViewModel>> GetVehicleTypeListByTransactionId([FromUri] VehicleTypeListByTransactionIdQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/vehicletype/gettransactiontypesbycsspmid")]
        public async Task<IEnumerable<LookupViewModel>> GetTransactionTypesByCsspmid([FromUri] VehicleTypeTransactionTypeQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpPost]
        [Authorize]
        [Route("api/vehicletype/{id}/delete")]
        public async Task<HttpResponseMessage> DeleteVehicleType(int id)
        {
            var result = await mediatr.SendAsync(new DeleteVehicleTypeCommand(user, id));
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/vehicletype/savevehicletype")]
        public async Task<HttpResponseMessage> SaveVehicleType(SaveVehicleTypeCommand saveVehicleTypeCommand)
        {
            var result = await mediatr.SendAsync(saveVehicleTypeCommand);
            return result.ToHttpResponseMessage();
        }
    }
}
