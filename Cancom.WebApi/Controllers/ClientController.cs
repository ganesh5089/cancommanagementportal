﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.Client;
using Cancom.Common;
using Cancom.Queries.Client;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public ClientController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }


        [HttpGet]
        [Authorize]
        [Route("api/client/getallclients")]
        public async Task<IEnumerable<ClientsViewModel>> GetClients()
        {
            return await mediatr.SendAsync(new ClientsQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/client/getclientdetails/{id}")]
        public async Task<ClientDetailsViewModel> GetClientDetails(int id)
        {
            return await mediatr.SendAsync(new ClientDetailsQuery(id));
        }

        [HttpPost]
        [Authorize]
        [Route("api/client/{id}/delete")]
        public async Task<HttpResponseMessage> DeleteClient(int id)
        {
            var result = await mediatr.SendAsync(new DeleteClientCommand(user, id));
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/client/saveclient")]
        public async Task<HttpResponseMessage> SaveClient(SaveClientCommand saveClientCommand)
        {
            var result = await mediatr.SendAsync(saveClientCommand);
            return result.ToHttpResponseMessage();
        }
    }
}