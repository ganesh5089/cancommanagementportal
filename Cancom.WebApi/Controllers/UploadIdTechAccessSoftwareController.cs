﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Cancom.Commands.UploadIdTechAccessSoftware;
using Cancom.Common;
using Cancom.Common.Enums;
using Cancom.Queries.UploadIdTechAccessSoftware;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class UploadIdTechAccessSoftwareController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public UploadIdTechAccessSoftwareController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/uploadidtechaccesssoftware/getallclientsidtechaccesssoftware/{isActive}")]
        public async Task<IEnumerable<UploadIdTechAccessSoftwareViewModel>> GetAllClientsIdTechAccessSoftware(bool isActive)
        {
            return await mediatr.SendAsync(new UploadIdTechAccessSoftwareQuery(isActive, IdTechSoftwareFileType.AllClient));
        }


        [HttpGet]
        [Authorize]
        [Route("api/uploadidtechaccesssoftware/getsabidtechaccesssoftware/{isActive}")]
        public async Task<IEnumerable<UploadIdTechAccessSoftwareViewModel>> GetSabIdTechAccessSoftware(bool isActive)
        {
            return await mediatr.SendAsync(new UploadIdTechAccessSoftwareQuery(isActive, IdTechSoftwareFileType.Sab));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/uploadidtechaccesssoftware/saveuploadidtechaccesssoftware")]
        public async Task<HttpResponseMessage> SaveUploadIdTechAccessSoftware()
        {
            var uploadedFileWithDataViewModel = await Request.GetUploadedFileWithData();
            var command = new UploadIdTechAccessSoftwareCommand(user);
            command = Mapper.Map(uploadedFileWithDataViewModel.Value, command);
            var result = await mediatr.SendAsync(command);
            return result.ToHttpResponseMessage();
        }

        [HttpGet]
        [Route("api/uploadidtechaccesssoftware/downloaduploadidtechaccesssoftwarezipfile")]
        public async Task<HttpResponseMessage> DownloadSoftwareZipFile([FromUri] DownloadIdTechAccessSoftwareFileQuery qry)
        {
            var result = await mediatr.SendAsync(qry);
            return Request.CreateFileResponse(result.ToArray(), qry.FileName);
        }
    }
}