﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Common.Extensions;
using Cancom.Queries.FineManagement;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class FineManagementController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public FineManagementController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/finemanagement/getallclientsfinemanagement")]
        public async Task<IEnumerable<LookupViewModel>> GetClientsFineManagement()
        {
            return await mediatr.SendAsync(new FineManagementClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/finemanagement/getfinemanagementbysearch")]
        public async Task<IEnumerable<FineManagementPropertyViewModel>> GetFineManagementDetailsBySearch([FromUri] FineManagementQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/finemanagement/getfinemanagementviewdetails/{ticketId}")]
        public async Task<FineManagementViewViewModel> GetFineManagementViewDetails(int ticketId)
        {
            return await mediatr.SendAsync(new FineManagementViewQuery(ticketId));
        }

        [HttpGet]
        [Route("api/finemanagement/downloadfinepdf/{id}")]
        public async Task<HttpResponseMessage> DownloadFinePdf(int id)
        {
            var result = await mediatr.SendAsync(new FineManagementPdfQuery(user, id));
            return Request.CreatePdfReportResponse(result.Item1.GetBuffer().EncryptPdf(), result.Item2);
        }
    }
}
