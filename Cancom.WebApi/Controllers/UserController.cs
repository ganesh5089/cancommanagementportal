﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.User;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public UserController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/user/getallclients")]
        public async Task<IEnumerable<LookupViewModel>> GetClients()
        {
            return await mediatr.SendAsync(new UserClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/user/getusersbyclientid")]
        public async Task<IEnumerable<LookupViewModel>> GetUsersByClientId([FromUri] UserUserQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/user/getclientuserpermissions")]
        public async Task<IEnumerable<UserAccessPermission>> GetClientUserPermissions([FromUri] UserAccessQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/user/getallProductsfromclientuser")]
        public async Task<IEnumerable<LookupViewModel>> GetAllProductsFromClientUser([FromUri] UserProductQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }
    }
}