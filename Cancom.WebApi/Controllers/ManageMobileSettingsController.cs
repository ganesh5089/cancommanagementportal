﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.ManageMobileSettings;
using Cancom.Common;
using Cancom.Queries.DownloadMobileFiles;
using Cancom.Queries.ManageMobileSettings;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class ManageMobileSettingsController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public ManageMobileSettingsController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/managemobilesettings/getsettings/{csspmId}")]
        public async Task<AccessSettingsViewModel> GetSettings(int csspmId)
        {
            return await mediatr.SendAsync(new ManageMobileSettingsQuery(csspmId));
        }

        [HttpGet]
        [Route("api/managemobilesettings/downloadmobilezipfile")]
        public async Task<HttpResponseMessage> DownloadMobileZipFile([FromUri] DownloadMobileFilesQuery qry)
        {
            var result = await mediatr.SendAsync(qry);
            return Request.CreateFileResponse(result.ToArray(), $"{qry.CsspmId}_{qry.ClientName}_{qry.MobileName}.zip");
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/saveclientsettings")]
        public async Task<HttpResponseMessage> SaveClientSettings(SaveClientSettingsCommand saveClientSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveClientSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/savegeneralsettings")]
        public async Task<HttpResponseMessage> SaveGeneralSettings(SaveGeneralSettingsCommand saveGeneralSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveGeneralSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/saveentersettings")]
        public async Task<HttpResponseMessage> SaveEnterSettings(SaveEnterSettingsCommand saveEnterSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveEnterSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/saveexitsettings")]
        public async Task<HttpResponseMessage> SaveExitSettings(SaveExitSettingsCommand saveExitSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveExitSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/savenetworksettings")]
        public async Task<HttpResponseMessage> SaveNetworkSettings(SaveNetworkSettingsCommand saveNetworkSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveNetworkSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/saveformsettings")]
        public async Task<HttpResponseMessage> SaveFormSettings(SaveFormSettingsCommand saveFormSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveFormSettingsCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/managemobilesettings/saveoptionssettings")]
        public async Task<HttpResponseMessage> SaveOptionsSettings(SaveOptionsSettingsCommand saveOptionsSettingsCommand)
        {
            var result = await mediatr.SendAsync(saveOptionsSettingsCommand);
            return result.ToHttpResponseMessage();
        }
    }
}