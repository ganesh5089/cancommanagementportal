﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Commands.Cssm;
using Cancom.Common;
using Cancom.Queries.Cssm;
using Cancom.ViewModel;
using Cancom.WebApi.Extensions;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class CssmController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public CssmController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/cssm/getallclients")]
        public async Task<IEnumerable<LookupViewModel>> GetAllClients()
        {
            return await mediatr.SendAsync(new CssmClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/cssm/getallproducts")]
        public async Task<IEnumerable<LookupViewModel>> GetAllProducts()
        {
            return await mediatr.SendAsync(new CssmProductQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/cssm/getcssmtreelistbyclientid/{id}")]
        public async Task<CssmViewModel> GetCssmTreeListByClientId(int id)
        {
            return await mediatr.SendAsync(new CssmTreeQuery(id));
        }

        [HttpPost]
        [Authorize]
        [Route("api/cssm/savecssm")]
        public async Task<HttpResponseMessage> SaveCssm(SaveCssmCommand saveCssmCommand)
        {
            var result = await mediatr.SendAsync(saveCssmCommand);
            return result.ToHttpResponseMessage();
        }

        [HttpPost]
        [Authorize]
        [Route("api/cssm/{id}/deletecssm")]
        public async Task<HttpResponseMessage> DeleteCssm(int id)
        {
            var result = await mediatr.SendAsync(new DeleteCssmCommand(user, id));
            return result.ToHttpResponseMessage();
        }
    }
}
