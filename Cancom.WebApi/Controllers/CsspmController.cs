﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.Csspm;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class CsspmController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public CsspmController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }

        [HttpGet]
        [Authorize]
        [Route("api/csspm/getallclients")]
        public async Task<IEnumerable<LookupViewModel>> GetAllClients()
        {
            return await mediatr.SendAsync(new CsspmClientQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/csspm/getallsitesbyclientid/{id}")]
        public async Task<IEnumerable<LookupViewModel>> GetAllSitesByClientId(int id)
        {
            return await mediatr.SendAsync(new CsspmSiteQuery(id));
        }

        [HttpGet]
        [Authorize]
        [Route("api/csspm/getallstationsbysiteid")]
        public async Task<IEnumerable<LookupViewModel>> GetAllStationsBySiteid([FromUri] CsspmStationQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/csspm/getallproductsbysiteid")]
        public async Task<IEnumerable<LookupViewModel>> GetAllProductsBySiteid([FromUri] CsspmProductQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }

        [HttpGet]
        [Authorize]
        [Route("api/csspm/getallmobilesbysiteid")]
        public async Task<IEnumerable<LookupViewModel>> GetAllMobilesBySiteid([FromUri] CsspmMobileQuery qry)
        {
            return await mediatr.SendAsync(qry);
        }
    }
}
