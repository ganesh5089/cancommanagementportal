﻿using System.Threading.Tasks;
using System.Web.Http;
using Cancom.Common;
using Cancom.Queries.Dashboard;
using Cancom.ViewModel;
using MediatR;

namespace Cancom.WebApi.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly IMediator mediatr;
        private readonly IUserProvider user;

        public DashboardController(IUserProvider user, IMediator mediatr)
        {
            this.mediatr = mediatr;
            this.user = user;
        }


        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getallclientscount")]
        public async Task<DashboardClientCountViewModel> GetTotalClientsCount()
        {
            return await mediatr.SendAsync(new TotalClientsCountQuery());
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getsabtodaylatetransactions")]
        public async Task<SabTransactionsViewModel> GetSabTodayLateTransactions()
        {
            return await mediatr.SendAsync(new SabTodayLateTransactionsQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getsabyesterdaylatetransactions")]
        public async Task<SabTransactionsViewModel> GetSabYesterdayLateTransactions()
        {
            return await mediatr.SendAsync(new SabYesterdayLateTransactionsQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getsablast7dayslatetransactions")]
        public async Task<SabTransactionsViewModel> GetSabLast7DaysLateTransactions()
        {
            return await mediatr.SendAsync(new SabLast7DaysLateTransactionsQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getcurrentmonthsabtransactionscount")]
        public async Task<SabTransactionCountViewModel> GetCurrentMonthSabTransactionsCount()
        {
            return await mediatr.SendAsync(new CurrentMonthSabTransactionsCountQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getpreviousmonthsabtransactionscount")]
        public async Task<SabTransactionCountViewModel> GetPreviousMonthSabTransactionsCount()
        {
            return await mediatr.SendAsync(new PreviousMonthSabTransactionsCountQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getcurrentmonthsummaryreport")]
        public async Task<SummaryReportViewModel> GetCurrentMonthSummaryReport()
        {
            return await mediatr.SendAsync(new CurrentMonthSummaryReportQuery(user));
        }

        [HttpGet]
        [Authorize]
        [Route("api/dashboard/getpreviousmonthsummaryreport")]
        public async Task<SummaryReportViewModel> GetPreviousMonthSummaryReport()
        {
            return await mediatr.SendAsync(new PreviousMonthSummaryReportQuery(user));
        }

    }
}