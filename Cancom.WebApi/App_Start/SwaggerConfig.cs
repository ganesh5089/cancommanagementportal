using System.Web.Http;
using WebActivatorEx;
using Cancom.WebApi;
using Swashbuckle.Application;

 

namespace Cancom.WebApi
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "Cancom.WebApi");
            })
                .EnableSwaggerUi(c =>
                {
                });
        }
    }
}