﻿using System;
using System.Linq;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using Cancom.Data;
using Cancom.WebApi;
using Cancom.WebApi.Infrastructure;
using Cancom.WebApi.Providers;
using Elmah.Contrib.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Cancom.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutoMapperConfig.Setup();

            var httpConfig = new HttpConfiguration();

            // Swagger
            SwaggerConfig.Register(httpConfig);

            AutofacConfig.Setup(httpConfig, app);

            ConfigureWebApi(httpConfig);

            ConfigureElmah(httpConfig);

            ConfigureOAuthTokenGeneration(app);

            ConfigureOAuthTokenConsumption(app);

            app.UseCors(CorsOptions.AllowAll);

           
            //Swashbuckle.Bootstrapper.Init(config);

            app.UseWebApi(httpConfig);

            FilterConfig.RegisterGlobalFilters(httpConfig);

            AreaRegistration.RegisterAllAreas();


            PrimeTheGlobalDataCache(httpConfig);
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        private void ConfigureElmah(HttpConfiguration config)
        {
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());
        }

        //acting as the Authorization server
        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            app.CreatePerOwinContext(AuthUserManager.Create);

            var options = new OAuthAuthorizationServerOptions
            {
                //TODO: For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString(AppSettings.OAuthTokenEndpoint),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(AppSettings.OAuthAccessTokenExpirationInHours),
                Provider = new CustomOAuthProvider(),
                AccessTokenFormat = new CustomJwtFormat(AppSettings.JwtIssuer),
                RefreshTokenProvider = new SimpleRefreshTokenProvider()
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(options);

            app.UseOAuthBearerAuthentication
                (
                    new OAuthBearerAuthenticationOptions
                    {
                        Provider = new OAuthBearerAuthenticationProvider()
                    }
                );
        }

        //acting as the Resource server
        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {
            var audienceSecret = TextEncodings.Base64Url.Decode(AppSettings.OAuthAudienceSecret);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { AppSettings.OAuthAudienceId },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(AppSettings.JwtIssuer, audienceSecret)
                    }
                });
        }

        private void PrimeTheGlobalDataCache(HttpConfiguration config)
        {
            var cache = config.DependencyResolver.GetService(typeof(IDataCache)) as IDataCache;

            var primeCacheTask = cache?.PrimeCache();
            primeCacheTask?.GetAwaiter().GetResult();
        }
    }
}
