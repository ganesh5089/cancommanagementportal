﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using Autofac;
using Autofac.Features.Variance;
using Autofac.Integration.WebApi;
using Cancom.Commands;
using Cancom.Queries;
using MediatR;
using Owin;

namespace Cancom.WebApi
{
    public static class AutofacConfig
    {
        private static IContainer container;
        private static AutofacWebApiDependencyResolver dependencyResolver;

        public static void Setup(HttpConfiguration config, IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterSource(new ContravariantRegistrationSource());

            builder.RegisterModule<WebApiAutofacModule>();
            builder.RegisterModule<CommandsAutofacModule>();
            builder.RegisterModule<QueriesAutofacModule>();

            builder.RegisterMediatr(() => container);

            container = builder.Build();

            var registrations =
              container.ComponentRegistry.Registrations.SelectMany(x => x.Services)
                  .Select(x => x.Description)
                  .ToList();

            registrations.ForEach(s => Debug.WriteLine(s));

            dependencyResolver = new AutofacWebApiDependencyResolver(container);

            config.DependencyResolver = dependencyResolver;

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);

        }

        public static void RegisterMediatr(this ContainerBuilder builder, Func<IContainer> getContainer)
        {
            //Register MediatR
            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).AsImplementedInterfaces();

            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });
            builder.Register<MultiInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
            });
        }

    }
}