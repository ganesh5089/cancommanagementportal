﻿using AutoMapper;
using Cancom.Commands.Client;
using Cancom.Commands.ManageMobileSettings;
using Cancom.Commands.RefreshToken;
using Cancom.Commands.UploadIdTechAccessSoftware;
using Cancom.Commands.UploadLoanForm;
using Cancom.Common;
using Cancom.Data.CFMSv3;
using Cancom.Data.IDTechAccessV2;
using Cancom.Data.LoginDB;
using Cancom.ViewModel;
using Cancom.WebApi.Infrastructure;

namespace Cancom.WebApi
{
    public static class AutoMapperConfig
    {
        public static void Setup()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<User, AuthUser>()
                    .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.UserID))
                    .ForMember(dest => dest.UserName, src => src.ResolveUsing(e => e.Username))
                    .ForMember(dest => dest.FullName, src => src.ResolveUsing(e => e.Name))
                    .ForMember(dest => dest.PermissionSet, src => src.Ignore())
                    .ForMember(dest => dest.TimeZone, src => src.Ignore());

                cfg.CreateMap<RefreshTokenViewModel, SaveRefreshTokenCommand>()
                    .IgnoreCancomMessageProperties();

                EntityToViewModelMap(cfg);
                CommandToEntityMap(cfg);
            });

            Mapper.AssertConfigurationIsValid();
        }

        private static void EntityToViewModelMap(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Client, ClientsViewModel>()
                .ForMember(dest => dest.ClientName, src => src.ResolveUsing(e => e.Name))
                .ForMember(dest => dest.CompanyReg, src => src.ResolveUsing(e => e.ClientID));

            cfg.CreateMap<Product, ProductViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name))
                .ForMember(dest => dest.ClientId, src => src.Ignore());

            cfg.CreateMap<User, UploadLoanFormViewModel>()
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.UserId, src => src.ResolveUsing(e => e.UserID))
                .ForMember(dest => dest.Username, src => src.ResolveUsing(e => e.Username))
                .ForMember(dest => dest.IsAdmin, src => src.ResolveUsing(e => e.isAdmin));

            cfg.CreateMap<client, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.id))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.client_name));

            cfg.CreateMap<CFMS_WEB_getTickets_Result, ViewTicketViewModel>()
                .ForMember(dest => dest.NoticeNo, src => src.ResolveUsing(e => e.Notice_No))
                .ForMember(dest => dest.RaNumber, src => src.ResolveUsing(e => e.RA_Number))
                .ForMember(dest => dest.CarReg, src => src.ResolveUsing(e => e.Car_Reg))
                .ForMember(dest => dest.MetroStatus, src => src.ResolveUsing(e => e.metro_status))
                .ForMember(dest => dest.ViewFine, src => src.ResolveUsing(e => e.View_Fine));

            cfg.CreateMap<sp_select_admin_csspm_view_Result, AccessClientViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.MobileId, src => src.ResolveUsing(e => e.MobileID))
                .ForMember(dest => dest.ProductId, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.StationId, src => src.ResolveUsing(e => e.StationID))
                .ForMember(dest => dest.ClientName, src => src.ResolveUsing(e => e.ClientName))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.MobileName, src => src.ResolveUsing(e => e.MobileName))
                .ForMember(dest => dest.ProductName, src => src.ResolveUsing(e => e.ProductName))
                .ForMember(dest => dest.StationName, src => src.ResolveUsing(e => e.StationName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description));

            cfg.CreateMap<sp_select_admin_csspm_view_Result, ManageMobileSettingsCsspmViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.ClientName, src => src.ResolveUsing(e => e.ClientName))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.MobileName, src => src.ResolveUsing(e => e.MobileName))
                .ForMember(dest => dest.ProductName, src => src.ResolveUsing(e => e.ProductName))
                .ForMember(dest => dest.StationName, src => src.ResolveUsing(e => e.StationName))
                .ForMember(dest => dest.IsDownloadMobileFile, src => src.Ignore());

            cfg.CreateMap<sp_select_admin_csspm_view_csspmid_Result, AccessClientViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.MobileId, src => src.ResolveUsing(e => e.MobileID))
                .ForMember(dest => dest.ProductId, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.StationId, src => src.ResolveUsing(e => e.StationID))
                .ForMember(dest => dest.ClientName, src => src.ResolveUsing(e => e.ClientName))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.MobileName, src => src.ResolveUsing(e => e.MobileName))
                .ForMember(dest => dest.ProductName, src => src.ResolveUsing(e => e.ProductName))
                .ForMember(dest => dest.StationName, src => src.ResolveUsing(e => e.StationName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description));

            cfg.CreateMap<CFMS_WEB_getTickets_AwaitingData_Details_Result, FineManagementPropertyViewModel>()
                .ForMember(dest => dest.NoticeNo, src => src.ResolveUsing(e => e.Notice_No))
                .ForMember(dest => dest.RaNumber, src => src.ResolveUsing(e => e.RA_Number))
                .ForMember(dest => dest.CarRegistration, src => src.ResolveUsing(e => e.Car_Reg))
                .ForMember(dest => dest.Region, src => src.ResolveUsing(e => e.Region))
                .ForMember(dest => dest.OffenceDate, src => src.ResolveUsing(e => e.Offence_Date))
                .ForMember(dest => dest.MetroStatus, src => src.ResolveUsing(e => e.metro_status))
                .ForMember(dest => dest.Status, src => src.ResolveUsing(e => e.Status))
                .ForMember(dest => dest.TicketId, opt => opt.Ignore());

            cfg.CreateMap<sp_select_csspm_clients_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.ClientName));

            cfg.CreateMap<sp_select_csspm_site_by_clientid_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.SiteName));

            cfg.CreateMap<sp_select_csspm_station_by_clientid_siteid_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.StationID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.StationName));

            cfg.CreateMap<sp_select_csspm_product_by_cid_sid_stationid_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.ProductName));

            cfg.CreateMap<sp_select_csspm_mobile_by_cid_sid_stationid_prodid_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.MobileID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.MobileName));

            cfg.CreateMap
                <sp_select_transaction_types_view_portal_Result, TransactionTypeViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.CreationDate, src => src.ResolveUsing(e => e.CreationDate))
                .ForMember(dest => dest.ModifiedDate, src => src.ResolveUsing(e => e.ModifiedDate))
                .ForMember(dest => dest.Active, src => src.ResolveUsing(e => e.Active))
                .ForMember(dest => dest.Csspmid, src => src.ResolveUsing(e => e.CSSPMID))
                .ForMember(dest => dest.ClientId, opt => opt.Ignore())
                .ForMember(dest => dest.StationId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.MobileId, opt => opt.Ignore());

            cfg.CreateMap<sp_select_transaction_types_by_transtypeID_Result, TransactionTypeViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.CreationDate, src => src.ResolveUsing(e => e.CreationDate))
                .ForMember(dest => dest.ModifiedDate, src => src.ResolveUsing(e => e.ModifiedDate))
                .ForMember(dest => dest.Active, src => src.ResolveUsing(e => e.Active))
                .ForMember(dest => dest.Csspmid, src => src.ResolveUsing(e => e.CSSPMID))
                .ForMember(dest => dest.ClientId, opt => opt.Ignore())
                .ForMember(dest => dest.StationId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.MobileId, opt => opt.Ignore());

            cfg.CreateMap<sp_select_info_data_cartype_types_view_Portal_Result, VehicleTypeViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.TransactionTypeId, src => src.ResolveUsing(e => e.TransactionTypeID))
                .ForMember(dest => dest.TransactionName, src => src.ResolveUsing(e => e.TransactionName))
                .ForMember(dest => dest.Csspmid, src => src.ResolveUsing(e => e.CSSPMID))
                .ForMember(dest => dest.ClientId, opt => opt.Ignore())
                .ForMember(dest => dest.ClientName, opt => opt.Ignore())
                .ForMember(dest => dest.StationId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.MobileId, opt => opt.Ignore());

            cfg.CreateMap<sp_select_info_data_cartype_types_id_Result, VehicleTypeViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.SiteName, src => src.ResolveUsing(e => e.SiteName))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.TransactionTypeId, src => src.ResolveUsing(e => e.TransactionTypeID))
                .ForMember(dest => dest.TransactionName, src => src.ResolveUsing(e => e.TransactionName))
                .ForMember(dest => dest.Csspmid, src => src.ResolveUsing(e => e.CSSPMID))
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.ClientName, src => src.ResolveUsing(e => e.ClientName))
                .ForMember(dest => dest.StationId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.MobileId, opt => opt.Ignore());

            cfg.CreateMap<transaction_types, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Description));

            cfg.CreateMap<getUsers_by_clientid_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.UserID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Username));

            cfg.CreateMap<getClientProducts_new_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Name));

            cfg.CreateMap<getAllClient_List_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Name));

            cfg.CreateMap<getClientUserPermissions_Products_List_Result, UserAccessPermission>()
                .ForMember(dest => dest.UserAccessId, src => src.ResolveUsing(e => e.UserAccessID))
                .ForMember(dest => dest.ProductId, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.ProductName, src => src.ResolveUsing(e => e.ProductName))
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.UserId, src => src.ResolveUsing(e => e.UserID));

            cfg.CreateMap<info_data_cartype_types, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Description));

            cfg.CreateMap<admin_products, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.ProductName));

            cfg.CreateMap<sp_select_info_data_cartype_types_view_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Description));

            cfg.CreateMap<formflow_forms, LookupViewModel>() //sp_select_formflow_formsResult
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.FormName))
                .ForMember(dest => dest.Key, opt => opt.Ignore());

            cfg.CreateMap<sp_select_formflow_allocated_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.FormName));

            cfg.CreateMap<sp_web_list_clientsite_webclientID_Result, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.WebClientID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.ClientSite));

            cfg.CreateMap<sp_web_select_transactions_views_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_view_idnumber_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_surname_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_view_registration_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.Id))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_view_idnumber_date_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_view_surname_date_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_view_registration_date_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.Id))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_web_select_transactions_views_date_portal_Result, AccessV2TransactionsViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.ID))
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.Name_and_Surname))
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegNo))
                .ForMember(dest => dest.DriverId, src => src.ResolveUsing(e => e.Driver_ID))
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Site, src => src.ResolveUsing(e => e.Site))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.Date));

            cfg.CreateMap<sp_webGetVehicleLicense_Result, AccessV2TransactionVehicleLicenseViewModel>()
                .ForMember(dest => dest.RegistrationNo, src => src.ResolveUsing(e => e.RegistrationNo))
                .ForMember(dest => dest.VehicleRegistrationNo, src => src.ResolveUsing(e => e.VehicleRegNo))
                .ForMember(dest => dest.Make, src => src.ResolveUsing(e => e.Make))
                .ForMember(dest => dest.Model, src => src.ResolveUsing(e => e.Model))
                .ForMember(dest => dest.Colour, src => src.ResolveUsing(e => e.Colour))
                .ForMember(dest => dest.ExpiryDate, src => src.ResolveUsing(e => e.ExpiryDate))
                .ForMember(dest => dest.VinNo, src => src.ResolveUsing(e => e.Vin))
                .ForMember(dest => dest.EngineNo, src => src.ResolveUsing(e => e.EngineNo))
                .ForMember(dest => dest.LicenseNo, src => src.ResolveUsing(e => e.LicenseNo));

            cfg.CreateMap<sp_select_transactions_information_Result, AccessV2TransactionAdditionalInfoViewModel>()
                .ForMember(dest => dest.Type, src => src.ResolveUsing(e => e.Type))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.Value));

            cfg.CreateMap<loanform, LookupViewModel>()
                .ForMember(dest => dest.Key, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.Value, src => src.ResolveUsing(e => e.LoanFormName));

            cfg.CreateMap<CFMS_fine_admin_list_status_history_portal_Result, FineManagementFineInfoStatusViewModel>()
                .ForMember(dest => dest.Status, src => src.ResolveUsing(e => e.CancomStatus))
                .ForMember(dest => dest.Date, src => src.ResolveUsing(e => e.ModifyDate));

            cfg.CreateMap<CFMS_fine_admin_list_metro_status_history_Result, FineManagementFineImageDetailsViewModel>()
                .ForMember(dest => dest.ImageId, src => src.ResolveUsing(e => e.image_id))
                .ForMember(dest => dest.FirstIssueDate, src => src.ResolveUsing(e => e.first_issue_date))
                .ForMember(dest => dest.CourtDate, src => src.ResolveUsing(e => e.court_date))
                .ForMember(dest => dest.DateRecieved, src => src.ResolveUsing(e => e.date_received))
                .ForMember(dest => dest.ElectronicUpload, opt => opt.Ignore());

            cfg.CreateMap<CFMS_misc_get_person_Result, FineManagementItcInfoViewModel>()
                .ForMember(dest => dest.Surname, src => src.ResolveUsing(e => e.Surname))
                .ForMember(dest => dest.Forename1, src => src.ResolveUsing(e => e.Forename1))
                .ForMember(dest => dest.Dob, src => src.ResolveUsing(e => e.DOB))
                .ForMember(dest => dest.Email, src => src.ResolveUsing(e => e.Email))
                .ForMember(dest => dest.Cell1No, src => src.ResolveUsing(e => e.Cell_1_no))
                .ForMember(dest => dest.Cell2No, src => src.ResolveUsing(e => e.Cell_2_no))
                .ForMember(dest => dest.Cell3No, src => src.ResolveUsing(e => e.Cell_3_no))
                .ForMember(dest => dest.ItcFirstAddr1, src => src.ResolveUsing(e => e.ITC_First_Addr_1))
                .ForMember(dest => dest.ItcFirstAddr2, src => src.ResolveUsing(e => e.ITC_First_Addr_2))
                .ForMember(dest => dest.ItcFirstAddr3, src => src.ResolveUsing(e => e.ITC_First_Addr_3))
                .ForMember(dest => dest.ItcFirstAddr4, src => src.ResolveUsing(e => e.ITC_First_Addr_4))
                .ForMember(dest => dest.ItcSecondAddr1, src => src.ResolveUsing(e => e.ITC_Second_Addr_1))
                .ForMember(dest => dest.ItcSecondAddr2, src => src.ResolveUsing(e => e.ITC_Second_Addr_2))
                .ForMember(dest => dest.ItcSecondAddr3, src => src.ResolveUsing(e => e.ITC_Second_Addr_3))
                .ForMember(dest => dest.ItcSecondAddr4, src => src.ResolveUsing(e => e.ITC_Second_Addr_4))
                .ForMember(dest => dest.ItcThirdAddr1, src => src.ResolveUsing(e => e.ITC_Third_Addr_1))
                .ForMember(dest => dest.ItcThirdAddr2, src => src.ResolveUsing(e => e.ITC_Third_Addr_2))
                .ForMember(dest => dest.ItcThirdAddr3, src => src.ResolveUsing(e => e.ITC_Third_Addr_3))
                .ForMember(dest => dest.ItcThirdAddr4, src => src.ResolveUsing(e => e.ITC_Third_Addr_4))
                .ForMember(dest => dest.ItcFourthAddr1, src => src.ResolveUsing(e => e.ITC_Fourth_Addr_1))
                .ForMember(dest => dest.ItcFourthAddr2, src => src.ResolveUsing(e => e.ITC_Fourth_Addr_2))
                .ForMember(dest => dest.ItcFourthAddr3, src => src.ResolveUsing(e => e.ITC_Fourth_Addr_3))
                .ForMember(dest => dest.ItcFourthAddr4, src => src.ResolveUsing(e => e.ITC_Fourth_Addr_4));

            cfg.CreateMap<retail_return_info, FineManagementRedirectionInfoViewModel>()
                .ForMember(dest => dest.OffenderName, src => src.ResolveUsing(e => e.offender_name))
                .ForMember(dest => dest.OffenderSurname, src => src.ResolveUsing(e => e.offender_surname))
                .ForMember(dest => dest.OffenderId, src => src.ResolveUsing(e => e.offender_id))
                .ForMember(dest => dest.IdDocType, src => src.ResolveUsing(e => e.id_doc_type))
                .ForMember(dest => dest.OffenderCell, src => src.ResolveUsing(e => e.offender_cell))
                .ForMember(dest => dest.OffenderEmail, src => src.ResolveUsing(e => e.offender_email))
                .ForMember(dest => dest.PhysAddr1, src => src.ResolveUsing(e => e.phys_addr1))
                .ForMember(dest => dest.PhysAddr2, src => src.ResolveUsing(e => e.phys_addr2))
                .ForMember(dest => dest.PhysAddr3, src => src.ResolveUsing(e => e.phys_addr3))
                .ForMember(dest => dest.PhysAddr4, src => src.ResolveUsing(e => e.phys_addr4))
                .ForMember(dest => dest.PostAddr1, src => src.ResolveUsing(e => e.post_addr1))
                .ForMember(dest => dest.PostAddr2, src => src.ResolveUsing(e => e.post_addr2))
                .ForMember(dest => dest.PostAddr3, src => src.ResolveUsing(e => e.post_addr3))
                .ForMember(dest => dest.PostAddr4, src => src.ResolveUsing(e => e.post_addr4))
                .ForMember(dest => dest.InfoSource, src => src.ResolveUsing(e => e.info_source))
                .ForMember(dest => dest.RaNumber, src => src.ResolveUsing(e => e.ra_number))
                .ForMember(dest => dest.ClientLicenceNo, src => src.ResolveUsing(e => e.client_licence_no))
                .ForMember(dest => dest.AssRentalComp, src => src.ResolveUsing(e => e.ass_rental_comp))
                .ForMember(dest => dest.AssClientNo, src => src.ResolveUsing(e => e.ass_client_no))
                .ForMember(dest => dest.AssClientName, src => src.ResolveUsing(e => e.ass_client_name))
                .ForMember(dest => dest.BranchCode, src => src.ResolveUsing(e => e.branch_code))
                .ForMember(dest => dest.DateRented, src => src.ResolveUsing(e => e.date_rented))
                .ForMember(dest => dest.TimeRented, src => src.ResolveUsing(e => e.time_rented))
                .ForMember(dest => dest.BranchRented, src => src.ResolveUsing(e => e.branch_rented))
                .ForMember(dest => dest.DateReturned, src => src.ResolveUsing(e => e.date_returned))
                .ForMember(dest => dest.TimeReturned, src => src.ResolveUsing(e => e.time_returned))
                .ForMember(dest => dest.BranchReturned, src => src.ResolveUsing(e => e.branch_returned));

            cfg.CreateMap<SABTransactions_Portal_Result, LateTransactionsViewModel>()
                .ForMember(dest => dest.UploadedTime, src => src.ResolveUsing(e => e.FormattedUploadedTime));

            cfg.CreateMap<SABTransactions_DaysDiff_Portal_Result, LateTransactionsViewModel>()
                .ForMember(dest => dest.UploadedTime, src => src.ResolveUsing(e => e.FormattedUploadedTime));

            cfg.CreateMap<AccessClientSetting, AccessClientSettingsViewModel>();

            cfg.CreateMap<AccessGeneralSetting, AccessGeneralSettingsViewModel>()
                .ForMember(dest => dest.ServerIp, src => src.ResolveUsing(e => e.ServerIP));

            cfg.CreateMap<AccessEnterSetting, AccessEnterSettingsViewModel>();

            cfg.CreateMap<AccessExitSetting, AccessExitSettingsViewModel>();

            cfg.CreateMap<AccessNetworkSetting, AccessNetworkSettingsViewModel>()
                .ForMember(dest => dest.UseSsFeature, src => src.ResolveUsing(e => e.UseSSFeature));

            cfg.CreateMap<AccessFormSetting, AccessFormSettingsViewModel>();

            cfg.CreateMap<AccessOptionsSetting, AccessOptionsSettingsViewModel>();

            cfg.CreateMap<AccessClientSetting, AccessClientSettingsXmlViewModel>()
                .ForMember(dest => dest.AccessVerification, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.BlacklistAlert, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseOldImager, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Impro, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.HoneyComb, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CustomerDocumentVerification, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.NetworkCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.NetworkInterval, opt => opt.NullSubstitute(0))
                .ForMember(dest => dest.NetworkErrorMessage, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.BatteryCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.BatteryWarningPercentage, opt => opt.NullSubstitute(0))
                .ForMember(dest => dest.BatteryInterval, opt => opt.NullSubstitute(0));

            cfg.CreateMap<getCsspmInfoAccordingtoCsspmId_Result, MobileDeviceSettingsXmlViewModel>()
                .ForMember(dest => dest.ClientId, src => src.ResolveUsing(e => e.ClientID))
                .ForMember(dest => dest.SiteId, src => src.ResolveUsing(e => e.SiteID))
                .ForMember(dest => dest.MobileId, src => src.ResolveUsing(e => e.MobileID))
                .ForMember(dest => dest.ProductId, src => src.ResolveUsing(e => e.ProductID))
                .ForMember(dest => dest.StationId, src => src.ResolveUsing(e => e.StationID));

            cfg.CreateMap<sp_select_admin_users_and_roles_new_Result, AdminUserSettingsXmlViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.id))
                .ForMember(dest => dest.UserName, src => src.ResolveUsing(e => e.username))
                .ForMember(dest => dest.Password, src => src.ResolveUsing(e => e.password))
                .ForMember(dest => dest.RoleId, src => src.ResolveUsing(e => e.roleid));

            cfg.CreateMap<AccessGeneralSetting, AccessGeneralSettingsXmlViewModel>()
                .ForMember(dest => dest.ServerIp, src => src.ResolveUsing(e => e.ServerIP));

            cfg.CreateMap<AccessEnterSetting, AccessEnterSettingsXmlViewModel>()
                .ForMember(dest => dest.TransactionType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.TransactionTypeValue, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.VehicleType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.VehicleTypeValue, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.VehicleLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.DriverLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Signature, opt => opt.NullSubstitute(false));

            cfg.CreateMap<AccessExitSetting, AccessExitSettingsXmlViewModel>()
                .ForMember(dest => dest.VehicleType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.VehicleTypeValue, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.VehicleLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.DriverLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Signature, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.ValidateDriver, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.SmsExit, opt => opt.NullSubstitute(false));

            cfg.CreateMap<AccessNetworkSetting, AccessNetworkSettingsXmlViewModel>()
                .ForMember(dest => dest.UseSsFeature, src => src.ResolveUsing(e => e.UseSSFeature.HasValue ? e.UseSSFeature : false))
                .ForMember(dest => dest.Use3G, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Use802Network, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseGps, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UsePhone, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseTranAccess, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Value, opt => opt.NullSubstitute(string.Empty));

            cfg.CreateMap<AccessFormSetting, AccessFormSettingsXmlViewModel>()
                .ForMember(dest => dest.IsCellLength, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CellLengthValue, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.IsCardNumberLength, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CardNumberLengthValue, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.AdvancedNumpad, opt => opt.NullSubstitute(false));

            cfg.CreateMap<AccessOptionsSetting, AccessOptionsSettingsXmlViewModel>()
                .ForMember(dest => dest.VehicleExitPermission, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.StockTracking, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OverRideBudgetFlow, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OnPermissionEntry, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.EnterPermissionDetails, opt => opt.NullSubstitute(string.Empty))
                .ForMember(dest => dest.RacCardCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForDuplicateEntry, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForDuplicateDocs, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForNoumberOfOccupants, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OverrideDriverLicense, opt => opt.NullSubstitute(false));

            cfg.CreateMap<LastMonth_Average_Summary_Report_Result, AverageSummaryReportViewModel>()
                .ForMember(dest => dest.AverageTime, src => src.ResolveUsing(e => e.AVGTIME));

            cfg.CreateMap<CurrentMonth_Average_Summary_Report_Result, AverageSummaryReportViewModel>()
                .ForMember(dest => dest.AverageTime, src => src.ResolveUsing(e => e.AVGTIME));

            cfg.CreateMap<new_get_all_idtechsoftwarerepository_Result, UploadIdTechAccessSoftwareViewModel>()
                .ForMember(dest => dest.Id, src => src.ResolveUsing(e => e.Id))
                .ForMember(dest => dest.FileName, src => src.ResolveUsing(e => e.FileName))
                .ForMember(dest => dest.FileType, src => src.ResolveUsing(e => e.FileType))
                .ForMember(dest => dest.UploadedBy, src => src.ResolveUsing(e => e.UploadedBy))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.CreationTs, src => src.ResolveUsing(e => e.CreationTs))
                .ForMember(dest => dest.Active, src => src.ResolveUsing(e => e.Active));
        }

        private static void CommandToEntityMap(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<SaveClientCommand, User>()
                .ForMember(dest => dest.Name, src => src.ResolveUsing(e => e.ClientName))
                .ForMember(dest => dest.Surname, src => src.UseValue("Admin"))
                .ForMember(dest => dest.Department, src => src.UseValue("ALL"))
                .ForMember(dest => dest.isActive, src => src.UseValue(true))
                .ForMember(dest => dest.ClientID, src => src.ResolveUsing(e => e.ClientId))
                .ForMember(dest => dest.isDisable, src => src.UseValue(false))
                .ForMember(dest => dest.isSuper, src => src.UseValue(false))
                .ForMember(dest => dest.isAdmin, src => src.UseValue(true))
                .ForMember(dest => dest.UserID, opt => opt.Ignore())
                .ForMember(dest => dest.Email, opt => opt.Ignore())
                .ForMember(dest => dest.Client, opt => opt.Ignore());

            cfg.CreateMap<ProductViewModel, ClientProduct>()
                .ForMember(dest => dest.ProductID, src => src.MapFrom(e => e.Id))
                .ForMember(dest => dest.ClientID, src => src.MapFrom(e => e.ClientId))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            cfg.CreateMap<SaveGeneralSettingsCommand, AccessGeneralSetting>()
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveClientSettingsCommand, AccessClientSetting>()
                .ForMember(dest => dest.AccessVerification, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.BlacklistAlert, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseOldImager, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Impro, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.HoneyComb, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CustomerDocumentVerification, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.NetworkCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.BatteryCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveEnterSettingsCommand, AccessEnterSetting>()
                .ForMember(dest => dest.TransactionType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.VehicleType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.VehicleLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.DriverLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Signature, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveExitSettingsCommand, AccessExitSetting>()
                .ForMember(dest => dest.VehicleType, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.VehicleLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.DriverLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Signature, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.ValidateDriver, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.SmsExit, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveNetworkSettingsCommand, AccessNetworkSetting>()
                .ForMember(dest => dest.Use3G, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.Use802Network, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseGps, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UsePhone, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseTranAccess, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.UseSSFeature, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveFormSettingsCommand, AccessFormSetting>()
                .ForMember(dest => dest.IsCellLength, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.IsCardNumberLength, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.AdvancedNumpad, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<SaveOptionsSettingsCommand, AccessOptionsSetting>()
                .ForMember(dest => dest.VehicleExitPermission, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.StockTracking, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OverRideBudgetFlow, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OnPermissionEntry, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.RacCardCheck, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForDuplicateEntry, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForDuplicateDocs, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CheckForNoumberOfOccupants, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.OverrideDriverLicense, opt => opt.NullSubstitute(false))
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedDate, opt => opt.Ignore());

            cfg.CreateMap<UploadIdTechAccessSoftwareCommand, IdTechSoftwareRepository>()
                .ForMember(dest => dest.FileName, src => src.ResolveUsing(e => e.FileName))
                .ForMember(dest => dest.FileType, src => src.ResolveUsing(e => e.FileType))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.Description))
                .ForMember(dest => dest.FileData, src => src.ResolveUsing(e => e.FileData))
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Version, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedUserId, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedUserId, opt => opt.Ignore())
                .ForMember(dest => dest.CreationTs, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedTs, opt => opt.Ignore())
                .ForMember(dest => dest.Active, opt => opt.Ignore());

            cfg.CreateMap<UploadDataViewModel, SaveLoanFormFileCommand>()
                .IgnoreCancomMessageProperties();

            cfg.CreateMap<UploadDataViewModel, UploadIdTechAccessSoftwareCommand>()
                .ForMember(dest => dest.FileName, src => src.ResolveUsing(e => e.FileViewModel.Name))
                .ForMember(dest => dest.FileType, src => src.ResolveUsing(e => e.FormData[0].Value))
                .ForMember(dest => dest.Description, src => src.ResolveUsing(e => e.FormData[1].Value))
                .ForMember(dest => dest.Version, src => src.ResolveUsing(e => e.FormData[2].Value))
                .ForMember(dest => dest.FileData, src => src.ResolveUsing(e => e.FileViewModel.Data))
                .ForMember(dest => dest.User, opt => opt.Ignore())
                .ForMember(dest => dest.LoggedOnUserId, opt => opt.Ignore())
                .ForMember(dest => dest.MessageId, opt => opt.Ignore())
                .ForMember(dest => dest.TimeStamp, opt => opt.Ignore())
                .ForMember(dest => dest.CommandResult, opt => opt.Ignore())
                .ForMember(dest => dest.AuditThisMessage, opt => opt.Ignore());

            cfg.CreateMap<SaveRefreshTokenCommand, PortalRefreshToken>();
        }

        public static IMappingExpression<TSource, TDestination> IgnoreCancomMessageProperties<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mapping) where TDestination : CancomMessage
        {
            return mapping
                 .ForMember(dest => dest.User, opt => opt.Ignore())
                 .ForMember(dest => dest.LoggedOnUserId, opt => opt.Ignore())
                 .ForMember(dest => dest.MessageId, opt => opt.Ignore())
                 .ForMember(dest => dest.TimeStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.CommandResult, opt => opt.Ignore())
                 .ForMember(dest => dest.AuditThisMessage, opt => opt.Ignore());

        }
    }
}