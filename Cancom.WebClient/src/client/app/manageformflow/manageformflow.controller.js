﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('manageFormFlowController', controller);

    controller.$inject = ['manageFormFlowDataService', 'SimpleListScreenViewModel', 'messagingService', 'notificationBarService', '$q'];
    / @ngInject /

    function controller(manageFormFlowDataService, SimpleListScreenViewModel, messagingService, notificationBarService, $q) {

        var vm = this;

        vm.manageFormFlow = {};

        vm.formList = [
            { listName: "Available Forms", items: [], dragging: false },
            { listName: "Allocated Forms in Order", items: [], dragging: false }
        ];

        vm.getSelectedItemsIncluding = function (list, item) {
            angular.forEach(list.items, function (item) { item.selected = false; });
            item.selected = true;
            return list.items.filter(function (item) { return item.selected; });
        };

        vm.onSelectionChangeItem = function (list, item) {
            var previousSelectedName = '';
            angular.forEach(list.items, function (item) {
                if (item.selected === true) {
                    previousSelectedName = item.value;
                }
                item.selected = false;
            });
            item.selected = previousSelectedName !== item.value;

            if (list.listName === 'Allocated Forms in Order') {
                vm.manageFormFlow.formName = item.selected ? item.value : null;
            }
        }

        vm.onDragstart = function (list, event) {
            vm.selectedFormBox = list.listName;
            list.dragging = true;
            if (event.dataTransfer.setDragImage) {
                var img = new Image();
                img.src = 'images/ic_content_copy_black_24dp_2x.png';
                event.dataTransfer.setDragImage(img, 0, 0);
            }
        };

        vm.onDrop = function (list, items, index) {
            if (vm.selectedFormBox !== list.listName) {
                if (list.listName === 'Allocated Forms in Order') {
                    var isAvailable = false;
                    angular.forEach(list.items, function (item) {
                        if (item.value === items[0].value) {
                            isAvailable = true;
                        }
                    });
                    if (!isAvailable) {
                        angular.forEach(items, function (item) { item.selected = false; });
                        list.items = list.items.slice(0, index)
                            .concat(items)
                            .concat(list.items.slice(index));
                        vm.manageFormFlow.newAllocatedForms = items;
                        manageFormFlowDataService.AddFormsToAllocatedForms(vm.manageFormFlow)
                            .then(function (results) {
                                vm.loadAllocatedFormBox('');
                            });
                    } else {
                        notificationBarService.warning('Form already exist');
                    }
                } else {
                    vm.manageFormFlow.removedAllocatedForms = items;
                    manageFormFlowDataService.removeFormsFromAllocatedForms(vm.manageFormFlow)
                        .then(function (results) {
                            vm.loadAllocatedFormBox('');
                        });
                }
            }
            return true;
        }

        vm.onMoved = function (list) {
            angular.forEach(list.items, function (item) { item.selected = false; });
            //Use this function later;
            //if (vm.selectedFormBox === 'Allocated Forms in Order' && list.listName !== 'Allocated Forms in Order') {
            //    list.items = list.items.filter(function(item) { return !item.selected; });
            //}
        };

        vm.getData = function () {
            $q.all([
                    manageFormFlowDataService.getAllClients()
            ])
                .then(function (responses) {
                    vm.allClients = responses[0];
                });
        };

        vm.onClientChange = function () {
            if (vm.manageFormFlow.clientId) {
                manageFormFlowDataService.getAllSitesByClientId(vm.manageFormFlow.clientId)
                    .then(function (results) {
                        vm.allSites = results;
                        vm.allStations = {};
                        vm.manageFormFlow.stationId = null;
                        vm.allProducts = {};
                        vm.manageFormFlow.productId = null;
                        vm.allMobiles = {};
                        vm.manageFormFlow.mobileId = null;
                        vm.allTransactionTypes = {};
                        vm.manageFormFlow.transactionTypeId = null;
                        vm.allVehicleTypes = {};
                        vm.manageFormFlow.vehicleTypeId = null;
                    });
            } else {
                vm.allSites = {};
                vm.manageFormFlow.siteId = null;
                vm.allStations = {};
                vm.manageFormFlow.stationId = null;
                vm.allProducts = {};
                vm.manageFormFlow.productId = null;
                vm.allMobiles = {};
                vm.manageFormFlow.mobileId = null;
                vm.allTransactionTypes = {};
                vm.manageFormFlow.transactionTypeId = null;
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;
            }

            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onSiteChange = function () {
            if (vm.manageFormFlow.siteId) {
                manageFormFlowDataService.getAllStationsBySiteId(vm.manageFormFlow)
                    .then(function (results) {
                        vm.allStations = results;
                        vm.allProducts = {};
                        vm.manageFormFlow.productId = null;
                        vm.allMobiles = {};
                        vm.manageFormFlow.mobileId = null;
                        vm.allTransactionTypes = {};
                        vm.manageFormFlow.transactionTypeId = null;
                        vm.allVehicleTypes = {};
                        vm.manageFormFlow.vehicleTypeId = null;
                    });
            } else {
                vm.allStations = {};
                vm.manageFormFlow.stationId = null;
                vm.allProducts = {};
                vm.manageFormFlow.productId = null;
                vm.allMobiles = {};
                vm.manageFormFlow.mobileId = null;
                vm.allTransactionTypes = {};
                vm.manageFormFlow.transactionTypeId = null;
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;
            }

            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onStationChange = function () {
            if (vm.manageFormFlow.siteId) {
                manageFormFlowDataService.getAllProductsBySiteId(vm.manageFormFlow)
                    .then(function (results) {
                        vm.allProducts = results;
                        vm.allMobiles = {};
                        vm.manageFormFlow.mobileId = null;
                        vm.allTransactionTypes = {};
                        vm.manageFormFlow.transactionTypeId = null;
                        vm.allVehicleTypes = {};
                        vm.manageFormFlow.vehicleTypeId = null;
                    });
            } else {
                vm.allProducts = {};
                vm.manageFormFlow.productId = null;
                vm.allMobiles = {};
                vm.manageFormFlow.mobileId = null;
                vm.allTransactionTypes = {};
                vm.manageFormFlow.transactionTypeId = null;
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;
            }

            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onProductChange = function () {
            if (vm.manageFormFlow.siteId) {
                manageFormFlowDataService.getAllMobilesBySiteId(vm.manageFormFlow)
                    .then(function (results) {
                        vm.allMobiles = results;
                        vm.allTransactionTypes = {};
                        vm.manageFormFlow.transactionTypeId = null;
                        vm.allVehicleTypes = {};
                        vm.manageFormFlow.vehicleTypeId = null;
                    });
            } else {
                vm.allMobiles = {};
                vm.manageFormFlow.mobileId = 0;
                vm.allTransactionTypes = {};
                vm.manageFormFlow.transactionTypeId = null;
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;
            }
            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onMobileChange = function () {
            if (vm.manageFormFlow.mobileId) {
                manageFormFlowDataService.getTransactionTypesByCsspmid(vm.manageFormFlow)
                    .then(function (results) {
                        vm.allTransactionTypes = results;
                        vm.allVehicleTypes = {};
                        vm.manageFormFlow.vehicleTypeId = null;
                    });
            } else {
                vm.allTransactionTypes = {};
                vm.manageFormFlow.transactionTypeId = null;
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;
            }
            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onTransactionTypeChange = function () {
            if (vm.manageFormFlow.transactionTypeId) {
                manageFormFlowDataService.getVehicleTypeListByTransactionId(vm.manageFormFlow)
                    .then(function (results) {
                        vm.allVehicleTypes = results;
                    });
            } else {
                vm.allVehicleTypes = {};
                vm.manageFormFlow.vehicleTypeId = null;

            }
            vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
            ];
        }

        vm.onVehicleTypeChange = function (frm) {
            vm.search(frm);
        }

        vm.loadAllocatedFormBox = function (value) {
            manageFormFlowDataService.getAllManageFormFlowAllocatedForms(vm.manageFormFlow)
                .then(function (results) {
                    angular.forEach(vm.formList, function (list) {
                        if (list.listName === "Allocated Forms in Order") {
                            list.items = [];
                            angular.forEach(results, function (item) {
                                list.items.push(
                                {
                                    value: item.value,
                                    selected: item.value === value
                                });
                            });
                        }
                    });
                });
        }

        vm.onFormMoveUp = function () {
            manageFormFlowDataService.AllocatedFormsMoveUp(vm.manageFormFlow)
                .then(function (results) {
                    vm.loadAllocatedFormBox(vm.manageFormFlow.formName);
                });
        }

        vm.onFormMoveDown = function () {
            manageFormFlowDataService.AllocatedFormsMoveDown(vm.manageFormFlow)
                .then(function (results) {
                    vm.loadAllocatedFormBox(vm.manageFormFlow.formName);
                });
        }

        vm.search = function (frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vm.formList = [
                    { listName: "Available Forms", items: [], dragging: false },
                    { listName: "Allocated Forms in Order", items: [], dragging: false }
                ];
                manageFormFlowDataService.getAllManageFormFlowAvailableForms()
                    .then(function (results) {
                        angular.forEach(vm.formList, function (list) {
                            if (list.listName === "Available Forms") {
                                angular.forEach(results, function (item) {
                                    list.items.push({ value: item.value });
                                });
                            }
                        });
                    });

                vm.loadAllocatedFormBox('');
                vm.manageFormFlow.formName = null;
            }
        };

        function activate() {
            vm.getData();
        }

        activate();

        return vm;
    }
})();