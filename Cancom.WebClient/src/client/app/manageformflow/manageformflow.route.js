﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.manageformflow',
                config: {
                    url: 'ManageFormFlow',
                    templateUrl: 'app/manageformflow/manageformflow.html',
                    controller: 'manageFormFlowController',
                    controllerAs: 'vm',
                    title: 'ManageFormFlow',
                    ncyBreadcrumb: { label: 'ManageFormFlow' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();