﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.useraccesspermission',
                config: {
                    url: 'useraccesspermission',
                    templateUrl: 'app/useraccesspermission/useraccesspermission.html',
                    controller: 'userAccessPermissionController',
                    controllerAs: 'vm',
                    title: 'UserAccessPermission',
                    ncyBreadcrumb: { label: 'UserAccessPermission' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();