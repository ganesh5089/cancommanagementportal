﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userAccessPermissionDetailsController', controller);

    controller.$inject = ['userDataService', 'navigation', 'messagingService', 'notificationBarService', 'id'];
    / @ngInject /

    function controller(userDataService, navigation, messagingService, notificationBarService, id) {

        var vm = this;

        vm.userAccessPermission = {};

        vm.getAllClients = function () {
            userDataService.getAllClients()
                .then(function (results) {
                    vm.allClients = results;
                });
        };

        vm.getUsersByClientid = function () {
            if (vm.userAccessPermission.clientId != null && vm.userAccessPermission.clientId !== 0) {
                userDataService.getUsersByClientid(vm.userAccessPermission.clientId, true)
                    .then(function(result) {
                        vm.allUsers = result;
                        vm.allProducts = {};
                    });
            } else {
                vm.allUsers = {};
                vm.userAccessPermission.userId = 0;
                vm.allProducts = {};
            }
        };

        vm.getAllProductsFromClientUser = function () {
            if (vm.userAccessPermission.userId != null && vm.userAccessPermission.userId !== 0) {
            userDataService.getAllProductsFromClientUser(vm.userAccessPermission.clientId, vm.userAccessPermission.userId, true)
                .then(function (result) {
                    vm.allProducts = result;
                });
            } else {
                vm.allProducts = {};
            }
        };

        vm.save = function (frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vm.userAccessPermission.products = vm.allProducts.filter(function (product) {
                    return product.selected;
                });
                userDataService.saveUserAccess(vm.userAccessPermission)
                    .then(function () {
                        notificationBarService.success('Record added Successfully');
                        navigation.gotToUserAccessPermission();
                    });
            }
        }


        function activate() {
            vm.getAllClients();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }


        activate();

        return vm;
    }
})();