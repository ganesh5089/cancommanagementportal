﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('userAccessPermissionController', controller);

    controller.$inject = ['fineManagementDataService', 'userDataService', 'SimpleListScreenViewModel', 'modalService', 'notificationBarService', 'messagingService'];
    / @ngInject /

    function controller(fineManagementDataService, userDataService, simpleListScreenViewModel, modalService, notificationBarService, messagingService) {

        var vm = new simpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'offencedate';

        vm.getData = function (refresh) {
            messagingService.broadcastCheckFormValidatity();
            if (vm.selectedClient && vm.selectedClient !== 0 && vm.selectedUser && vm.selectedUser !== 0) {
                userDataService.getClientUserPermissions(vm.selectedClient, vm.selectedUser, refresh, vm.dataOperations, vm.filterFn)
                    .then(function (result) {
                        vm.pagedData = result.pagedData;
                        vm.fullCount = result.dataCount;
                        vm.filteredCount = result.filteredDataCount;
                    });
            } else {
                vm.pagedData = null;
                vm.fullCount = null;
                vm.filteredCount = null;
            }
        }

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.productName && datum.productName.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.clientName && datum.clientName.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.getAllClientData = function () {
            userDataService.getAllClients()
                .then(function (result) {
                    vm.allClients = result;
                });
        };

        vm.getUsersByClientid = function () {
            userDataService.getUsersByClientid(vm.selectedClient, true)
                .then(function (result) {
                    vm.user = result;
                });
        };

        vm.delete = function(record) {
            modalService.confirmDelete('Are you sure you want to delete the Product : ' + record.productName + ' ?').result.then(
                function() {
                    userDataService.deleteUserAccess(record).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.getData(true);
                    });
                }, function () {
                });
        };
 
        function activate() {
            vm.getAllClientData();
            vm.getData();
        }



        activate();

        return vm;

    }
})();