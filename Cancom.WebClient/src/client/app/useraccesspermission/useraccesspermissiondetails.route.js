﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.useraccesspermission.useraccesspermissiondetails',
                config: {
                    url: '/{id}',
                    title: 'UserAccessPermissionDetails',
                    ncyBreadcrumb: { label: 'UserAccessPermissionDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/useraccesspermission/useraccesspermissiondetails.html',
                            controller: 'userAccessPermissionDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();