﻿(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardHomeController', controller);

    controller.$inject = ['dashboardDataService', '$timeout'];

    /* @ngInject */
    function controller(dashboardDataService, $timeout) {

        var vm = this;

        vm.getData = function (refresh) {
            vm.getTotalClientCount(refresh);
            vm.getSabTodayLateTransactions(refresh);
            vm.getSabYesterdayLateTransactions(refresh);
            vm.getSabLast7DaysLateTransactions(refresh);
            vm.getCurrentMonthSabTransactionsCount(refresh);
            vm.getPreviousMonthSabTransactionsCount(refresh);
            vm.getCurrentMonthSummaryReport(refresh);
            vm.getPreviousMonthSummaryReport(refresh);
        }

        vm.getTotalClientCount = function (refresh) {
            dashboardDataService.getTotalClientCount(refresh)
              .then(function (response) {
                  vm.allClientsCount = response.totalWebClient;
                  vm.allAccessClientsCount = response.totalAccessClient;
                  vm.activeAccessClientsCount = response.totalActiveAccessClient;
                  vm.inActiveAccessClientsCount = response.totalInActiveAccessClient;
              });
        }

        vm.getSabTodayLateTransactions = function (refresh) {
            dashboardDataService.getSabTodayLateTransactions(refresh)
              .then(function (response) {
                  vm.todayLateTransactions = response.lateTransactions;
                  vm.todayLateTransactionsCount = response.count;
              });
        }

        vm.getSabYesterdayLateTransactions = function (refresh) {
            dashboardDataService.getSabYesterdayLateTransactions(refresh)
              .then(function (response) {
                  vm.yesterdayLateTransactions = response.lateTransactions;
                  vm.yesterdayLateTransactionsCount = response.count;
              });
        }

        vm.getSabLast7DaysLateTransactions = function (refresh) {
            dashboardDataService.getSabLast7DaysLateTransactions(refresh)
              .then(function (response) {
                  vm.last7DaysLateTransactions = response.lateTransactions;
                  vm.last7DaysLateTransactionsCount = response.count;
              });
        }

        
        vm.getCurrentMonthSabTransactionsCount = function (refresh) {
            dashboardDataService.getCurrentMonthSabTransactionsCount(refresh)
                .then(function (response) {
                    vm.currentMonthDonutchartData = [
                        { label: "LateTransaction", value: response.lateTransaction },
                        { label: "OnTimeTransaction", value: response.onTimeTransaction }
                    ];
                    vm.currentMonthSabTransactionsCount = response;
                });
        }

        vm.getPreviousMonthSabTransactionsCount = function (refresh) {
            dashboardDataService.getPreviousMonthSabTransactionsCount(refresh)
                .then(function (response) {
                    vm.previousMonthSabTransactionsCount = response;
                    vm.lastMonthDonutchartData = [
                         { label: "LateTransaction", value: response.lateTransaction },
                         { label: "OnTimeTransaction", value: response.onTimeTransaction }
                    ];
                });
        }

        vm.getCurrentMonthSummaryReport = function(refresh) {
            dashboardDataService.getCurrentMonthSummaryReport(refresh)
                .then(function(response) {
                    vm.currentMonthSummary = response;
                    vm.currentMonthChartDataSummary = response.chartData;
                });
        }

        vm.getPreviousMonthSummaryReport = function (refresh) {
            dashboardDataService.getPreviousMonthSummaryReport(refresh)
                .then(function (response) {
                    vm.previousMonthSummary = response;
                    vm.previousMonthChartDataSummary = response.chartData;
                });
        }

        vm.dataSetOverride =
        {
            backgroundColor: ['#31C0BE', '#E04B4A'],
            hoverBackgroundColor: ['#31C0BE', '#E04B4A'],
            hoverBorderColor: ['#31C0BE', '#E04B4A']
        };

        vm.options = {
            legend: { display: true, position: 'top' },
            responsive: true
        }

        vm.chartColors = ["#c7254e", "#31C0BE"];
        vm.color = ["#31C0BE", "#E04B4A"];

        function activate() {
            vm.getData(false);

        }
       
        activate();

        return vm;

    }
})();