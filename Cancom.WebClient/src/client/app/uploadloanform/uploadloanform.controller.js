﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('uploadLoanFormController', controller);

    controller.$inject = ['uploadLoanFormDataService', 'navigation', 'messagingService', 'notificationBarService', 'FileUploader', 'environment', '$q', 'modalService'];
    / @ngInject /

    function controller(uploadLoanFormDataService, navigation, messagingService, notificationBarService, FileUploader, env, $q, modalService) {
        var vm = this;

        vm.getAllClients = function () {
            uploadLoanFormDataService.getAllClients()
                .then(function (results) {
                    vm.allClients = results;
                });
        };

        vm.search = function (frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                uploadLoanFormDataService.GetUserListByClient(vm.clientId)
                    .then(function (results) {
                        vm.userList = results;
                    });

                uploadLoanFormDataService.GetFileListByClient(vm.clientId)
                    .then(function (results) {
                        vm.fileList = results;
                    });
            }
        }

        vm.fileSelect = function () {
            var df = vm.uploaderFileName;
        }

        function activate() {
            vm.initUploader();
            vm.getAllClients();
        }

        vm.initialInstance = function () {
            var uploader = vm.getFileUploaderInstance();
            uploader.autoUpload = false;
            uploader.removeAfterUpload = true;
            uploader.data = vm.ImportInfo;
            uploader.queueLimit = 1;
            uploader.filters.push({
                name: 'rdlcFilter',
                fn: function (item, options) {

                    var regex = /(?:\.([^.]+))?$/;

                    var extension = regex.exec(item.name)[1];
                    if (extension != null && (extension.toLowerCase() !== 'rdlc')) {
                        return false;
                    }

                    var result = item.size < 4194304; //4MB

                    if (!result) {
                        return result;
                    }

                    result = this.queue.length < 1;

                    return result;
                }
            });

            return uploader;
        };

        vm.getFileUploaderInstance = function () {
            var uploader = vm.uploader = new FileUploader({
                url: env.serverBaseUrl + '/api/uploadloanform/import/'
            });
            // FILTERS
            uploader.filters.push({
                name: 'customFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    return this.queue.length < 10;
                }
            });

            uploader.onErrorItem = function (fileItem, response, status, headers) {
                console.info('onErrorItem', fileItem, response, status, headers);
            };
            
            return uploader;
        }


        vm.initUploader = function () {
            vm.uploader = vm.initialInstance();
            vm.uploader.onSuccessItem = function (item, response) {
                vm.Preloader = false;
                if (response === '') {
                    vm.uploaderFileName = '';
                    notificationBarService.success('File Added Successfully');

                    uploadLoanFormDataService.GetFileListByClient(vm.clientId)
                    .then(function (results) {
                        vm.fileList = results;
                    });
                } else {
                    notificationBarService.success(response);
                }
            };

            vm.uploader.onBeforeUploadItem = function (item) {
                vm.showProgressbar = true;
                item.formData.push({ ClientId: vm.clientId });
            };
            vm.uploader.onProgressAll = function (progress) {

            };

            vm.uploader.onErrorItem = function (item, response, status, headers) {
                vm.Preloader = false;
                vm.uploaderFileName = "";
                document.getElementById("uploadBtn").value = "";
            };

            vm.uploader.onAfterAddingFile = function (fileUploader) {
                vm.uploaderFileName = '';
                vm.uploaderFileName = fileUploader.file.name;
                if (!vm.uploaderFileName) {
                    vm.isEmpty = true;
                    return;
                }
            };
        }

        vm.upload = function () {
            vm.uploader.uploadAll();
        }

        activate();

        return vm;
    }
})();