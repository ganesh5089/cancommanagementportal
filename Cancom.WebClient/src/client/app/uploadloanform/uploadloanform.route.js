﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.uploadloanform',
                config: {
                    url: 'uploadloanform',
                    templateUrl: 'app/uploadloanform/uploadloanform.html',
                    controller: 'uploadLoanFormController',
                    controllerAs: 'vm',
                    title: 'UploadLoanForm',
                    ncyBreadcrumb: { label: 'UploadLoanForm' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();