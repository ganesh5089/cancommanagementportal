﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('viewTicketController', controller);

    controller.$inject = ['fineManagementDataService', 'viewTicketDataService', 'SimpleListScreenViewModel', 'notificationBarService', 'messagingService'];
    / @ngInject /

    function controller(fineManagementDataService, viewTicketDataService, SimpleListScreenViewModel, notificationBarService, messagingService) {
        var vm = new SimpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'noticeno';
        vm.alphabetFilterAttributeName = 'noticeno';

        vm.getData = function (refresh) {
            messagingService.broadcastCheckFormValidatity();
            if (vm.selectedClient && vm.selectedClient !== 0) {
                viewTicketDataService.getAllTicket(vm.selectedClient, vm.referenceNumber, vm.carReg, vm.raNumber, refresh, vm.dataOperations, vm.filterFn)
                    .then(function(result) {
                        vm.pagedData = result.pagedData;
                        vm.fullCount = result.dataCount;
                        vm.filteredCount = result.filteredDataCount;

                        if (vm.pagedData.length === 0) {
                            notificationBarService.warning('No data found !');
                        }
                    });
            } else {
                vm.pagedData = null;
                vm.fullCount = null;
                vm.filteredCount = null;
            }
        }


        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.noticeNo && datum.noticeNo.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.raNumber && datum.raNumber.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.carReg && datum.carReg.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.region && datum.region.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.status && datum.status.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.metroStatus && datum.metroStatus.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.viewFine && datum.viewFine.toLowerCase().contains(lowerCaseSearchTerm));

                  
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.getAllClientData = function () {
            fineManagementDataService.getAllClientsFineManagement()
               .then(function (result) {
                   vm.allClients = result;
               });
        };

        function activate() {
            vm.getAllClientData();
        }

        activate();

        return vm;

    }
})();