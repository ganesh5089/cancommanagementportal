﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.viewticket',
                config: {
                    url: 'viewticket',
                    templateUrl: 'app/viewticket/viewticket.html',
                    controller: 'viewTicketController',
                    controllerAs: 'vm',
                    title: 'ViewTicket',
                    ncyBreadcrumb: { label: 'View Ticket' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();