﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.webclient.webclientdetails',
                config: {
                    url: '/{id}',
                    title: 'webClientDetails',
                    ncyBreadcrumb: { label: 'WebClientDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/webclient/webclientdetails.html',
                            controller: 'webClientDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();