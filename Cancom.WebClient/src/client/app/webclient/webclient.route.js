﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.webclient',
                config: {
                    url: 'webclient',
                    templateUrl: 'app/webclient/webclient.html',
                    controller: 'webClientController',
                    controllerAs: 'vm',
                    title: 'WebClient',
                    ncyBreadcrumb: { label: 'WebClient' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();