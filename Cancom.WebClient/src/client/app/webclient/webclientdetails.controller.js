﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('webClientDetailsController', controller);

    controller.$inject = ['clientDataService', 'navigation', 'messagingService', 'notificationBarService', 'id', '$q'];
    / @ngInject /

    function controller(clientDataService, navigation, messagingService, notificationBarService, id, $q) {

        var vm = this;

        vm.client = {};

        vm.client.clientId = parseInt(id);

        vm.getAllClientsFromCFMS = function () {
            clientDataService.getAllClientsFromCFMS().then(function (results) {
                vm.allClients = results;
            });
        }

        vm.onClientChange = function () {
            if (vm.selectedClient) {
                vm.client.clientId = vm.selectedClient;
                clientDataService.getClientDetails(vm.selectedClient)
                    .then(function (results) {
                        vm.client = results;
                        vm.client.password = "Admin";
                        vm.client.username = "admin" + vm.client.clientName;
                        angular.forEach(results.products, function (product) {
                            angular.forEach(vm.allProducts, function (allProduct) {
                                if (product.name === allProduct.name) {
                                    allProduct.selected = true;
                                }
                            });
                        });
                    });
            } else {
                vm.client = {};
                angular.forEach(vm.allProducts, function(allProduct) {
                    allProduct.selected = false;
                });
            }
        };

        vm.getAllProducts = function () {
            clientDataService.getAllProducts()
                .then(function (productResults) {
                    vm.allProducts = productResults;
                });
        };

        vm.save = function (client, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vm.client.products = vm.allProducts.filter(function (product) {
                    product.clientId = vm.client.clientId;
                    return product.selected;
                });
                clientDataService.saveClient(client)
                    .then(function () {
                        notificationBarService.success('Record added Successfully');
                        navigation.gotToClient();
                    });
            }
        }


        function activate() {
            vm.getAllProducts();
            //vm.getClientDetails();
            vm.getAllClientsFromCFMS();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }


        activate();

        return vm;
    }
})();