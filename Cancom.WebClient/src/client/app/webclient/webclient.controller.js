﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('webClientController', controller);

    controller.$inject = ['clientDataService', 'SimpleListScreenViewModel', 'modalService', 'notificationBarService'];
    / @ngInject /
    function controller(clientDataService, SimpleListScreenViewModel, modalService, notificationBarService) {

        var vm = new SimpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'clientname';
        vm.dataOperations.sortOrder = false;

        vm.getData = function (refresh) {
            clientDataService.getAllClients(refresh, vm.dataOperations, vm.filterFn)
                .then(function (result) {
                    vm.pagedData = result.pagedData;
                    vm.fullCount = result.dataCount;
                    vm.filteredCount = result.filteredDataCount;
                });
        };

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.clientName && datum.clientName.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.companyReg && datum.companyReg.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.delete = function (client) {
            modalService.confirmDelete('Are you sure you want to delete the Client : ' + client.clientName + '?').result.then(
                function () {
                    clientDataService.deleteClient(client.companyReg).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.getData(true);
                    });

                },
                function () {
                });
        };

        function activate() {
            vm.searchBy(vm.dataOperations.search);
        }

        activate();

        return vm;

    }
})();