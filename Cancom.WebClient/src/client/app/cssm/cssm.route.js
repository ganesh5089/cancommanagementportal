﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.cssm',
                config: {
                    url: 'cssm',
                    templateUrl: 'app/cssm/cssm.html',
                    controller: 'cssmController',
                    controllerAs: 'vm',
                    title: 'Cssm',
                    ncyBreadcrumb: { label: 'Cssm' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();