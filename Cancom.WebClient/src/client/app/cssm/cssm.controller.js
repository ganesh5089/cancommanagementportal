﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('cssmController', controller);

    controller.$inject = ['cssmDataService', 'messagingService', 'notificationBarService', '$q', 'modalService'];
    / @ngInject /

    function controller(cssmDataService, messagingService, notificationBarService, $q, modalService) {
        var vm = this;

        vm.groups = [
            {
                title: "Room1",
                content: "Dynamic Group Body - 1"
            },
            {
                title: "Room2",
                content: "Dynamic Group Body - 2"
            }
        ];

        vm.getAllClients = function () {
            cssmDataService.getAllClients()
                .then(function (results) {
                    vm.allClients = results;
                });
        };

        vm.search = function (frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                cssmDataService.getCssmTreeListByClientId(vm.clientId)
                    .then(function (results) {
                        vm.cssmTreeList = results;

                        if (vm.cssmTreeList.levels.length === 0) {
                            notificationBarService.warning('No data found !');
                        }
                    });
            }
        };

        vm.onTreeAdd = function (clientName, siteName, stationName, actionName) {
            modalService.cssmModal("CSSM Details - Add " + actionName, false, clientName, siteName, stationName, actionName).result.then(
                function() {
                    cssmDataService.getCssmTreeListByClientId(vm.clientId)
                        .then(function(results) {
                            vm.cssmTreeList = results;
                        });
                }, function() {
                });
        };

        vm.onTreeDelete = function (csspmid, action, message, frm) {
            modalService.confirmDelete('Are you sure you want to delete this ' + action + '? ' + message).result.then(
                function () {
                    cssmDataService.deleteCssm(csspmid).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.search(frm);
                    });
                }, function () {
                });
        }

        function activate() {
            vm.getAllClients();
        }

        activate();

        return vm;
    }
})();