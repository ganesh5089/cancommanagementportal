﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('cssmDetailsController', controller);

    controller.$inject = ['cssmDataService', 'messagingService', 'notificationBarService', '$uibModalInstance', 'title', 'includeDangerHeader', 'clientName', 'siteName', 'stationName', 'actionName'];
    / @ngInject /

    function controller(cssmDataService, messagingService, notificationBarService, $uibModalInstance, title, includeDangerHeader, clientName, siteName, stationName, actionName) {

        var vm = this;

        vm.cssm = {};

        vm.getAllProducts = function () {
            cssmDataService.getAllProducts()
                .then(function (productResults) {
                    vm.allProducts = productResults;
                });
        };

        vm.save = function (frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vm.cssm.csspmid = 0;
                cssmDataService.saveCssm(vm.cssm)
                    .then(function () {
                        notificationBarService.success('Record added Successfully');
                        $uibModalInstance.close();
                    });
            }
        }

        function activate() {
            vm.title = title;
            vm.includeDangerHeader = includeDangerHeader;
            vm.cssm.clientName = clientName;
            vm.cssm.siteName = siteName;
            vm.cssm.stationName = stationName;
            vm.cssm.actionName = actionName;
            vm.getAllProducts();
        }

        vm.cancel = function () {
            $uibModalInstance.dismiss();
        }

        activate();

        return vm;
    }
})();