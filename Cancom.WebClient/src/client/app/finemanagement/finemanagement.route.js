﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.finemanagement',
                config: {
                    url: 'finemanagement',
                    templateUrl: 'app/finemanagement/finemanagement.html',
                    controller: 'fineManagementController',
                    controllerAs: 'vm',
                    title: 'FineManagement',
                    ncyBreadcrumb: { label: 'FineManagement' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();