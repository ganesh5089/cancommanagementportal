﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('fineManagementController', controller);

    controller.$inject = ['fineManagementDataService', 'SimpleListScreenViewModel', 'modalService', 'notificationBarService', 'messagingService'];
    / @ngInject /

    function controller(fineManagementDataService, simpleListScreenViewModel, modalService, notificationBarService, messagingService) {

        var vm = new simpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'offencedate';

        vm.getData = function (refresh) {
            messagingService.broadcastCheckFormValidatity();
            if (vm.selectedClient && vm.selectedClient !== 0)
            {
                fineManagementDataService.getFineManagementDetailsBySearch(vm.selectedClient, vm.carRegistration, vm.referenceNo, vm.searchCriteria, refresh, vm.dataOperations, vm.filterFn)
                    .then(function (result) {
                        vm.pagedData = result.pagedData;
                        vm.fullCount = result.dataCount;
                        vm.filteredCount = result.filteredDataCount;

                        if (vm.pagedData.length === 0) {
                            notificationBarService.warning('No data found !');
                        }
                    });
            }
            else {
                vm.pagedData = null;
                vm.fullCount = null;
                vm.filteredCount = null;
            }
        }

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.noticeNo && datum.noticeNo.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.raNumber && datum.raNumber.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.carReg && datum.carReg.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.region && datum.region.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.status && datum.status.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.metroStatus && datum.metroStatus.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.viewFine && datum.viewFine.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.getAllClientData = function () {
            fineManagementDataService.getAllClientsFineManagement()
                .then(function (result) {
                    vm.allClients = result;
                });
        };

        vm.finemanagementview = function (id) {
            modalService.fineManagementViewModal("Fine Management View", false, id).result.then(function () {
            }, function () {
            });
        };

        function activate() {
            vm.getAllClientData();
            vm.getData();
        }

        activate();

        return vm;

    }
})();