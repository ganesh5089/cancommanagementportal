﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('fineManagementViewController', controller);

    controller.$inject = ['fineManagementDataService', 'notificationBarService', 'id', '$uibModalInstance', 'title', 'includeDangerHeader'];
    / @ngInject /
    function controller(fineManagementDataService, notificationBarService, id, $uibModalInstance, title, includeDangerHeader) {

        var vm = this;
        var ticketId = parseInt(id);
        vm.title = title;
        vm.includeDangerHeader = includeDangerHeader;

        vm.getFineManagementViewDetails = function () {
            fineManagementDataService.getFineManagementViewDetails(ticketId)
                .then(function (results) {
                    vm.fineInfo = results.fineInfo;
                    vm.fineImageDetails = results.fineImages.fineImageDetailList;
                    vm.returnInfo = results.returnInfo;
                    vm.redirectionInfo = results.redirectionInfo;
                    vm.itcInfo = results.itcInfo;
                });

        };

        function activate() {
            vm.getFineManagementViewDetails();
        }

        vm.viewPdfImage = function (fineId) {
            fineManagementDataService.downloadFinePdf(fineId).then(function (dataUrl) {
                vm.pdfUrl = dataUrl;
            });
        }

        vm.cancel = function () {
            $uibModalInstance.dismiss();
        }

        activate();

        return vm;
    }
})();