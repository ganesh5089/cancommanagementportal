(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('shellController', shellController);

    shellController.$inject = ['session', 'authService', 'environment', '$q', 'navigation', 'notificationBarService', 'modalService'];
    /* @ngInject */
    /**
     * NotificationBarService:
  
     * DO NOT remove this dependency EVEN though it is not used directly in this controller, 
     * long story but it enables notifications appearing for child controllers that do not depend directly on NotificationBarService
     
     */
    function shellController(session, authService, env, $q, navigation, notificationBarService, modalService) {
        var vm = this;

        vm.isAuthenticated = authService.isAuthenticated;
        vm.getUsername = function () { return session.username; };
        //vm.logout = authService.logOut;
        vm.environment = env.environment;
        vm.version = env.version;
        vm.endpoint = env.serverBaseUrl;
        vm.showEnvDetails = env.showEnvDetails;
        vm.versionNotificationModalShowing = false;

        function activate() {
            vm.isOnLoginScreen = navigation.isOnLoginScreen;
        }

        vm.logout = function () {
            var message = "<p>Are you sure you want to log out?</p> <p>Press No if you want to continue work. Press Yes to logout current user.</p>";
            modalService.questionModal('Logout Confirmation', message, true).result.then(function () {
                authService.logOut();
            }, function () {
            });
        };

        $(".x-navigation-minimize").click(function () {
            if ($(".page-sidebar .x-navigation").hasClass("x-navigation-minimized")) {
                $(".page-container").removeClass("page-navigation-toggled");
                x_navigation_minimize("open");
            } else {
                $(".page-container").addClass("page-navigation-toggled");
                x_navigation_minimize("close");
            }

            onresize();

            return false;
        });

        function x_navigation_minimize(action) {

            if (action == 'open') {
                $(".page-container").removeClass("page-container-wide");
                $(".page-sidebar .x-navigation").removeClass("x-navigation-minimized");
                $(".x-navigation-minimize").find(".fa").removeClass("fa-indent").addClass("fa-dedent");
                //$(".page-sidebar.scroll").mCustomScrollbar("update");
            }

            if (action == 'close') {
                $(".page-container").addClass("page-container-wide");
                $(".page-sidebar .x-navigation").addClass("x-navigation-minimized");
                $(".x-navigation-minimize").find(".fa").removeClass("fa-dedent").addClass("fa-indent");
                // $(".page-sidebar.scroll").mCustomScrollbar("disable", true);
            }

            $(".x-navigation li.active").removeClass("active");

        }

        function page_content_onresize() {
            $(".page-content,.content-frame-body,.content-frame-right,.content-frame-left").css("width", "").css("height", "");

            var content_minus = 0;
            content_minus = ($(".page-container-boxed").length > 0) ? 40 : content_minus;
            content_minus += ($(".page-navigation-top-fixed").length > 0) ? 50 : 0;

            var content = $(".page-content");
            var sidebar = $(".page-sidebar");

            if (content.height() < $(document).height() - content_minus) {
                content.height($(document).height() - content_minus);
            }

            if (sidebar.height() > content.height()) {
                content.height(sidebar.height());
            }

            if ($(window).width() > 1024) {

                if ($(".page-sidebar").hasClass("scroll")) {
                    if ($("body").hasClass("page-container-boxed")) {
                        var doc_height = $(document).height() - 40;
                    } else {
                        var doc_height = $(window).height();
                    }
                    $(".page-sidebar").height(doc_height);

                }

                if ($(".content-frame-body").height() < $(document).height() - 162) {
                    $(".content-frame-body,.content-frame-right,.content-frame-left").height($(document).height() - 162);
                } else {
                    $(".content-frame-right,.content-frame-left").height($(".content-frame-body").height());
                }

                $(".content-frame-left").show();
                $(".content-frame-right").show();
            } else {
                $(".content-frame-body").height($(".content-frame").height() - 80);

                if ($(".page-sidebar").hasClass("scroll"))
                    $(".page-sidebar").css("height", "");
            }

            if ($(window).width() < 1200) {
                if ($("body").hasClass("page-container-boxed")) {
                    $("body").removeClass("page-container-boxed").data("boxed", "1");
                }
            } else {
                if ($("body").data("boxed") === "1") {
                    $("body").addClass("page-container-boxed").data("boxed", "");
                }
            }
        }

        function onresize(timeout) {
            timeout = timeout ? timeout : 200;

            setTimeout(function () {
                page_content_onresize();
            }, timeout);
        }

        $(".x-navigation-control").click(function () {
            $(this).parents(".x-navigation").toggleClass("x-navigation-open");

            onresize();

            return false;
        });

        if ($(".page-navigation-toggled").length > 0) {
            x_navigation_minimize("close");
        }

        $(".x-navigation  li > a").click(function () {

            var li = $(this).parent('li');
            var ul = li.parent("ul");

            ul.find(" > li").not(li).removeClass("active");

        });

        $(".x-navigation li").click(function (event) {
            event.stopPropagation();

            var li = $(this);

            if (li.children("ul").length > 0 || li.children(".panel").length > 0 || $(this).hasClass("xn-profile") > 0) {
                if (li.hasClass("active")) {
                    li.removeClass("active");
                    li.find("li.active").removeClass("active");
                } else
                    li.addClass("active");

                onresize();

                if ($(this).hasClass("xn-profile") > 0)
                    return true;
                else
                    return false;
            }
        });


        activate();
    }
})();
