(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', controller);

    controller.$inject = ['authService', 'navigation', 'messagingService'];
    /* @ngInject */
    function controller(authService, navigation, messagingService) {
        var vm = this;// jshint ignore:line

        vm.credentials = {
            username: '',
            password: ''
        };

        vm.login = function (credentials, form) {
            messagingService.broadcastCheckFormValidatity();

            if (!form.$invalid) {
                authService.login(credentials).then(function () {
                    messagingService.broadcastLoginSuccess();
                    navigation.gotToDashboard();
                });
            }
        };

        function activate() { }

        activate();
    }
})();
