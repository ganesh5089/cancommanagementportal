﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.vehicletype.vehicletypedetails',
                config: {
                    url: '/{id}',
                    title: 'VehicleTypeDetails',
                    ncyBreadcrumb: { label: 'VehicleTypeDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/vehicletype/vehicletypedetails.html',
                            controller: 'vehicleTypeDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();