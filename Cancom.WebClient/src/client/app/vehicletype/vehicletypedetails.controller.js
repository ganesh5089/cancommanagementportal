﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('vehicleTypeDetailsController', controller);

    controller.$inject = ['vehicleTypeDataService', 'navigation', 'messagingService', 'notificationBarService', 'id', '$q'];
    / @ngInject /

    function controller(vehicleTypeDataService, navigation, messagingService, notificationBarService, id, $q) {

        var vm = this;

        vm.vehicleType = {};

        vm.vehicleType.id = parseInt(id);

        vm.getData = function () {
            $q.all([
                    vehicleTypeDataService.getAllClients()
            ])
                .then(function (responses) {
                    vm.allClients = responses[0];
                    vm.getVehicleTypeDetails();
                });
        };

        vm.getVehicleTypeDetails = function () {
            if (vm.vehicleType.id !== 0) {
                vehicleTypeDataService.getVehicleTypeDetails(vm.vehicleType.id)
                    .then(function (results) {
                        vm.vehicleType = results;
                    });
            }
        };

        vm.save = function (vehicleType, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vehicleTypeDataService.saveVehicleType(vehicleType)
                    .then(function () {
                        notificationBarService.success('Record added Successfully');
                        navigation.gotToVehicleType();
                    });
            }
        }

        vm.onClientChange = function () {
            if (vm.vehicleType.clientId) {
                vehicleTypeDataService.getAllSitesByClientId(vm.vehicleType.clientId)
                    .then(function (results) {
                        vm.allSites = results;
                        vm.allStations = {};
                        vm.vehicleType.stationId = 0;
                        vm.allProducts = {};
                        vm.vehicleType.productId = 0;
                        vm.allMobiles = {};
                        vm.vehicleType.mobileId = 0;
                    });
            } else {
                vm.allSites = {};
                vm.vehicleType.siteId = 0;
                vm.allStations = {};
                vm.vehicleType.stationId = 0;
                vm.allProducts = {};
                vm.vehicleType.productId = 0;
                vm.allMobiles = {};
                vm.vehicleType.mobileId = 0;
            }
        }

        vm.onSiteChange = function (siteId) {
            if (vm.vehicleType.siteId) {
                vehicleTypeDataService.getAllStationsBySiteId(vm.vehicleType)
                    .then(function (results) {
                        vm.allStations = results;
                        vm.allProducts = {};
                        vm.vehicleType.productId = 0;
                        vm.allMobiles = {};
                        vm.vehicleType.mobileId = 0;
                    });
            } else {
                vm.allStations = {};
                vm.vehicleType.stationId = 0;
                vm.allProducts = {};
                vm.vehicleType.productId = 0;
                vm.allMobiles = {};
                vm.vehicleType.mobileId = 0;
            }
        }

        vm.onStationChange = function (siteId) {
            if (vm.vehicleType.siteId) {
                vehicleTypeDataService.getAllProductsBySiteId(vm.vehicleType)
                    .then(function (results) {
                        vm.allProducts = results;
                        vm.allMobiles = {};
                        vm.vehicleType.mobileId = 0;
                    });
            } else {
                vm.allProducts = {};
                vm.vehicleType.productId = 0;
                vm.allMobiles = {};
                vm.vehicleType.mobileId = 0;
            }
        }

        vm.onProductChange = function (siteId) {
            if (vm.vehicleType.siteId) {
                vehicleTypeDataService.getAllMobilesBySiteId(vm.vehicleType)
                    .then(function (results) {
                        vm.allMobiles = results;
                        vm.allTransactionTypes = {};
                        vm.vehicleType.transactionTypeId = 0;
                    });
            } else {
                vm.allMobiles = {};
                vm.vehicleType.mobileId = 0;
                vm.allTransactionTypes = {};
                vm.vehicleType.transactionTypeId = 0;
            }
        }

        vm.onMobileChange = function () {
            if (vm.vehicleType.siteId) {
                vehicleTypeDataService.getTransactionTypesByCsspmid(vm.vehicleType)
                    .then(function (results) {
                        vm.allTransactionTypes = results;
                    });
            } else {
                vm.allTransactionTypes = {};
                vm.vehicleType.transactionTypeId = 0;
            }
        }

        function activate() {
            vm.getData();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }


        activate();

        return vm;
    }
})();