﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('vehicleTypeController', controller);

    controller.$inject = ['vehicleTypeDataService', 'SimpleListScreenViewModel', 'navigation', 'modalService', 'notificationBarService'];
    / @ngInject /
    function controller(vehicleTypeDataService, SimpleListScreenViewModel, navigation, modalService, notificationBarService) {
        var vm = new SimpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'sitename';
        vm.alphabetFilterAttributeName = 'sitename';

        vm.getData = function (refresh) {
            vehicleTypeDataService.getAllVehicleType(refresh, vm.dataOperations, vm.filterFn)
                .then(function (result) {
                    vm.pagedData = result.pagedData;
                    vm.fullCount = result.dataCount;
                    vm.filteredCount = result.filteredDataCount;
                });
        };

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.siteName && datum.siteName.toLowerCase().contains(lowerCaseSearchTerm)) ||
                        (datum.transactionName && datum.transactionName.toLowerCase().contains(lowerCaseSearchTerm)) ||
                        (datum.siteId && datum.siteId.toString().toLowerCase().contains(lowerCaseSearchTerm)) ||
                        (datum.csspmid && datum.csspmid.toString().toLowerCase().contains(lowerCaseSearchTerm)) ||
                        (datum.description && datum.description.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.delete = function (vehicleType) {
            modalService.confirmDelete('Are you sure you want to delete the Vehicle Type : ' + vehicleType.siteName + '?').result.then(
                function () {
                    vehicleTypeDataService.deleteVehicleType(vehicleType.id).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.getData(true);
                    });
                },
                function () {
                });
        };

        vm.vehicleTypeEdit = function (id) {
            navigation.gotToVehicleTypeDetails(id);
        };

        function activate() {
            vm.searchBy(vm.dataOperations.search);
        }

        activate();

        return vm;
    }
})();