﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.vehicletype',
                config: {
                    url: 'vehicletype',
                    templateUrl: 'app/vehicletype/vehicletype.html',
                    controller: 'vehicleTypeController',
                    controllerAs: 'vm',
                    title: 'VehicleType',
                    ncyBreadcrumb: { label: 'VehicleType' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();