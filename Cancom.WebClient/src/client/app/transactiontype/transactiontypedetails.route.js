﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.transactiontype.transactiontypedetails',
                config: {
                    url: '/{id}',
                    title: 'TransactionTypeDetails',
                    ncyBreadcrumb: { label: 'TransactionTypeDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/transactiontype/transactiontypedetails.html',
                            controller: 'transactionTypeDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();