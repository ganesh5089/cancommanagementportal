﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.transactiontype',
                config: {
                    url: 'transactiontype',
                    templateUrl: 'app/transactiontype/transactiontype.html',
                    controller: 'transactionTypeController',
                    controllerAs: 'vm',
                    title: 'TransactionType',
                    ncyBreadcrumb: { label: 'TransactionType' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();