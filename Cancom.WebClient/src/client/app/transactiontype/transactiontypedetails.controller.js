﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('transactionTypeDetailsController', controller);

    controller.$inject = ['transactionTypeDataService', 'navigation', 'messagingService', 'notificationBarService', 'id', '$q'];
    / @ngInject /

    function controller(transactionTypeDataService, navigation, messagingService, notificationBarService, id, $q) {

        var vm = this;

        vm.transactionType = {};

        vm.transactionType.id = parseInt(id);

        vm.getData = function () {
            $q.all([
                    transactionTypeDataService.getAllClients()
            ])
                .then(function (responses) {
                    vm.allClients = responses[0];
                    vm.getTransactionTypeDetails();
                });
        };

        vm.getTransactionTypeDetails = function () {
            if (vm.transactionType.id !== 0) {
                transactionTypeDataService.getTransactionTypeDetails(vm.transactionType.id)
                    .then(function (results) {
                        var transactionTypeId = vm.transactionType.id;
                        vm.transactionType = results;
                        if (transactionTypeId > 0 && results == null) {
                            vm.transactionType = {};
                            vm.transactionType.id = transactionTypeId;
                        }
                    });
            }
        };

        vm.save = function (transactionType, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                transactionTypeDataService.saveTransactionType(transactionType)
                    .then(function () {
                        notificationBarService.success('Record added Successfully');
                        navigation.gotToTransactionType();
                    });
            }
        }

        vm.onClientChange = function () {
            if (vm.transactionType.clientId) {
                transactionTypeDataService.getAllSitesByClientId(vm.transactionType.clientId)
                        .then(function (results) {
                            vm.allSites = results;
                            vm.allStations = {};
                            vm.transactionType.stationId = 0;
                            vm.allProducts = {};
                            vm.transactionType.productId = 0;
                            vm.allMobiles = {};
                            vm.transactionType.mobileId = 0;
                        });
            } else {
                vm.allSites = {};
                vm.transactionType.siteId = 0;
                vm.allStations = {};
                vm.transactionType.stationId = 0;
                vm.allProducts = {};
                vm.transactionType.productId = 0;
                vm.allMobiles = {};
                vm.transactionType.mobileId = 0;
            }
        }

        vm.onSiteChange = function () {
            if (vm.transactionType.siteId) {
                transactionTypeDataService.getAllStationsBySiteId(vm.transactionType)
                    .then(function (results) {
                        vm.allStations = results;
                        vm.allProducts = {};
                        vm.transactionType.productId = 0;
                        vm.allMobiles = {};
                        vm.transactionType.mobileId = 0;
                    });
            } else {
                vm.allStations = {};
                vm.transactionType.stationId = 0;
                vm.allProducts = {};
                vm.transactionType.productId = 0;
                vm.allMobiles = {};
                vm.transactionType.mobileId = 0;
            }
        }

        vm.onStationChange = function () {
            if (vm.transactionType.siteId) {
                transactionTypeDataService.getAllProductsBySiteId(vm.transactionType)
                    .then(function (results) {
                        vm.allProducts = results;
                        vm.allMobiles = {};
                        vm.transactionType.mobileId = 0;
                    });
            } else {
                vm.allProducts = {};
                vm.transactionType.productId = 0;
                vm.allMobiles = {};
                vm.transactionType.mobileId = 0;
            }
        }

        vm.onProductChange = function () {
            if (vm.transactionType.siteId) {
                transactionTypeDataService.getAllMobilesBySiteId(vm.transactionType)
                    .then(function (results) {
                        vm.allMobiles = results;
                    });
            } else {
                vm.allMobiles = {};
                vm.transactionType.mobileId = 0;
            }
        }

        function activate() {
            vm.getData();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }

        activate();

        return vm;
    }
})();