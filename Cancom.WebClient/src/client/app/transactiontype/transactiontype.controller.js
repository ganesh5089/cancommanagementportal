﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('transactionTypeController', controller);

    controller.$inject = ['transactionTypeDataService', 'SimpleListScreenViewModel', 'navigation', 'modalService', 'notificationBarService'];
    / @ngInject /
    function controller(transactionTypeDataService, SimpleListScreenViewModel, navigation, modalService, notificationBarService) {
        var vm = new SimpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'sitename';
        vm.alphabetFilterAttributeName = 'sitename';

        vm.getData = function (refresh) {
            transactionTypeDataService.getAllTransactionType(refresh, vm.dataOperations, vm.filterFn)
                .then(function (result) {
                    vm.pagedData = result.pagedData;
                    vm.fullCount = result.dataCount;
                    vm.filteredCount = result.filteredDataCount;
                });
        };

        vm.searchBy = function(search) {
            if (search && search.length > 0) {
                vm.filterFn = function(datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.siteName && datum.siteName.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.description && datum.description.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.csspmid && datum.csspmid.toString().toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.delete = function (transactionType) {
            modalService.confirmDelete('Are you sure you want to delete the Transaction Type : ' + transactionType.siteName + '?').result.then(
                function () {
                    transactionTypeDataService.deleteTransactionType(transactionType.id).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.getData(true);
                    });
                },
                function () {
                });
        };

        vm.transactionTypeEdit = function (id) {
            navigation.gotToTransactionTypeDetails(id);
        };

        function activate() {
            vm.searchBy(vm.dataOperations.search);
        }

        activate();

        return vm;
    }
})();