﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.accessclient',
                config: {
                    url: 'accessclient',
                    templateUrl: 'app/accessclient/accessclient.html',
                    controller: 'accessClientController',
                    controllerAs: 'vm',
                    title: 'AccessClient',
                    ncyBreadcrumb: { label: 'AccessClient' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();