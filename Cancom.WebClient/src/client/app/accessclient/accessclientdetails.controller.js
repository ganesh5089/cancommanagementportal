﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('accessClientDetailsController', controller);

    controller.$inject = ['accessClientDataService', 'navigation', 'messagingService', 'notificationBarService', 'id', '$q'];
    / @ngInject /

    function controller(accessClientDataService, navigation, messagingService, notificationBarService, id, $q) {

        var vm = this;

        vm.accessClient = {};

        vm.csspmid = parseInt(id);

        vm.getData = function() {
            $q.all([
                    accessClientDataService.getAllProducts()
                ])
                .then(function (responses) {
                    vm.allProducts = responses[0];
                    vm.getAccessClientDetails();
                });
        };

        vm.getAccessClientDetails = function() {
            if (vm.csspmid !== 0) {
                accessClientDataService.getAccessClientDetails(vm.csspmid)
                    .then(function (results) {
                        vm.accessClient = results;
                    });
            }
        };

        vm.save = function (accessClient, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                accessClientDataService.saveAccessClient(accessClient)
                    .then(function() {
                        notificationBarService.success('Record added Successfully');
                        navigation.gotToAccessClient();
                    });
            }
        }

        function activate() {
            vm.getData();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }

        activate();

        return vm;
    }
})();