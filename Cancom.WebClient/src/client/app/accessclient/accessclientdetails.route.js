﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.accessclient.accessclientdetails',
                config: {
                    url: '/{id}',
                    title: 'AccessClientDetails',
                    ncyBreadcrumb: { label: 'AccessClientDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/accessclient/accessclientdetails.html',
                            controller: 'accessClientDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();