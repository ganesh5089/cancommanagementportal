﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('accessClientController', controller);

    controller.$inject = ['accessClientDataService', 'SimpleListScreenViewModel', 'navigation', 'modalService', 'notificationBarService'];
    / @ngInject /
    function controller(accessClientDataService, simpleListScreenViewModel, navigation, modalService, notificationBarService) {

        var vm = new simpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'clientname';

        vm.getData = function (refresh) {
            accessClientDataService.getAllAccessClients(refresh, vm.dataOperations, vm.filterFn)
                .then(function (result) {
                    vm.pagedData = result.pagedData;
                    vm.fullCount = result.dataCount;
                    vm.filteredCount = result.filteredDataCount;
                });
        };

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.clientName && datum.clientName.toLowerCase().contains(lowerCaseSearchTerm)) ||
                    (datum.siteName && datum.siteName.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.delete = function (accessclient) {
            modalService.confirmDelete('Are you sure you want to delete the Access Client : ' + accessclient.clientName + '?').result.then(
                function () {
                    accessClientDataService.deleteAccessClient(accessclient.id).then(function () {
                        notificationBarService.success('Deleted Successfully');
                        vm.getData(true);
                    });
                },
                function () {
                });
        };

        vm.accessClientEdit = function (id) {
            navigation.gotToAccessClientDetails(id);
        };

        function activate() {
            vm.searchBy(vm.dataOperations.search);
        }

        activate();

        return vm;
    }
})();