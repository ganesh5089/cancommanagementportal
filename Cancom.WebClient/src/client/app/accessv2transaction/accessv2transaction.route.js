﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.accessv2transaction',
                config: {
                    url: 'accessv2transaction',
                    templateUrl: 'app/accessv2transaction/accessv2transaction.html',
                    controller: 'accessV2TransactionController',
                    controllerAs: 'vm',
                    title: 'AccessV2Transaction',
                    ncyBreadcrumb: { label: 'AccessV2Transaction' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();