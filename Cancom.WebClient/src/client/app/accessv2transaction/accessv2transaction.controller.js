﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('accessV2TransactionController', controller);

    controller.$inject = ['accessV2TransactionDataService', 'SimpleListScreenViewModel', 'modalService', 'notificationBarService', 'messagingService', '$filter'];
    / @ngInject /

    function controller(accessV2TransactionDataService, SimpleListScreenViewModel, modalService, notificationBarService, messagingService, $filter) {
        var vm = new SimpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'name';
        vm.alphabetFilterAttributeName = 'name';

        vm.accessV2Transaction = {};

        vm.fileName = "AccessV2Report";

        vm.exportData = [];

        vm.exportData.push(["Id", "Name & Surname", "Driver Id", "Registration No", "Type", "Site", "Date"]);

        vm.getData = function (refresh, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                accessV2TransactionDataService.getAccessV2TransactionList(vm.accessV2Transaction, refresh, vm.dataOperations, vm.filterFn)
                    .then(function (result) {
                        vm.pagedData = result.pagedData;
                        vm.fullCount = result.dataCount;
                        vm.filteredCount = result.filteredDataCount;

                        angular.forEach(result.allData, function (value, key) {
                            var formattedDate = $filter('date')(value.date, "dd-MM-yyyy HH:mm:ss");
                            vm.exportData.push([value.id, value.name, value.driverId, value.registrationNo, value.type, value.site, formattedDate]);
                        });
                    });
            }
        }

        vm.searchBy = function (search, frm) {
            if (search && search.length > 0) {
                vm.filterFn = function (datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.id && datum.id.toString().toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.name && datum.name.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.driverId && datum.driverId.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.registrationNo && datum.registrationNo.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.type && datum.type.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.site && datum.site.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData(false, frm);
        }

        vm.getAllClientData = function () {
            accessV2TransactionDataService.getAllClients()
                .then(function (result) {
                    vm.allClients = result;
                });
        };

        vm.accessV2TransactionView = function(id, regNo, driverId) {
            modalService.accessV2TransactionViewModal("Access V2 Transaction Details", false, id, regNo, driverId).result.then(function() {
            }, function() {
            });
        };

       
        function activate() {
            vm.getAllClientData();
        }

        activate();

        return vm;

    }
})();