﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('accessV2TransactionDetailsController', controller);

    controller.$inject = ['accessV2TransactionDataService', 'notificationBarService', '$uibModalInstance', 'title', 'includeDangerHeader', 'id', 'regNo', 'driverId', '$q'];
    / @ngInject /

    function controller(accessV2TransactionDataService, notificationBarService, $uibModalInstance, title, includeDangerHeader, id, regNo, driverId, $q) {
        var vm = this;

        vm.accessV2TransactionDetails = {};

        vm.title = title;
        vm.includeDangerHeader = includeDangerHeader;
        vm.accessV2TransactionDetails.id = parseInt(id);
        vm.accessV2TransactionDetails.regNo = regNo;
        vm.accessV2TransactionDetails.driverId = driverId;

        vm.getAccessV2TransactionDetails = function () {
            if (vm.accessV2TransactionDetails.id !== 0) {
                accessV2TransactionDataService.getAccessV2TransactionDetailsById(vm.accessV2TransactionDetails.id, vm.accessV2TransactionDetails.regNo, vm.accessV2TransactionDetails.driverId)
                    .then(function (results) {
                        vm.accessV2TransactionDetails = results;
                    });
            }
        }



        function activate() {
            vm.getAccessV2TransactionDetails();
        }

        vm.cancel = function () {
            $uibModalInstance.dismiss();
        }

        activate();

        return vm;
    }
})();