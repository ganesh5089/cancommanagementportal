﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.managemobilesettings.managemobilesetting',
                config: {
                    url: '/{id}',
                    title: 'ManageMobileSetting',
                    ncyBreadcrumb: { label: 'ManageMobileSetting' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/managemobilesettings/managemobilesetting.html',
                            controller: 'manageMobileSettingController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();