﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('manageMobileSettingController', controller);

    controller.$inject = ['accessClientDataService', 'navigation', 'messagingService', 'notificationBarService', 'id', '$q', 'manageMobileSettingsDataService', 'mobileSettingSession', 'modalService'];
    / @ngInject /

    function controller(accessClientDataService, navigation, messagingService, notificationBarService, id, $q, manageMobileSettingsDataService, mobileSettingSession, modalService) {

        var vm = this;

        vm.csspmId = parseInt(id);

        vm.tabs = [
            { id: 0, title: 'General', index: 0, formname: 'generalFrm' },
            { id: 1, title: 'Client', index: 1, formname: 'clientFrm' },
            { id: 2, title: 'Enter', index: 2, formname: 'enterFrm' },
            { id: 3, title: 'Exit', index: 3, formname: 'exitFrm' },
            { id: 4, title: 'Network', index: 4, formname: 'networkFrm' },
            { id: 5, title: 'Form Settings', index: 5, formname: 'formFrm' },
            { id: 6, title: 'Options', index: 6, formname: 'optionsFrm' }
        ];

        vm.getData = function () {
            manageMobileSettingsDataService.getManageMobileSettings(vm.csspmId)
                .then(function (results) {
                    vm.csspmId = results.csspmId;
                    vm.client = results.clientSettings;
                    vm.general = results.generalSettings;
                    vm.enter = results.enterSettings;
                    vm.exit = results.exitSettings;
                    vm.network = results.networkSettings;
                    vm.form = results.formSettings;
                    vm.options = results.optionsSettings;
                });
        };

        function saveMobileSettingSession() {
            mobileSettingSession.create(true);
        }

        function resetForm(frm) {
            frm.$setPristine();
            frm.$setUntouched();
        }

        vm.generalSettingsSave = function (generalFrm) {
            vm.general.csspmId = vm.csspmId;
            messagingService.broadcastCheckFormValidatity();
            if (generalFrm.$valid) {
                manageMobileSettingsDataService.saveGeneralSettings(vm.general)
                    .then(function (id) {
                        vm.general.id = id;
                        resetForm(generalFrm);
                        notificationBarService.success('Record added Successfully');
                        saveMobileSettingSession();
                    });
            }
        }

        vm.clientSettingsSave = function (clientFrm) {
            vm.client.csspmId = vm.csspmId;
            messagingService.broadcastCheckFormValidatity();
            if (clientFrm.$valid) {
                manageMobileSettingsDataService.saveClientSettings(vm.client)
                    .then(function (id) {
                        vm.client.id = id;
                        resetForm(clientFrm);
                        notificationBarService.success('Record added Successfully');
                    });
            }
        }

        vm.enterSettingsSave = function (frm) {
            vm.enter.csspmId = vm.csspmId;
            manageMobileSettingsDataService.saveEnterSettings(vm.enter)
                .then(function (id) {
                    vm.enter.id = id;
                    resetForm(frm);
                    notificationBarService.success('Record added Successfully');
                });
        }

        vm.exitSettingsSave = function (frm) {
            vm.exit.csspmId = vm.csspmId;
            manageMobileSettingsDataService.saveExitSettings(vm.exit)
                .then(function (id) {
                    vm.exit.id = id;
                    resetForm(frm);
                    notificationBarService.success('Record added Successfully');
                });
        }

        vm.networkSettingsSave = function (frm) {
            vm.network.csspmId = vm.csspmId;
            manageMobileSettingsDataService.saveNetworkSettings(vm.network)
                .then(function (id) {
                    vm.network.id = id;
                    resetForm(frm);
                    notificationBarService.success('Record added Successfully');
                });
        }

        vm.formSettingsSave = function (frm) {
            vm.form.csspmId = vm.csspmId;
            manageMobileSettingsDataService.saveFormSettings(vm.form)
                .then(function (id) {
                    vm.form.id = id;
                    resetForm(frm);
                    notificationBarService.success('Record added Successfully');
                });
        }

        vm.optionsSettingsSave = function (frm) {
            vm.options.csspmId = vm.csspmId;
            manageMobileSettingsDataService.saveOptionsSettings(vm.options)
                .then(function (id) {
                    vm.options.id = id;
                    resetForm(frm);
                    notificationBarService.success('Record added Successfully');
                });
        }

        vm.networkCheckChange = function () {
            if (!vm.client.networkCheck) {
                vm.client.networkInterval = '';
                vm.client.networkErrorMessage = '';
            }
        }

        vm.batteryCheckChange = function () {
            if (!vm.client.batteryCheck) {
                vm.client.batteryWarningPercentage = '';
                vm.client.batteryInterval = '';
            }
        }

        vm.deselect = function ($event, $selectedIndex, frm, tab) {
            if ($event) {
                $event.preventDefault();
                if (frm.$dirty) {
                    var message = "<p> You have made changes to this form. Do you want to save these changes?</p>";
                    modalService.questionModal('Unsaved Changes', message, true).result.then(function () {
                        switch (tab.index) {
                            case 0:
                                vm.generalSettingsSave(frm);
                                break;
                            case 1:
                                vm.clientSettingsSave(frm);
                                break;
                            case 2:
                                vm.enterSettingsSave(frm);
                                break;
                            case 3:
                                vm.exitSettingsSave(frm);
                                break;
                            case 4:
                                vm.networkSettingsSave(frm);
                                break;
                            case 5:
                                vm.formSettingsSave(frm);
                                break;
                            case 6:
                                vm.optionsSettingsSave(frm);
                                break;
                        }
                        vm.active = vm.tabs[$selectedIndex].index;
                    }, function () {
                    });
                } else {
                    vm.active = vm.tabs[$selectedIndex].index;
                }
            }
        }

        function activate() {
            mobileSettingSession.destroy();
            vm.getData();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }

        activate();

        return vm;
    }
})();