﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('manageMobileSettingsController', controller);

    controller.$inject = ['manageMobileSettingsDataService', 'SimpleListScreenViewModel', 'accessClientDataService', 'navigation', 'modalService', 'mobileSettingSession'];
    / @ngInject /

    function controller(manageMobileSettingsDataService, simpleListScreenViewModel, accessClientDataService, navigation, modalService, mobileSettingSession) {

        var vm = new simpleListScreenViewModel();

        vm.dataOperations.sortPredicate = 'clientname';

        vm.getData = function (refresh) {
            accessClientDataService.getManageMobileSettingsCsspm(refresh, vm.dataOperations, vm.filterFn)
                .then(function (result) {
                    vm.pagedData = result.pagedData;
                    vm.fullCount = result.dataCount;
                    vm.filteredCount = result.filteredDataCount;
                });
        };

        vm.searchBy = function (search) {
            if (search && search.length > 0) {
                vm.filterFn = function(datum) {
                    var lowerCaseSearchTerm = search.toLowerCase();
                    var result = (datum.id && datum.id.toString().toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.clientName && datum.clientName.toLowerCase().contains(lowerCaseSearchTerm))
                        || (datum.siteName && datum.siteName.toLowerCase().contains(lowerCaseSearchTerm));
                    return result;
                };
            } else {
                vm.filterFn = null;
            }
            vm.getData();
        }

        vm.manageMobileSetting = function (id) {
            navigation.gotToManageMobileSetting(id);
        };

        vm.downloadMobileFiles = function (accessclient) {
            modalService.questionModal('Proceed', 'Are you sure you want to download mobile files for the Client : ' + accessclient.clientName + '?', false).result.then(
              function () {
                  manageMobileSettingsDataService.downloadMobileFiles(accessclient.id, accessclient.clientName, accessclient.mobileName).then(function (url) {
                      window.open(url, '_blank', '');
                  });
              },
              function () {
              });

        };

        function activate() {
            mobileSettingSession.load();
            if (mobileSettingSession.isGeneralSettingsChanged) {
                vm.getData(true);
                mobileSettingSession.destroy();
            } else {
                vm.searchBy(vm.dataOperations.search);
            }
        }

        activate();

        return vm;
    }
})();