﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.managemobilesettings',
                config: {
                    url: 'managemobilesettings',
                    templateUrl: 'app/managemobilesettings/managemobilesettings.html',
                    controller: 'manageMobileSettingsController',
                    controllerAs: 'vm',
                    title: 'ManageMobileSettings',
                    ncyBreadcrumb: { label: 'ManageMobileSettings' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();