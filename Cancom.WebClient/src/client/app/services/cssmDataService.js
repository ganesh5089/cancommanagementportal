﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('cssmDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};
        
        svc.getAllClients = function () {
            return dataService.getRecord('/api/cssm/getallclients');
        };

        svc.getAllProducts = function () {
            return dataService.getRecord('/api/cssm/getallproducts');
        };

        svc.getCssmTreeListByClientId = function (id) {
            return dataService.getRecord('/api/cssm/getcssmtreelistbyclientid/' + id);
        };

        svc.saveCssm = function (cssm) {
            return dataService.post('/api/cssm/savecssm', cssm);
        };

        svc.deleteCssm = function (id) {
            return dataService.post('/api/cssm/' + id + '/deletecssm');
        };
        
        return svc;
    }

})();