﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('transactionTypeDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllTransactionType = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/transactiontype/getalltransactiontype', refresh, dataOperations, filterFn);
        };

        svc.getTransactionTypeDetails = function (id) {
            return dataService.getRecord('/api/transactiontype/gettransactiontypedetails/' + id);
        };

        svc.getAllClients = function () {
            return dataService.getRecord('/api/csspm/getallclients');
        };

        svc.getAllSitesByClientId = function (id) {
            return dataService.getRecord('/api/csspm/getallsitesbyclientid/' + id);
        };

        svc.getAllStationsBySiteId = function (transactionType) {
            var paramFilters = {};
            paramFilters.clientId = transactionType.clientId;
            paramFilters.siteId = transactionType.siteId;
            return dataService.getRecordWithParams('/api/csspm/getallstationsbysiteid', paramFilters);
        };

        svc.getAllProductsBySiteId = function (transactionType) {
            var paramFilters = {};
            paramFilters.clientId = transactionType.clientId;
            paramFilters.siteId = transactionType.siteId;
            paramFilters.stationId = transactionType.stationId;
            return dataService.getRecordWithParams('/api/csspm/getallproductsbysiteid', paramFilters);
        };

        svc.getAllMobilesBySiteId = function (transactionType) {
            var paramFilters = {};
            paramFilters.clientId = transactionType.clientId;
            paramFilters.siteId = transactionType.siteId;
            paramFilters.stationId = transactionType.stationId;
            paramFilters.productId = transactionType.productId;
            return dataService.getRecordWithParams('/api/csspm/getallmobilesbysiteid', paramFilters);
        };

        svc.saveTransactionType = function (transactionType) {
            return dataService.post('/api/transactiontype/savetransactiontype', transactionType);
        };

        svc.deleteTransactionType = function (id) {
            return dataService.post('/api/transactiontype/' + id + '/delete');
        };

        return svc;
    }

})();