(function() {
    'use strict';

    angular
        .module('app')
        .factory('authService', service);

    service.$inject = ['$rootScope', '$http', 'session', 'navigation', 'environment', 'dataService'];

    function service($rootScope, $http, session, navigation, env, dataService) {

        var authService = {};
        var baseUrl = env.serverBaseUrl;

        authService.login = function (credentials) {
            var payload = 'grant_type=password&username=' + credentials.username + '&password=' + credentials.password;

            var start = moment();// jshint ignore:line
            var oauthRoute = '/api/oauth/token';
            return $http
                .post(baseUrl + oauthRoute,
                        payload,
                        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                .then(function (res) {
                    var end = moment();// jshint ignore:line
                    if (angular.isDefined(console)) console.log(oauthRoute + ' took: ' + Math.round(end-start) + ' milliseconds, from: ' + start.format('h:mm:ss.SSS') + ' to: ' + end.format('h:mm:ss.SSS'));
                    session.create(credentials.username, res.data.access_token);// jshint ignore:line
                    return res.data;
                });
        };

        authService.logOut = function(){
            session.destroy();
            navigation.goToLogin();
            dataService.clearCache();
        };

        authService.isAuthenticated = function () {
            return !!session.username && !!session.accessToken;
        };

        authService.hasRequiredPermission = function (permission) {
            return true;
            //TODO: implement permissions in portal
            return (session.userPermissions & permission) === permission;
        };

        return authService;

    }

})();
