﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('uploadIdTechAccessSoftwareDataService', service);

    service.$inject = ['dataService', 'notificationBarService'];

    function service(dataService, notificationBarService) {

        var svc = {};

        svc.getAllClientsIdTechAccessSoftware = function (isActive) {
            return dataService.getRecord('/api/uploadidtechaccesssoftware/getallclientsidtechaccesssoftware/' + isActive);
        };

        svc.getSabIdTechAccessSoftware = function (isActive) {
            return dataService.getRecord('/api/uploadidtechaccesssoftware/getsabidtechaccesssoftware/' + isActive);
        };

        svc.getIdtechAccessFileUploaderInstance = function () {
            var uploader = dataService.getFileUploaderInstanceWithData('/api/uploadidtechaccesssoftware/saveuploadidtechaccesssoftware');
            uploader.autoUpload = false;
            uploader.removeAfterUpload = true;
            uploader.queueLimit = 1;
            uploader.filters.push({
                name: 'zipFilter',
                fn: function (item, options) {

                    var regex = /(?:\.([^.]+))?$/;

                    var extension = regex.exec(item.name)[1];
                    if (extension != null && (extension.toLowerCase() !== 'zip' && extension.toLowerCase() !== 'rar')) {
                        notificationBarService.error("The file being uploaded needs to be of type ZIP or RAR Format (zip, rar)");
                        return false;
                    }

                    var result = item.size < 10485760; //10MB

                    if (!result) {
                        notificationBarService.error("The file being uploaded can't be more than 10MB");
                        return result;
                    }

                    result = this.queue.length < 1;

                    return result;
                }
            });

            return uploader;
        };

        svc.downloadFile = function (id, fileName) {
            var paramFilters = {};
            paramFilters.id = id;
            paramFilters.fileName = fileName;
            debugger;
            return dataService.getReportWithParams('/api/uploadidtechaccesssoftware/downloaduploadidtechaccesssoftwarezipfile', paramFilters);
        };


        return svc;
    }

})();