(function () {
    'use strict';

    angular
        .module('app')
        .factory('navigation', service);

    service.$inject = ['$state', '$location'];

    function service($state, $location) {

        var nav = {};

        nav.gotToDashboard = function () { $state.go('dashboard'); };

        nav.goToLogin = function () { $state.go('login'); };

        nav.goToInsufficientPermissions = function () { $state.go('dashboard.insufficientpermissions'); };

        nav.goToParentState = function () { $state.go('^'); };

        nav.isOnLoginScreen = function () {
            return $state.is('login');
        };

        nav.gotToClient = function () { $state.go('dashboard.webclient'); };

        nav.gotToUserAccessPermission = function () { $state.go('dashboard.useraccesspermission'); };

        nav.gotToAccessClient = function () { $state.go('dashboard.accessclient'); };

        nav.gotToAccessV2Transaction = function () { $state.go('dashboard.accessv2transaction'); };

        nav.gotToTransactionType = function () { $state.go('dashboard.transactiontype'); };

        nav.gotToUploadIdTechAccessSoftware = function () { $state.go('dashboard.uploadidtechaccesssoftware'); };

        nav.gotToVehicleType = function () { $state.go('dashboard.vehicletype'); };

        nav.gotToCssm = function () { $state.go('dashboard.cssm'); };

        nav.gotToClientDetails = function (id) { $state.go('dashboard.webclient.webclientdetails', { id: id }); };

        nav.gotToAccessClientDetails = function (id) { $state.go('dashboard.accessclient.accessclientdetails', { id: id }); };

        nav.gotToAccessV2TransactionDetails = function (id, regNo, driverId) { $state.go('dashboard.accessv2transaction.accessv2transactiondetails', { id: id, regNo: regNo, driverId: driverId }); };

        nav.gotToTransactionTypeDetails = function (id) { $state.go('dashboard.transactiontype.transactiontypedetails', { id: id }); };

        nav.gotToVehicleTypeDetails = function (id) { $state.go('dashboard.vehicletype.vehicletypedetails', { id: id }); };

        nav.gotToUploadIdTechAccessSoftwareDetails = function (id) { $state.go('dashboard.uploadidtechaccesssoftware.uploadidtechaccesssoftwaredetails', { id: id }); };

        nav.gotToFineManagementView = function (id) { $state.go('dashboard.finemanagement.finemanagementview', { id: id }); };

        nav.gotToManageMobileSetting = function (id) { $state.go('dashboard.managemobilesettings.managemobilesetting', { id: id }); };

        return nav;
    }

})();
