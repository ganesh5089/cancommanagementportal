﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('modalService', service);

    service.$inject = ['$uibModal'];

    function service($uibModal) {

        var svc = {};

        //prevent the backspace navigation issue, when user hit backspace
        //page nagvigates to previous screen
        $(document).bind('keydown', function (e) {
            if (e.which === 8 && (e.target.className.indexOf("modal") > -1 || e.target.parentNode.className.indexOf("modal") > -1)) {
                e.preventDefault();
            }
        });

        svc.openPdfReportModal = function (reportname, pdfurl) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/layout/pdfReportModal.html',
                controller: 'PdfReportModalController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    reportName: function () {
                        return reportname;
                    },
                    pdfUrl: function () {
                        return pdfurl;
                    }
                }
            });
        };

        svc.confirmDelete = function (confirmationMessage) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/layout/genericDeleteConfirmationModal.html',
                controller: 'GenericDeleteConfirmationModalController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                    confirmationMessage: function () {
                        return confirmationMessage;
                    }
                }
            });
        };

        svc.questionModal = function (title, message, includeDangerHeader) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/layout/genericQuestionModal.html',
                controller: 'GenericQuestionModalController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    }
                }
            });
        };

        svc.messageModal = function (title, message) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/layout/genericMessageModal.html',
                controller: 'GenericMessageModalController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                    title: function () {
                        return title;
                    },
                    message: function () {
                        return message;
                    }
                }
            });
        };

        svc.cssmModal = function (title, includeDangerHeader, clientName, siteName, stationName, actionName) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/cssm/cssmdetails.html',
                controller: 'cssmDetailsController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    },
                    clientName: function () {
                        return clientName;
                    },
                    siteName: function () {
                        return siteName;
                    },
                    stationName: function () {
                        return stationName;
                    },
                    actionName: function () {
                        return actionName;
                    }
                }
            });
        };

        svc.accessV2TransactionViewModal = function (title, includeDangerHeader, id, regNo, driverId) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/accessv2transaction/accessv2transactiondetails.html',
                controller: 'accessV2TransactionDetailsController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    },
                    id: function () {
                        return id;
                    },
                    regNo: function () {
                        return regNo;
                    },
                    driverId: function () {
                        return driverId;
                    }
                }
            });
        };

        svc.fineManagementViewModal = function (title, includeDangerHeader, id) {
            return $uibModal.open({
                animation: true,
                templateUrl: 'app/finemanagement/finemanagementview.html',
                controller: 'fineManagementViewController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    includeDangerHeader: function () {
                        return includeDangerHeader;
                    },
                    id: function () {
                        return id;
                    }
                }
            });
        };
 
        return svc;

    }

})();
