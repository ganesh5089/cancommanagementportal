﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('clientDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllClients = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/client/getallclients', refresh, dataOperations, filterFn);
        };

        svc.getAllClientsFromCFMS = function () {
            return dataService.getRecord('/api/finemanagement/getallclientsfinemanagement');
        };

        svc.getClientDetails = function (id) {
            return dataService.getRecord('/api/client/getclientdetails/' + id);
        };

        svc.deleteClient = function (id) {
            return dataService.post('/api/client/' + id + '/delete');
        };

        svc.saveClient = function (client) {
            return dataService.post('/api/client/saveclient', client);
        };

        svc.getAllProducts = function () {
            return dataService.getRecord('/api/product/getallproducts');
        };

        return svc;

    }

})();
