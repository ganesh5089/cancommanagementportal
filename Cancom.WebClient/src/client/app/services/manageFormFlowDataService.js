﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('manageFormFlowDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllVehicleType = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/vehicletype/getallvehicletype', refresh, dataOperations, filterFn);
        };

        svc.getVehicleTypeDetails = function (id) {
            return dataService.getRecord('/api/vehicletype/getvehicletypedetails/' + id);
        };

        svc.getVehicleTypeListByTransactionId = function (manageFormFlow) {
            return dataService.getRecordWithParams('/api/vehicletype/getvehicletypelistbytransactionid', manageFormFlow);
        };

        svc.getAllClients = function () {
            return dataService.getRecord('/api/csspm/getallclients');
        };

        svc.getAllSitesByClientId = function (id) {
            return dataService.getRecord('/api/csspm/getallsitesbyclientid/' + id);
        };

        svc.getAllStationsBySiteId = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            return dataService.getRecordWithParams('/api/csspm/getallstationsbysiteid', paramFilters);
        };

        svc.getAllProductsBySiteId = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            paramFilters.stationId = manageFormFlow.stationId;
            return dataService.getRecordWithParams('/api/csspm/getallproductsbysiteid', paramFilters);
        };

        svc.getAllMobilesBySiteId = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            paramFilters.stationId = manageFormFlow.stationId;
            paramFilters.productId = manageFormFlow.productId;
            return dataService.getRecordWithParams('/api/csspm/getallmobilesbysiteid', paramFilters);
        };

        svc.getAllManageFormFlowAvailableForms = function () {
            return dataService.getRecord('/api/manageformflow/getallmanageformflowavailableforms');
        };

        svc.getTransactionTypesByCsspmid = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            paramFilters.stationId = manageFormFlow.stationId;
            paramFilters.productId = manageFormFlow.productId;
            paramFilters.mobileId = manageFormFlow.mobileId;

            return dataService.getRecordWithParams('/api/vehicletype/gettransactiontypesbycsspmid', paramFilters);
        };

        svc.getAllManageFormFlowAllocatedForms = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            paramFilters.stationId = manageFormFlow.stationId;
            paramFilters.productId = manageFormFlow.productId;
            paramFilters.mobileId = manageFormFlow.mobileId;
            paramFilters.transactionTypeId = manageFormFlow.transactionTypeId;
            paramFilters.vehicleTypeId = manageFormFlow.vehicleTypeId;

            return dataService.getRecordWithParams('/api/manageformflow/getallmanageformflowallocatedforms', paramFilters);
        };


        svc.getallocatedform = function (manageFormFlow) {
            var paramFilters = {};
            paramFilters.clientId = manageFormFlow.clientId;
            paramFilters.siteId = manageFormFlow.siteId;
            paramFilters.stationId = manageFormFlow.stationId;
            paramFilters.productId = manageFormFlow.productId;
            paramFilters.mobileId = manageFormFlow.mobileId;
            paramFilters.transactionTypeId = manageFormFlow.transactionTypeId;
            paramFilters.vehicleTypeId = manageFormFlow.vehicleTypeId;

            return dataService.getRecordWithParams('/api/manageformflow/getallocatedform', paramFilters);
        };

        svc.AddFormsToAllocatedForms = function (manageFormFlow) {
            return dataService.post('/api/manageformflow/formflowaddtoallocatedforms', manageFormFlow);
        };

        svc.removeFormsFromAllocatedForms = function (manageFormFlow) {
            return dataService.post('/api/manageformflow/formflowremovefromallocatedforms', manageFormFlow);
        };

        svc.AllocatedFormsMoveUp = function (manageFormFlow) {
            return dataService.post('/api/manageformflow/formflowupallocatedforms', manageFormFlow);
        };

        svc.AllocatedFormsMoveDown = function (manageFormFlow) {
            return dataService.post('/api/manageformflow/formflowdownallocatedforms', manageFormFlow);
        };

        return svc;
    }

})();