﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('viewTicketDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllTicket = function (clientId, referenceNumber, carReg, raNumber, refresh, dataOperations, filterFn) {
            var paramFilters = {};
            paramFilters.clientId = clientId;
            paramFilters.referenceNumber = referenceNumber;
            paramFilters.carReg = carReg;
            paramFilters.raNumber = raNumber;

            return dataService.getDataWithParams('/api/viewticket/getalltickets', paramFilters, refresh, dataOperations, filterFn);
        };

        return svc;

    }

})();