(function () {
    'use strict';

    angular
        .module('app')
        .factory('dataService', service);

    service.$inject = ['$q', '$http', '$linq', '$sce', 'queryBuilderService', 'session', 'environment', 'FileUploader'];

    function service($q, $http, $linq, $sce, queryBuilderService, session, env, fileUploader) {

        var svc = {};
        var cache = {};
        var baseUrl = env.serverBaseUrl;
        var api_auth_key = env.api_auth_key;
        var api_auth_key_header = 'api_auth_key';

        svc.getFileUploaderInstance = function (route) {
            return new fileUploader({
                url: baseUrl + route,
                headers: {
                    Authorization: 'Bearer ' + session.accessToken,
                    api_auth_key: api_auth_key
                }
            });
        }

        svc.getFileUploaderInstanceWithData = function (route) {
            return new fileUploader({
                url: baseUrl + route,
                formData: [],
                headers: {
                    Authorization: 'Bearer ' + session.accessToken,
                    api_auth_key: api_auth_key
                }
            });
        }

        svc.clearCache = function () {
            cache = {};
        }

        svc.clearRouteCache = function (route) {
            cache[route] = null;
        }

        svc.post = function (route, data) {
            var start = moment(); // jshint ignore:line
            return $http.post(baseUrl + route, data).then(function (result) {
                consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                return result.data;
            });
        };

        svc.delete = function (route) {
            var start = moment(); // jshint ignore:line
            return $http.delete(baseUrl + route).then(function (result) {
                consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                return result.data;
            });
        };

        svc.getReport = function (route) {
            var authTokenParam = [];
            authTokenParam["authtoken"] = session.accessToken;
            authTokenParam[api_auth_key_header] = api_auth_key;
            return $q.when($sce.trustAsResourceUrl(queryBuilderService.getQueryUrl(baseUrl + route, authTokenParam)));
        };

        svc.getReportWithParams = function (route, paramValues) {
            paramValues["authtoken"] = session.accessToken;
            paramValues[api_auth_key_header] = api_auth_key;
            return $q.when($sce.trustAsResourceUrl(queryBuilderService.getQueryUrl(baseUrl + route, paramValues)));
        };

        svc.getRecord = function (route) {
            var start = moment(); // jshint ignore:line
            return $http.get(baseUrl + route).then(function (result) {
                consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                return result.data;
            });
        };

        svc.getRecordWithParams = function (route, paramValues) {
            var start = moment(); // jshint ignore:line
            return $http.get(baseUrl + route, { params: paramValues }).then(function (result) {
                consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                return result.data;
            });
        };
      
        svc.getLookupData = function (route, refresh) {
            var paramValues = {
                t: moment().millisecond()
            }
            return svc.getLookupDataWithParams(route, paramValues, refresh);
        };

        svc.getLookupDataWithParams = function (route, paramValues, refresh) {
            if (dataForRouteIsCached(route, refresh)) {
                return $q.when(cache[route]);
            } else {
                var start = moment(); // jshint ignore:line
                var params = paramValues ? { params: paramValues } : null;
                return $http.get(baseUrl + route, params).then(function (result) {
                    consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                    cache[route] = result.data;
                    return result.data;
                });
            }
        };

        svc.getDataWithParams = function (route, paramValues, refresh, dataOperations, filterFn) {
            if (dataForRouteIsCached(route, refresh)) {
                return $q.when(getPagedData(cache[route], dataOperations, filterFn));
            } else { //no cached data or refresh requested
                var start = moment(); // jshint ignore:line
                paramValues.t = start.millisecond();
                return $http.get(baseUrl + route, { params: paramValues })
                    .then(function (result) {
                        consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                        cache[route] = result.data;
                        return getPagedData(cache[route], dataOperations, filterFn);
                    });
            }
        };

        svc.getData = function (route, refresh, dataOperations, filterFn) {
            if (dataForRouteIsCached(route, refresh)) {
                return $q.when(getPagedData(cache[route], dataOperations, filterFn));
            } else { //no cached data or refresh requested
                var start = moment(); // jshint ignore:line
                return $http.get(baseUrl + route)
                    .then(function (result) {
                        consoleLogRequestTime(route, start, moment()); // jshint ignore:line
                        cache[route] = result.data;
                        return getPagedData(cache[route], dataOperations, filterFn);
                    });
            }
        };

        function getPagedData(data, dataOperations, filterFn) {
            var take = dataOperations.paging.pageSize;
            var skip = dataOperations.paging.currentPage ? (dataOperations.paging.currentPage - 1) * dataOperations.paging.pageSize : 0;
            var filteredData = $linq.Enumerable().From(data).Where(filterFn);

            var sortedData;

            var sortFn = function (datum) {
                if (datum[dataOperations.sortPredicate])
                    return datum[dataOperations.sortPredicate].toLowerCase;
                else
                    return datum[dataOperations.sortPredicate];
            };
            if (dataOperations.sortPredicate) {
                if (dataOperations.sortOrder) {
                    sortedData = $linq.Enumerable().From(filteredData.ToArray()).OrderBy(sortFn);
                } else {
                    sortedData = $linq.Enumerable().From(filteredData.ToArray()).OrderByDescending(sortFn);
                }
            } else {
                sortedData = filteredData;
            }

            return {
                allData: data,
                pagedData: (sortedData && sortedData.ToArray().length <= skip) ? sortedData.ToArray() : sortedData.Skip(skip).Take(take).ToArray(),
                filteredDataCount: filteredData.Count(),
                dataCount: data.length
            };
        }

        function consoleLogRequestTime(action, start, end) {
            if (angular.isDefined(console))
                console.log(action + ' took: ' + Math.round(end - start) + ' milliseconds, from: ' + start.format('h:mm:ss.SSS') + ' to: ' + end.format('h:mm:ss.SSS'));
        }

        function dataForRouteIsCached(route, refresh) {
            return cache[route] && (refresh === false || refresh == undefined);
        }

        return svc;

    }

})();
