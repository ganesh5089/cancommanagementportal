﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('uploadLoanFormDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllClients = function () {
            return dataService.getRecord('/api/client/getallclients');
        };

        svc.GetUserListByClient = function (id) {
            return dataService.getRecord('/api/uploadloanform/getuserlistbyclient/' + id);
        };

        svc.GetFileListByClient = function (id) {
            return dataService.getRecord('/api/uploadloanform/getfilelistbyclient/' + id);
        };

        svc.saveUploadFile = function (uploadLoanForm) {
            return dataService.post('/api/uploadloanform/saveuploadfile', uploadLoanForm);
        };

        return svc;
    }

})();