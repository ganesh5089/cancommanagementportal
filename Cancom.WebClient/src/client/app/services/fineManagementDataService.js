﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('fineManagementDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllClientsFineManagement = function () {
            return dataService.getRecord('/api/finemanagement/getallclientsfinemanagement');
        };

        svc.getFineManagementDetailsBySearch = function (clientId, carRegistration, referenceNo, searchCriteria, refresh, dataOperations, filterFn) {
            var paramFilters = {};
            paramFilters.clientId = clientId;
            paramFilters.carRegistration = carRegistration;
            paramFilters.referenceNo = referenceNo;
            paramFilters.searchCriteria = searchCriteria;
            return dataService.getDataWithParams('/api/finemanagement/getfinemanagementbysearch', paramFilters, refresh, dataOperations, filterFn);
        };

        svc.getFineManagementViewDetails = function (ticketId) {
            return dataService.getRecord('/api/finemanagement/getfinemanagementviewdetails/' + ticketId);
        };

        svc.downloadFinePdf = function (fineId) {
            return dataService.getReport('/api/finemanagement/downloadfinepdf/' + fineId);
        };

        return svc;

    }

})();
