(function () {
    'use strict';

    angular
        .module('app')
        .service('session', session);

    session.$inject = ['localStorageService', 'environment'];

    function session(localStorageService, env) {

        var localStorageSessionKey = 'CancomPortal-' + env.environment + '-AuthData';

        this.create = function (username, accessToken) {// jshint ignore:line
            this.setLocalStorageProperties(username, accessToken);
            this.setSessionProperties(username, accessToken);
        };

        this.destroy = function () {// jshint ignore:line
            this.setLocalStorageProperties();
            this.setSessionProperties();
        };

        this.load = function () {// jshint ignore:line
            var localData = localStorageService.get(localStorageSessionKey);
            if (localData) {
                this.setSessionProperties(localData.username,
                    localData.accessToken);
            }
        };

        this.setSessionProperties = function (username, accessToken) {// jshint ignore:line
            this.username = username;
            this.accessToken = accessToken;
        };

        this.setLocalStorageProperties = function (username, accessToken) {// jshint ignore:line
            localStorageService.set(localStorageSessionKey, {
                accessToken: accessToken,// jshint ignore:line
                username: username
            });
        };
    }


})();
