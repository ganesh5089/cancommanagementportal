﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('manageMobileSettingsDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getManageMobileSettings = function (csspmId) {
            return dataService.getRecord('/api/managemobilesettings/getsettings/' + csspmId);
        };

        svc.downloadMobileFiles = function (id, clientName, mobileName) {
            var paramFilters = {};
            paramFilters.csspmId = id;
            paramFilters.clientName = clientName;
            paramFilters.mobileName = mobileName;
            return dataService.getReportWithParams('/api/managemobilesettings/downloadmobilezipfile', paramFilters);
        };

        svc.saveClientSettings = function (clientSettings) {
            return dataService.post('/api/managemobilesettings/saveclientsettings', clientSettings);
        };

        svc.saveGeneralSettings = function (generalSettings) {
            return dataService.post('/api/managemobilesettings/savegeneralsettings', generalSettings);
        };

        svc.saveEnterSettings = function (enterSettings) {
            return dataService.post('/api/managemobilesettings/saveentersettings', enterSettings);
        };

        svc.saveExitSettings = function (exitSettings) {
            return dataService.post('/api/managemobilesettings/saveexitsettings', exitSettings);
        };

        svc.saveNetworkSettings = function (networkSettings) {
            return dataService.post('/api/managemobilesettings/savenetworksettings', networkSettings);
        };

        svc.saveFormSettings = function (formSettings) {
            return dataService.post('/api/managemobilesettings/saveformsettings', formSettings);
        };

        svc.saveOptionsSettings = function (optionsSettings) {
            return dataService.post('/api/managemobilesettings/saveoptionssettings', optionsSettings);
        };

        return svc;
    }

})();