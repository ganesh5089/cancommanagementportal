﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('accessClientDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllAccessClients = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/accessclient/getallaccessclients', refresh, dataOperations, filterFn);
        };

        svc.getManageMobileSettingsCsspm = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/accessclient/getmanagemobilesettingscsspm', refresh, dataOperations, filterFn);
        };

        svc.getAccessClientDetails = function (id) {
            return dataService.getRecord('/api/accessclient/getaccessclientdetails/' + id);
        };

        svc.getAllProducts = function () {
            return dataService.getRecord('/api/accessclient/getallproducts');
        };

        svc.saveAccessClient = function (accessClient) {
            return dataService.post('/api/accessclient/saveaccessclient', accessClient);
        };

        svc.deleteAccessClient = function (id) {
            return dataService.post('/api/accessclient/' + id + '/delete');
        };

        return svc;
    }

})();