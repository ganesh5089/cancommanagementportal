﻿(function () {
    'use strict';

    angular
        .module('app')
        .service('mobileSettingSession', session);

    session.$inject = ['localStorageService', 'environment'];

    function session(localStorageService, env) {

        var localStorageSessionKey = 'CancomPortal-' + env.environment + '-MobileSettingSession';

        this.create = function (value) {// jshint ignore:line
            this.setLocalStorageProperties(value);
            this.setSessionProperties(value);
        };

        this.destroy = function () {// jshint ignore:line
            this.setLocalStorageProperties();
            this.setSessionProperties();
        };

        this.load = function () {// jshint ignore:line
            var localData = localStorageService.get(localStorageSessionKey);
            if (localData) {
                this.setSessionProperties(localData.isGeneralSettingsChanged);
            }
        };

        this.setSessionProperties = function (value) {// jshint ignore:line
            this.isGeneralSettingsChanged = value;
        };

        this.setLocalStorageProperties = function (value) {// jshint ignore:line
            localStorageService.set(localStorageSessionKey, {
                isGeneralSettingsChanged: value// jshint ignore:line
            });
        };

    }

})();
