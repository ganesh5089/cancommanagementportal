﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('dashboardDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getTotalClientCount = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getallclientscount', refresh);
        };

        svc.getSabTodayLateTransactions = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getsabtodaylatetransactions', refresh);
        };

        svc.getSabYesterdayLateTransactions = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getsabyesterdaylatetransactions', refresh);
        };

        svc.getSabLast7DaysLateTransactions = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getsablast7dayslatetransactions', refresh);
        };

        svc.getCurrentMonthSabTransactionsCount = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getcurrentmonthsabtransactionscount', refresh);
        };

        svc.getPreviousMonthSabTransactionsCount = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getpreviousmonthsabtransactionscount', refresh);
        };

        svc.getCurrentMonthSummaryReport = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getcurrentmonthsummaryreport', refresh);
        };

        svc.getPreviousMonthSummaryReport = function (refresh) {
            return dataService.getLookupData('/api/dashboard/getpreviousmonthsummaryreport', refresh);
        };
        return svc;

    }

})();
