﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('userDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllClients = function () {
            return dataService.getRecord('/api/user/getallclients');
        };
 
        svc.getUsersByClientid = function (clientId, isAdmin) {
            var paramFilters = {};
            paramFilters.clientId = clientId;
            paramFilters.isAdmin = isAdmin;
            return dataService.getRecordWithParams('/api/user/getusersbyclientid', paramFilters);
        };

        svc.getClientUserPermissions = function (clientId, userId, refresh, dataOperations, filterFn) {
            var paramFilters = {};
            paramFilters.clientId = clientId;
            paramFilters.userId = userId;
            return dataService.getDataWithParams('/api/user/getclientuserpermissions', paramFilters, refresh, dataOperations, filterFn);
        };

        svc.getAllProductsFromClientUser = function (clientId,userId, isAdmin) {
            var paramFilters = {};
            paramFilters.clientId = clientId;
            paramFilters.userId = userId;
            paramFilters.isAdmin = isAdmin;
            return dataService.getRecordWithParams('/api/user/getallProductsfromclientuser', paramFilters);
        };
        
        svc.deleteUserAccess = function (record) {
            var param = {};
            param.userAccessId = record.userAccessId;
            param.clientId = record.clientId;
            param.userId = record.userId;
            return dataService.post('/api/useraccess/delete', record);
        };

        svc.saveUserAccess = function (userAccess) {
            return dataService.post('/api/useraccess/saveuseraccess', userAccess);
        };

        return svc;
    }

})();
