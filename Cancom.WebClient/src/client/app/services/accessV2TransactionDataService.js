﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('accessV2TransactionDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllClients = function () {
            return dataService.getRecord('/api/accessv2transaction/getallclients');
        };

        svc.getAccessV2TransactionList = function (accessV2Transaction, refresh, dataOperations, filterFn) {
            var paramFilters = {};
            paramFilters.clientId = accessV2Transaction.clientId;
            paramFilters.driverId = accessV2Transaction.driverId != null ? accessV2Transaction.driverId : '';
            paramFilters.surname = accessV2Transaction.surname != null ? accessV2Transaction.surname : '';
            paramFilters.vehicleRegistration = accessV2Transaction.vehicleRegistration != null ? accessV2Transaction.vehicleRegistration : '';
            paramFilters.fromDate = accessV2Transaction.fromDate != null ? accessV2Transaction.fromDate : null;
            paramFilters.toDate = accessV2Transaction.toDate != null ? accessV2Transaction.toDate : null;
            return dataService.getDataWithParams('/api/accessv2transaction/getaccessv2transactionlist', paramFilters, refresh, dataOperations, filterFn);
        };

        svc.getAccessV2TransactionDetailsById = function (id, regNo, driverId) {
            var paramFilters = {};
            paramFilters.id = id;
            paramFilters.regNo = regNo;
            paramFilters.driverId = driverId;
            return dataService.getRecordWithParams('/api/accessv2transaction/getaccessv2transactiondetailsbyid', paramFilters);
        };

        return svc;
    }

})();