﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('vehicleTypeDataService', service);

    service.$inject = ['dataService'];

    function service(dataService) {

        var svc = {};

        svc.getAllVehicleType = function (refresh, dataOperations, filterFn) {
            return dataService.getData('/api/vehicletype/getallvehicletype', refresh, dataOperations, filterFn);
        };

        svc.getVehicleTypeDetails = function (id) {
            return dataService.getRecord('/api/vehicletype/getvehicletypedetails/' + id);
        };

        svc.getAllClients = function () {
            return dataService.getRecord('/api/csspm/getallclients');
        };

        svc.getAllSitesByClientId = function (id) {
            return dataService.getRecord('/api/csspm/getallsitesbyclientid/' + id);
        };

        svc.getAllStationsBySiteId = function (vehicleType) {
            var paramFilters = {};
            paramFilters.clientId = vehicleType.clientId;
            paramFilters.siteId = vehicleType.siteId;
            return dataService.getRecordWithParams('/api/csspm/getallstationsbysiteid', paramFilters);
        };

        svc.getAllProductsBySiteId = function (vehicleType) {
            var paramFilters = {};
            paramFilters.clientId = vehicleType.clientId;
            paramFilters.siteId = vehicleType.siteId;
            paramFilters.stationId = vehicleType.stationId;
            return dataService.getRecordWithParams('/api/csspm/getallproductsbysiteid', paramFilters);
        };

        svc.getAllMobilesBySiteId = function (vehicleType) {
            var paramFilters = {};
            paramFilters.clientId = vehicleType.clientId;
            paramFilters.siteId = vehicleType.siteId;
            paramFilters.stationId = vehicleType.stationId;
            paramFilters.productId = vehicleType.productId;
            return dataService.getRecordWithParams('/api/csspm/getallmobilesbysiteid', paramFilters);
        };

        svc.getTransactionTypesByCsspmid = function (vehicleType) {
            var paramFilters = {};
            paramFilters.clientId = vehicleType.clientId;
            paramFilters.siteId = vehicleType.siteId;
            paramFilters.stationId = vehicleType.stationId;
            paramFilters.productId = vehicleType.productId;
            paramFilters.mobileId = vehicleType.mobileId;
            
            return dataService.getRecordWithParams('/api/vehicletype/gettransactiontypesbycsspmid', paramFilters);
        };

        svc.saveVehicleType = function (vehicleType) {
            return dataService.post('/api/vehicletype/savevehicletype', vehicleType);
        };

        svc.deleteVehicleType = function (id) {
            return dataService.post('/api/vehicletype/' + id + '/delete');
        };
        
        return svc;
    }

})();