﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('uploadIdTechAccessSoftwareController', controller);

    controller.$inject = ['uploadIdTechAccessSoftwareDataService', 'SimpleListScreenViewModel', 'navigation', 'messagingService', 'notificationBarService', 'FileUploader', 'environment', '$q', 'modalService'];
    / @ngInject /

    function controller(uploadIdTechAccessSoftwareDataService, SimpleListScreenViewModel, navigation, messagingService, notificationBarService, FileUploader, env, $q, modalService) {

        var vm = new SimpleListScreenViewModel();

        vm.sabActiveFlag = true;
        vm.allClientsActiveFlag = true;

        vm.sabGetData = function () {
            uploadIdTechAccessSoftwareDataService.getSabIdTechAccessSoftware(vm.sabActiveFlag)
		        .then(function (result) {
		            vm.sabData = result;
		        });
        };

        vm.allClientGetData = function () {
            uploadIdTechAccessSoftwareDataService.getAllClientsIdTechAccessSoftware(vm.allClientsActiveFlag)
		        .then(function (result) {
		            vm.allClientData = result;
		        });
        };

        vm.sabRefreshData = function () {
            vm.sabGetData();
        }

        vm.allClientRefreshData = function () {
            vm.allClientGetData();
        }

        vm.downloadFile = function (id, fileName) {
            modalService.questionModal('Proceed', 'Are you sure you want to download the file : ' + fileName + '?', false).result.then(
             function () {
                 uploadIdTechAccessSoftwareDataService.downloadFile(id, fileName).then(function (url) {
                     window.open(url, '_blank', '');
                 });
             },
             function () {
             });
        }

        function activate() {
            vm.sabGetData();
            vm.allClientGetData();
        }

        activate();

        return vm;
    }
})();