﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.uploadidtechaccesssoftware',
                config: {
                    url: 'uploadidtechaccesssoftware',
                    templateUrl: 'app/uploadidtechaccesssoftware/uploadidtechaccesssoftware.html',
                    controller: 'uploadIdTechAccessSoftwareController',
                    controllerAs: 'vm',
                    title: 'UploadIdTechAccessSoftware',
                    ncyBreadcrumb: { label: 'UploadIdTechAccessSoftware' },
                    settings: {
                        mustBeAuthenticated: true
                    }
                }
            }
        ];
    }
})();