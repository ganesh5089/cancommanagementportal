﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('uploadIdTechAccessSoftwareDetailsController', controller);

    controller.$inject = ['uploadIdTechAccessSoftwareDataService', 'navigation', 'messagingService', 'FileUploader', 'environment', 'notificationBarService', 'id', '$q'];
    / @ngInject /

    function controller(uploadIdTechAccessSoftwareDataService, navigation, messagingService, FileUploader, env, notificationBarService, id, $q) {

        var vm = this;

        vm.uploadIdTechAccessSoftware = {};
        vm.uploadIdTechAccessSoftware.id = parseInt(id);

        vm.allTypes = [
               { value: 'SAB', key: 1 },
               { value: 'All Client', key: 2 }
        ];

        vm.getData = function () {
           
        };

        vm.save = function (uploadIdTechAccessSoftware, frm) {
            messagingService.broadcastCheckFormValidatity();
            if (frm.$valid) {
                vm.uploader.uploadAll();
            }
        }
         
       function initUploader()   {
           vm.uploader = uploadIdTechAccessSoftwareDataService.getIdtechAccessFileUploaderInstance();

           vm.uploader.onBeforeUploadItem = function (item) {
               vm.showProgressbar = true;
               item.formData.push({
                   fileType: vm.uploadIdTechAccessSoftware.type,
                   description: vm.uploadIdTechAccessSoftware.description,
                   version: vm.uploadIdTechAccessSoftware.version
               });
           };
           
            vm.uploader.onSuccessItem = function (item, response) {
                navigation.gotToUploadIdTechAccessSoftware();
                vm.showProgressbar = false;
                notificationBarService.success('File Uploaded Successfully');
            };

            vm.uploader.onErrorItem = function (item, response, status) {
                vm.showProgressbar = false;
                vm.uploaderFileName = '';
                if (status === 400 && response)
                    notificationBarService.error(response);
                else
                    notificationBarService.error('Sorry your File upload failed.  Please ensure you have selected the correct file to import.');
            }
        

            vm.uploader.onAfterAddingFile = function (fileUploader) {
                vm.uploaderFileName = fileUploader.file.name;
            };
        }

        vm.upload = function() {
            vm.uploader.uploadAll();
        }

        function activate() {
            vm.getData();
            initUploader();
        }

        vm.cancel = function () {
            navigation.goToParentState();
        }

        activate();

        return vm;
    }
})();