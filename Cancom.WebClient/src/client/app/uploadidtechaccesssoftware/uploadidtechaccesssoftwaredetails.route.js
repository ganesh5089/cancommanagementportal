﻿(function () {
    'use strict';

    angular
        .module('app')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    / @ngInject /
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'dashboard.uploadidtechaccesssoftware.uploadidtechaccesssoftwaredetails',
                config: {
                    url: '/{id}',
                    title: 'UploadIdTechAccessSoftwareDetails',
                    ncyBreadcrumb: { label: 'UploadIdTechAccessSoftwareDetails' },
                    resolve: {
                        id: ['$stateParams', function ($stateParams) {
                            return $stateParams.id;
                        }]
                    },
                    settings: {
                        mustBeAuthenticated: true
                    },
                    views: {
                        '@dashboard': {
                            templateUrl: 'app/uploadidtechaccesssoftware/uploadidtechaccesssoftwaredetails.html',
                            controller: 'uploadIdTechAccessSoftwareDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();