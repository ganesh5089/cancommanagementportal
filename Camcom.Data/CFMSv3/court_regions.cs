//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cancom.Data.CFMSv3
{
    using System;
    using System.Collections.Generic;
    
    public partial class court_regions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public court_regions()
        {
            this.tickets = new HashSet<ticket>();
        }
    
        public int id { get; set; }
        public string court_region { get; set; }
        public System.DateTime created_date { get; set; }
        public System.DateTime modify_date { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> application_id { get; set; }
        public string stored_procedure { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ticket> tickets { get; set; }
    }
}
