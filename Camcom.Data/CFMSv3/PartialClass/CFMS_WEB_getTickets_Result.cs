﻿using System;

namespace Cancom.Data.CFMSv3
{
    public partial class CFMS_WEB_getTickets_Result
    {
        public DateTime? FormattedDate
        {
            get
            {
                if (string.IsNullOrEmpty(Offence_Date))
                    return null;

                return Convert.ToDateTime(Offence_Date);
            }
        }
    }
}
