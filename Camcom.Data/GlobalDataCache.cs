﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Cancom.Data.CFMSv3;
using Cancom.Data.CFMSv3_FileRepo;
using Cancom.Data.IDTechAccessV2;
using Cancom.Data.LoginDB;

namespace Cancom.Data
{
    public class GlobalDataCache : IDataCache
    {
        private readonly loginDBDataContext context;
        private readonly CFMSv3DataContext cfmsContext;
        private readonly IDTechAccessV2Entities idTechAccessV2Entities;
        private readonly CFMSv3_FileRepoEntities cfmSv3FileRepoEntities;

        private IEnumerable<User> apiClients;

        //public GlobalDataCache(CfmSv3DataContextFactory factory)
        //{
        //    context = factory.GetNewLoginDbDataContext();
        //}

        public GlobalDataCache(loginDBDataContext context, CFMSv3DataContext cfmSv3DataContext, IDTechAccessV2Entities idTechAccessV2Entities, CFMSv3_FileRepoEntities cfmSv3FileRepoEntities)
        {
            this.context = context;
            this.cfmsContext = cfmSv3DataContext;
            this.idTechAccessV2Entities = idTechAccessV2Entities;
            this.cfmSv3FileRepoEntities = cfmSv3FileRepoEntities;
        }

        public async Task<IEnumerable<User>> ApiClients()
        {
            return apiClients ??
                   (apiClients = await context.Users.AsNoTracking().ToListAsync());
        }

        public async Task PrimeCache()
        {
            await ApiClients();
        }
    }
}
