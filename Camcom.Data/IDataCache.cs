﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cancom.Data.LoginDB;

namespace Cancom.Data
{
    public interface IDataCache
    {
        Task<IEnumerable<User>> ApiClients();
        Task PrimeCache();
    }
}
