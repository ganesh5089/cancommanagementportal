﻿using Cancom.Data.LoginDB;

namespace Cancom.Data
{
    public class CfmSv3DataContextFactory
    {
        private readonly loginDBDataContext perHttpRequestContext;

        public CfmSv3DataContextFactory(loginDBDataContext perHttpRequestContext)
        {
            this.perHttpRequestContext = perHttpRequestContext;
        }

        public loginDBDataContext GetPerHttpRequestContext()
        {
            return perHttpRequestContext;
        }

        public loginDBDataContext GetNewLoginDbDataContext()
        {
            return new loginDBDataContext();
        }
    }
}
