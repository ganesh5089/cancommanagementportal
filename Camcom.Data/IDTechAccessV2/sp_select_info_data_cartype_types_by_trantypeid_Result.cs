//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cancom.Data.IDTechAccessV2
{
    using System;
    
    public partial class sp_select_info_data_cartype_types_by_trantypeid_Result
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public bool Active { get; set; }
        public int SiteID { get; set; }
        public Nullable<int> Seq { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
    }
}
