﻿using System;

namespace Cancom.Data.IDTechAccessV2
{
    public partial class SABTransactions_Portal_Result
    {
        public string FormattedUploadedTime
        {
            get
            {
                TimeSpan time = TimeSpan.FromSeconds(UploadedTime.GetValueOrDefault());
                return time.ToString(@"dd\:hh\:mm\:ss");
            }
        }
    }
}
