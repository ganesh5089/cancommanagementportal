//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cancom.Data.IDTechAccessV2
{
    using System;
    
    public partial class sp_select_csspm_product_by_cid_sid_stationid_Result
    {
        public int ClientID { get; set; }
        public int SiteID { get; set; }
        public string ClientName { get; set; }
        public string SiteName { get; set; }
        public int StationID { get; set; }
        public string StationName { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
    }
}
